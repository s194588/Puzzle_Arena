To run the application start run.bat.
Running the application works when using Java 13.
We expect that it should work as long as the java run time is java 11 or newer.


Javafx and its vm arguments should already be set up.
However if for some reason it does not work, then you can open cmd
and type:

java --module-path "path to javafx lib" --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.media -jar Puzzle_Arena.jar

(where the path is your path to javafx)