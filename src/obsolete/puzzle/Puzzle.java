package obsolete.puzzle;

import main.puzzle.dataloading.IPuzzleLoader;
import main.puzzle.logic.IFigure;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

//Class code by Frederik (s194619)

public class Puzzle {

    private IPuzzleLoader puzzleLoader;

    public Polygon form;
    Map<Integer, SimpFig> pieceMap = new HashMap<Integer, SimpFig>();
    Map<Integer, PUnion> unionMap = new HashMap<Integer, PUnion>();

    public Puzzle(IPuzzleLoader puzzleLoader) throws IOException {
        this.puzzleLoader = puzzleLoader;
        this.loadForm();
        this.reload();
    }

    public IFigure getPieceFigure(int id) {
        //return null; pieceMap.get(id)
        //return pieceMap.get(id);
        return null;
    }

    public IFigure getUnionFigure(int id) {
        // return unionMap.get(id);
        return null;
    }


    private void loadForm() throws IOException {
        Vec[] points = puzzleLoader.loadForm();
        form = new Polygon(points);
    }

    public void connect(int id1, int id2) {

    }

    public void disconnect(int id) {
        // unionMap.get(id)
    }

    public Polygon getForm() {
        return form;
    }


    public void clear() {
        unionMap.clear();
        pieceMap.clear();
    }

    public int getPieceCount() {
        return pieceMap.size();
    }


    public int getUnionCount() {
        return unionMap.size();
    }


    public void reload() throws IOException {
        clear();
        Vec[][] points = puzzleLoader.loadPuzzle();
        int i = 0;
        for (Vec[] v : points) {
            SimpFig p = new SimpFig(v);
            pieceMap.put(i, p);
            i++;
        }
    }


    public boolean isSolved() {
        return unionMap.size() <= 1;
    }
}
class SimpFig {

    private Vec position = new Vec(0,0);
    float angle = 0;

    private Vec[] vertices;

    public SimpFig(Vec[] vertices){
        this.vertices = vertices;
    }


    public final Vec[] getVertices() {
        return vertices;
    }


    public Vec getPosition() {
        return position;
    }


    public void setPosition(Vec v) {

        for (int i = 0; i < vertices.length; i++){
            vertices[i] = Vec.add(v, Vec.sub(vertices[i],getPosition()));
        }
        position = v;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float a) {
        Vec[] newVertices = new Vec[vertices.length];
        for (int i = 0; i < vertices.length; i++){
            Vec vertex = vertices[i];
            newVertices[i] = new Vec((float) (vertex.x * Math.cos(a-angle)), (float) (vertex.y * Math.sin(a-angle)));
        }
        vertices = newVertices;
    }
}

/*
   @Override
    public void reload() throws IOException {
        clear();
        Vec[][] points = puzzleLoader.loadPuzzle();
        int i = 0;
        for (Vec[] v : points) {
            PuzzlePiece p = new PuzzlePiece(v);
            pieceMap.put(i, p);
            unionMap.put(i, new PUnion(p));
            i++;
        }
    }
 */