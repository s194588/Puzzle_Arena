package obsolete.puzzle;

import main.puzzle.util.interfaces.ISingleArgumentFunction;
import main.puzzle.util.algorithms.graphs.GraphSearch;
import main.puzzle.util.algorithms.graphs.INode;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;

//Class code by Frederik (s194619)
public class PuzzlePiece implements INode {

    private List<PuzzlePiece> neighbors = new LinkedList<>();

    private GroupInfo groupInfo;
    private Vec position = Vec.zero;
    private Matrix2 rotation = Matrix2.rotMatrix(0);
    private Vec[] vertices;

    public PuzzlePiece(Vec[] vertices){
        this.vertices = vertices;
        Vec midpoint = Vec.zero;
        for (Vec v : vertices){
            midpoint = Vec.add(midpoint,v);
        }
        midpoint = Vec.mul(midpoint,vertices.length);
        groupInfo = new GroupInfo(1, midpoint);
    }

    public void move(Vec dv){
        ISingleArgumentFunction<PuzzlePiece> func = new ISingleArgumentFunction<PuzzlePiece>() {
            @Override
            public void run(PuzzlePiece p) {
                p.position = Vec.add(p.position, dv);
                for (int i = 0; i < p.vertices.length; i++) {
                    p.vertices[i] = Vec.add(p.vertices[i], dv);
                }
            }
        };
        GraphSearch.bfs(this,func);
    }

    public void moveTo(Vec v){
        move(Vec.sub(v,position));
    }

    public void getAvailableContactEdges(){

    }

    @Override
    public List<PuzzlePiece> getNeighbors() {
        return neighbors;
    }
}

/*
 private float angle;
    private Vec position = new Vec();

    private Vec[] vertices;

    public PuzzlePiece(Vec[] vertices){
        this.vertices = vertices;
    }

    @Override
    public final Vec[] getVertices() {
        return vertices;
    }

    @Override
    public Vec getPosition() {
        return new Vec(position);
    }

    @Override
    public void setPosition(Vec v) {
        for (Vec vertex : vertices){
            vertex.sub(getPosition());
            vertex.add(v);
        }
        position = new Vec(v);
    }

    @Override
    public float getAngle() {
        return angle;
    }

    @Override
    public void setAngle(float a) {
        for (Vec vertex : vertices){
            vertex = new Vec((float) (vertex.x * Math.cos(a-angle)), (float) (vertex.y * Math.sin(a-angle)));
        }
    }
    */
