package obsolete.puzzle.pieces;

//Enum code by Frederik (s194619)
public enum ConnectionCondition
{
    VERTEX_OVERLAP,
    EDGE_OVERLAP
}
