package obsolete.puzzle.pieces;

import main.puzzle.logic.PuzzlePiece;
import main.puzzle.util.geometry.polygon.Polygon;

import java.util.List;

//Class code by Frederik (s194619)
public class PG {

    public PG (PuzzlePiece initial){

    }

    private List<PuzzlePiece> pieces;


    public void add(PuzzlePiece piece){
        pieces.add(piece);
        updated = true;
    }

    public void remove(PuzzlePiece piece){
        pieces.remove(piece);
        updated = true;
    }

    private PuzzlePiece union;
    private boolean updated;

    public PuzzlePiece getUnion()
    {
        if (updated){
            updated = false;
            calculateUnion();
        }
        return union;
    }

    private void calculateUnion()
    {
        union = pieces.get(0);
        for (int i = 1; i < pieces.size(); i++)
        {
            union = (PuzzlePiece) Polygon.union(union, pieces.get(i));
        }
    }
}
