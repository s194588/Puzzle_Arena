package obsolete.puzzle;

import main.puzzle.logic.IFigure;
import main.puzzle.util.geometry.linearalgebra.Vec;

//Class code by Frederik (s194619)
public abstract class Figure implements IFigure {
    private Vec position = new Vec(0, 0);

    @Override
    public Vec getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vec v) {
        position = v;
    }

    @Override
    public float getAngle() {
        return 0;
    }

    @Override
    public void setAngle(float a) {

    }
}
