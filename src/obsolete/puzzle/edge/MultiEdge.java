package obsolete.puzzle.edge;

import javafx.collections.transformation.SortedList;

//Class code by Frederik (s194619)
class MultiEdge
{
    private SortedList<SubEdge> subEdges;
    public void neighborUnification(SubEdge e){
        int index = subEdges.indexOf(e);
        if (index == -1){
            throw new IllegalArgumentException("The given edge is not a sub edge to this super edge");
        }
        SubEdge m = subEdges.get(index);
        if (index != subEdges.size() - 1){
            m = join(m,subEdges.get(subEdges.size() - 1));
        }
        if (index != 0){
            join(m,subEdges.get(0));
        }
    }
    public SubEdge join(SubEdge e1, SubEdge e2){
        int index1 = subEdges.indexOf(e1);
        int index2 = subEdges.indexOf(e2);
        if (index1 == -1 || index2 == -1) {
            throw new IllegalArgumentException("At least one of the given sub edges do not belong to this super edge");
        }
        if (Math.abs(index1 - index2) != 1){
            throw new IllegalArgumentException("The 2 sub edges are not next to each other");
        }
        subEdges.remove(index1);
        subEdges.remove(index2);
        SubEdge m = new SubEdge(Math.min(e1.FROM_PERCENT, e2.FROM_PERCENT),Math.max(e1.TO_PERCENT, e2.TO_PERCENT));
        subEdges.add(m);
        return m;
    }
}
