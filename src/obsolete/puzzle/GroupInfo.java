package obsolete.puzzle;

import main.puzzle.util.geometry.linearalgebra.Vec;

//Class code by Frederik (s194619)
public class GroupInfo {
    public final int pieceCount;
    public final Vec midpoint;
    public GroupInfo(int pieceCount, Vec midpoint){
        this.pieceCount = pieceCount; this.midpoint = midpoint;
    }
    public static GroupInfo combine(GroupInfo g1, GroupInfo g2)
    {
        Vec w1 = Vec.mul(g1.midpoint, 1f/g1.pieceCount);
        Vec w2 = Vec.mul(g2.midpoint, 1f/g1.pieceCount);
        int count = g1.pieceCount + g2.pieceCount;
        return new GroupInfo(count, Vec.mul(Vec.add(w1,w2),count));
    }
}
