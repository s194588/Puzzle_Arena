package obsolete.puzzle;

import java.util.HashSet;
import java.util.Set;

//Class code by Frederik (s194619)
public class PUnion {

    public PUnion(PuzzlePiece p) {
        if (p == null) {
            throw new IllegalArgumentException("PUnion must contain at least one puzzle piece.");
        }
        pieceSet.add(p);
    }

    private PUnion(Set<PuzzlePiece> set) {
        this.pieceSet = set;
    }

    private Set<PuzzlePiece> pieceSet = new HashSet<PuzzlePiece>();

    public static PUnion union(PUnion pu1, PUnion pu2) {
        Set s = new HashSet<PuzzlePiece>();
        s.addAll(pu1.pieceSet);
        s.addAll(pu2.pieceSet);
        PUnion ret = new PUnion(s);
        return ret;
    }
}
