package obsolete.sample;

import main.gui.GameScene;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        GameScene x = new GameScene();
        primaryStage.setScene(x.onLoad());
        primaryStage.show();
    }

    public void test(Stage primaryStage) {
        Group root = new Group();

        Scene s = new Scene(root, 300, 275);
        // Set scene color
        s.setFill(Color.BLUE);

        primaryStage.setTitle("Title");
        primaryStage.setScene(s);
        primaryStage.show();

        // Polygon creation starts here:
        Polygon p = new Polygon();

        // The polygons vertices
        p.getPoints().addAll(5.0, 5.0, 50.0, 10.0, 30.0, 30.0, 10.0, 20.0);
        // Puts image on polygon
        // Image img = new Image("obsolete.sample/shroom.jpg");
        Image img = new Image("obsolete/sample/test.gif");
        ImagePattern x = new ImagePattern(img, 100.0, 100.0, 100.0, 100.0, false);
        p.setFill(x);

        // Rotates polygon
        p.setRotate(50);
        p.setLayoutY(100);
        p.setLayoutX(100);

        // Adds it to the scene by adding it to the scene's root
        root.getChildren().add(p);
    }

    public static void main(String[] args) {
        launch(args);
    }

}
