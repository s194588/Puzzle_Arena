package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGridNode;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGrid;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.Polygon;

import java.util.*;

//Class code by Frederik (s194619)
public class PrioritySolver implements ISolvingAlgorithm {

    private final Comparator<LoopingGridNode> comparator = new Comparator<LoopingGridNode>() {
        @Override
        public int compare(LoopingGridNode o1, LoopingGridNode o2) {
            return -Integer.compare(count(o1), count(o2));
        }
        private int count(LoopingGridNode o){
            int res = 0;
            /*for (GridNode n : o.plusNeighbors()){
                if (n.id != null)
                    res+=10;
            }
            for (GridNode n : o.diagNeighbors()){
                if (n.id != null)
                    res++;
            }*/
            for (LoopingGridNode n : o.plusNeighbors()){
                if (n.id != null)
                    res++;
            }
            return res;
        }
    };

    private int testcount(LoopingGridNode o){
        int res = 0;
        for (LoopingGridNode n : o.plusNeighbors()){
            if (n.id != null)
                res++;
        }
        return res;
    }

    private int width, height;
    private LoopingGrid<LoopingGridNode> grid;
    //Heap invariant: always contains grid nodes where id is null.
    private PriorityQueue<LoopingGridNode> heap = new PriorityQueue<>(comparator);
    private Set<Integer> remaining = new HashSet<>();

    private IPuzzle puzzle;
    private IConnectionCalculator calculator;

    private int p0;

    public PrioritySolver(IConnectionCalculator calculator){
        this.calculator = calculator;
    }

    @Override
    public boolean solve(IPuzzle puzzle) {
        this.puzzle = puzzle;
        setup();
        while (!remaining.isEmpty()){
            LoopingGridNode node = heap.poll();
            System.out.println("polled: (" +node.x + ", " + node.y + ")");
            if (heap.isEmpty())
                return false;
            PuzzleConnection next = findFit(node);
            System.out.println("fit: " + (next != null?next.outlierID:null));
            if (next == null) {
                /*System.out.println("Failed: " + remaining.size());
                System.out.println(calculator.getConnections(puzzle,7,0).size());
                System.out.println(calculator.getConnections(puzzle,2,0).get(0).deltaPos + " | " + getQuadrant(calculator.getConnections(puzzle,2,0).get(0).deltaPos, (float)Math.PI/4f));
                System.out.println(node.x + ", " + node.y);
                System.out.println(grid.get(2,0).id);
                for (GridNode n : heap){
                    System.out.println("("+n.x+", "+n.y+") -> " + testcount(n));
                }
                System.out.println("****");

                for (GridNode n : node.plusNeighbors()){
                    System.out.println(n.id);
                }
                //return false;*/
            }else {
                next.tryApply();
                put(node, next.outlierID);
            }
        }
        centralize();
        return true;
    }

    private void setup(){
        atLeastX(puzzle);
        System.out.println("test: ***");
        System.out.println(calculator.getConnections(8,2).size());
        System.out.println(calculator.getConnections(8,3).size());
        p0 = 0;
        width = puzzle.getPieceCount() + 1;
        height = puzzle.getPieceCount() + 1;
        heap.clear();
        remaining.clear();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            if (i != p0)
                remaining.add(i);
        }

        int dimx = width, dimy = height;
        LoopingGridNode[][] g = new LoopingGridNode[dimx][dimy];
        grid = new LoopingGrid<>(g,width,height);
        for (int y = 0; y < dimy; y++){
            for (int x = 0; x < dimx; x++){
                g[x][y] = new LoopingGridNode(grid, x,y);
            }
        }
        put(grid.get(0,0), p0);
    }

    private void put(LoopingGridNode node, int id)
    {
        List<LoopingGridNode> neighbors = node.plusNeighbors();
        for (LoopingGridNode n : neighbors) {
            heap.remove(n);
        }
        node.id = id;
        heap.remove(node);
        remaining.remove(id);

        for (LoopingGridNode n : neighbors) {
            if (n.id == null)
                heap.add(n);
        }
    }

    private PuzzleConnection findFit(LoopingGridNode node){
        for (int p : remaining){
            PuzzleConnection fit = getConnectionFit(node, p);
            if (fit != null)
                return fit;
        }
        return null;
    }

    private PuzzleConnection getConnectionFit(LoopingGridNode node, int id)
    {

        LoopingGridNode up = grid.get(node.x, node.y+1);
        LoopingGridNode down = grid.get(node.x, node.y-1);
        LoopingGridNode right = grid.get(node.x+1, node.y);
        LoopingGridNode left = grid.get(node.x-1, node.y);

        boolean x = false;
        if (up.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, up.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 3) {
                    return c;
                }
            }
            return null;
        }
        if (down.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, down.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 1) {
                    return c;
                }
            }
            return null;
        }
        if (right.id != null) {
            System.out.println("yea");
            x= true;
            List<PuzzleConnection> cons = calculator.getConnections(id, right.id);
            for (PuzzleConnection c : cons) {
                System.out.println("Yeah...");
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 2) {
                    System.out.println("YEAAAAHHHH BOI");
                    return c;
                }
            }
            return null;
        }
        if (left.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, left.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 4) {
                    return c;
                }
            }
            return null;
        }
        return null;
    }

    public int getQuadrant(Vec v, float offsetAngle){
        Vec base = Vec.polarVec(offsetAngle,1);
        float angle = Vec.angleBetween(base, v);
        if (angle >= 0 && angle < Math.PI/2)
            return 1;
        if (angle >= Math.PI/2 && angle < Math.PI)
            return 2;
        if (angle >= Math.PI && angle < 3*Math.PI/2)
            return 3;
        if (angle >= 3*Math.PI/2 && angle < 2*Math.PI)
            return 4;
        throw new RuntimeException("Something went wrong");
    }


    public void centralize()
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }

    public void atLeastX(IPuzzle p){
        System.out.println("Checking at least x...");
        for (int i = 0; i < p.getPieceCount(); i++){
            int c = 0;
            for (int j = 0; j < p.getPieceCount(); j++){
                List<PuzzleConnection> cons = calculator.getConnections(i,j);
                if (!cons.isEmpty()){
                    c++;
                }
            }
            if (!(c>=2)){
                System.out.println("Failed on: " + i);
            }
        }
        System.out.println("Done checking.");
    }
}
/*

    private PuzzleConnection getConnectionFitOld(GridNode node, int id)
    {
        //GridNode up = grid[node.x][GeneralUtil.loop(node.y+1, 0, height)];
        //GridNode down = grid[node.x][GeneralUtil.loop(node.y-1, 0, height)];
        //GridNode right = grid[GeneralUtil.loop(node.x+1, 0, width)][node.y];
        //GridNode left = grid[GeneralUtil.loop(node.x-1, 0, width)][node.y];

    GridNode up = grid.get(node.x, node.y+1);
    GridNode down = grid.get(node.x, node.y-1);
    GridNode right = grid.get(node.x+1, node.y);
    GridNode left = grid.get(node.x-1, node.y);

    List<PuzzleConnection> connections = new ArrayList<>();
    boolean x = false;
        if (up.id != null) {
                x = true;
                List<PuzzleConnection> cons = calculator.getConnections(puzzle, id, up.id);
        //if (cons.isEmpty())
        //    return null;
        connections.addAll(cons);
        }
        if (down.id != null) {
        x = true;
        List<PuzzleConnection> cons = calculator.getConnections(puzzle, id, down.id);
        //if (cons.isEmpty())
        //    return null;
        connections.addAll(cons);
        }
        if (right.id != null) {
        x= true;
        List<PuzzleConnection> cons = calculator.getConnections(puzzle, id, right.id);
        //if (cons.isEmpty())
        //    return null;
        connections.addAll(cons);
        }
        if (left.id != null) {
        x = true;
        List<PuzzleConnection> cons = calculator.getConnections(puzzle, id, left.id);
        //if (cons.isEmpty())
        //    return null;
        connections.addAll(cons);
        }
        if (!x)
        System.out.println("BIG :(");
        if (connections.isEmpty())
        return null;
        return connections.get(0);
        }
 */
/*

    @Override
    public boolean solve(IPuzzle puzzle) {
        this.puzzle = puzzle;
        setup();
        while (!remaining.isEmpty()){
            GridNode node = heap.poll();

            System.out.println("polled: (" +node.x + ", " + node.y + ")");
            PuzzleConnection next = findFit(node);
            System.out.println("fit: " + (next != null?next.outlierID:null));
            if (next == null && heap.isEmpty()) {
                System.out.println("Failed: " + remaining.size());
                System.out.println(calculator.getConnections(puzzle,7,0).size());
                System.out.println(calculator.getConnections(puzzle,2,0).get(0).deltaPos + " | " + getQuadrant(calculator.getConnections(puzzle,2,0).get(0).deltaPos, (float)Math.PI/4f));
                System.out.println(node.x + ", " + node.y);
                System.out.println(grid.get(2,0).id);
                for (GridNode n : heap){
                    System.out.println("("+n.x+", "+n.y+") -> " + testcount(n));
                }
                System.out.println("****");

                for (GridNode n : node.plusNeighbors()){
                    System.out.println(n.id);
                }
                return false;
            }else {
                next.tryApply();
                put(node, next.outlierID);
            }
        }
        centralize();
        return true;
    }
 */