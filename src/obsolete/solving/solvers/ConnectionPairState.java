package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.connecting.PuzzleConnection;

import java.util.Arrays;
import java.util.Collection;

//Class code by Frederik (s194619)
public class ConnectionPairState
{
    private IPuzzle puzzle;
    private Collection<PuzzleConnection> connections;
    private Integer[][] descriptiveID;
    private int id;


    public ConnectionPairState(IPuzzle puz, Collection<PuzzleConnection> cons){
        this.puzzle = puz;
        connections = cons;
        calculateID();
    }

    private void calculateID()
    {
        descriptiveID = new Integer[puzzle.getPieceCount()][2];
        for (PuzzleConnection c : connections)
        {
            descriptiveID[c.outlierID][0] = c.targetID;
            descriptiveID[c.outlierID][1] = c.getPlacementID();
        }
        //id = Arrays.deepHashCode(descriptiveID);
    }

    @Override
    public boolean equals(Object obj) {
        //System.out.println("Checking equals.");
        ConnectionPairState other = (ConnectionPairState) obj;
        if (descriptiveID.length != other.descriptiveID.length)
            return false;
        for (int i = 0; i < descriptiveID.length; i++){
            for (int j = 0; j < 2; j++){
                if (descriptiveID[i][j] == null && other.descriptiveID[i][j] == null)
                    continue;
                if (descriptiveID[i][j] == null || other.descriptiveID[i][j] == null)
                    return false;
                if (!descriptiveID[i][j].equals(other.descriptiveID[i][j])){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(descriptiveID);
    }

    public String getID(){
        return descriptiveIDToString();
    }
    public String descriptiveIDToString(){
        String ret = "";
        for (Integer[] a : descriptiveID){
            for (Integer i : a){
                if (i == null)
                    ret += "x";
                else
                    ret += i;
                ret+= ";";
            }
            ret += "-";
        }
        return ret;
    }
}