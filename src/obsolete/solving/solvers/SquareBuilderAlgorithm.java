package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.general.GeneralUtil;

import java.util.*;

//Class code by Frederik (s194619)
public class SquareBuilderAlgorithm {
/*
    private IPuzzle puzzle;
    private int width, height;
    private int layer;
    private GridNode[][] grid;
    private IConnectionCalculator calculator;
    private Queue<Integer> remaining = new LinkedList<>();
    private int p0;

    public boolean solve(IPuzzle puzzle){
        setup();

        Rim rim = extend();

        while (remaining.size() > 0) {
            rim.tryBuild();
            rim = extend();
        }

        return true;
    }

    private void setup(){
        p0 = 0;
        remaining.clear();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            if (i != p0)
                remaining.add(i);
        }

        int dimx = width + 1, dimy = height + 1;
        grid = new GridNode[dimx][dimy];
        for (int x = 0; x < dimx; x++){
            for (int y = 0; y < dimy; y++){
                grid[x][y] = new GridNode(grid, x,y);
            }
        }

        grid[0][0].id = p0;
        layer = 0;
    }

    //TODO: Make sure the tryApply applies the correct connection. That is the one where it fits perfectly.

    private PuzzleConnection next()
    {
        return null;
    }

    private Rim extend()
    {
        List<GridNode> top = new ArrayList<>();
        List<GridNode> down = new ArrayList<>();
        List<GridNode> right = new ArrayList<>();
        List<GridNode> left = new ArrayList<>();

        for (int i = 0; i < layer*2-1; i++){
            top.add(grid[(layer % height)][i % width]);
            down.add(grid[-(layer % height)][i % width]);
        }
        for (int i = 0; i < layer*2-1; i++){
            right.add(grid[i % height][(layer%width)]);
            left.add(grid[i % height][-(layer%width)]);
        }
        GridNode topright, topleft, botright, botleft;
        //Not finished
        return null;
    }

    private Rim retract(){

        return null;
    }
    private class Rim
    {
        private GridNode[][] grid;
        public List<GridNode> left, right, top, bot;
        public GridNode topright, topleft, botright, botleft;
        //Improvement: reset based on stack.

        public boolean tryBuild()
        {
            boolean t = collectiveMatch(top);
            boolean b = collectiveMatch(bot);
            boolean r = collectiveMatch(right);
            boolean l = collectiveMatch(left);
            boolean failed = false;
            if (t && r) {
                PuzzleConnection c = findFit(topright);
                if (c == null)
                    failed = true;
                else
                    c.tryApply();
            }
            if (t && l) {
                PuzzleConnection c = findFit(topleft);
                if (c == null)
                    failed = true;
                else
                    c.tryApply();
            }
            if (b && r) {
                PuzzleConnection c = findFit(botright);
                if (c == null)
                    failed = true;
                else
                    c.tryApply();
            }
            if (b && l) {
                PuzzleConnection c = findFit(botleft);
                if (c == null)
                    failed = true;
                else
                    c.tryApply();
            }
            if (failed){
                clear();

            }
            return true;
        }

        private PuzzleConnection getConnectionFit(GridNode node, int id){
            GridNode up = grid[node.x][(node.y + 1) % height];
            GridNode down = grid[node.x][(node.y - 1) % height];
            GridNode right = grid[(node.x+1) % width][node.y];
            GridNode left = grid[(node.x-1) % width][node.y];

            List<PuzzleConnection> cons = new ArrayList<>();

            if (up.id != null)
                cons.addAll(calculator.getConnections(puzzle, id, up.id));
            if (down.id != null)
                cons.addAll(calculator.getConnections(puzzle, id, down.id));
            if (right.id != null)
                cons.addAll(calculator.getConnections(puzzle, id, right.id));
            if (left.id != null)
                cons.addAll(calculator.getConnections(puzzle, id, left.id));

            if (cons.isEmpty())
                return null;
            return cons.get(0);
        }

        public void clear(){
            topright.id = null;
            topleft.id = null;
            botright.id = null;
            botleft.id = null;
            resetNodes(top);
            resetNodes(bot);
            resetNodes(right);
            resetNodes(left);
        }
        //Complete match (fuldstaendig)
        private boolean collectiveMatch(List<GridNode> nodes)
        {
            for (GridNode n : nodes)
            {
                PuzzleConnection fit = findFit(n);
                if (fit == null) {
                    resetNodes(nodes);
                    return false;
                }
                fit.tryApply();
                n.id = fit.outlierID;
                remaining.remove(fit.outlierID);
            }
            return true;
        }
        private void resetNodes(List<GridNode> nodes){
            for (GridNode n : nodes)
                n.id = null;
        }

        private PuzzleConnection findFit(GridNode node){
            for (int p : remaining){
                PuzzleConnection fit = getConnectionFit(node, p);
                if (fit != null)
                    return fit;
            }
            return null;
        }
    }*/
}

/*
private boolean collectiveMatchBT(List<GridNode> nodes)
    {
        Stack<PuzzleConnection> s = new Stack<>();
        Set<ConnectionPairState> checked = new HashSet<>();
        for (GridNode n : nodes)
        {
            PuzzleConnection c = findFit(n);
            ConnectionPairState state = new ConnectionPairState(puzzle, s);
            if (c == null) {
                checked.add(state);
            }
            if (checked.contains(state)){
                if (s.isEmpty())
                    return false;
                s.pop();
            }else{
                s.push(c);
            }
        }
        return true;
    }
 */