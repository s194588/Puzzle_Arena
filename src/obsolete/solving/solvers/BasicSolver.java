package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;

import java.util.LinkedList;
import java.util.List;

//Class code by Frederik (s194619)
public class BasicSolver implements ISolvingAlgorithm {

    public BasicSolver(IConnectionCalculator c)
    {
        connectionCalculator = c;
    }
    private IConnectionCalculator connectionCalculator;

    @Override
    public boolean solve(IPuzzle p) {

        int x = 0;
        for (int j =0; j < p.getPieceCount(); j++){
            if (!connectionCalculator.getConnections(j,0).isEmpty()){
                x++;
                System.out.println("p: " + j);
            }
        }
        System.out.println("DFFF "+x);

        atLeastOne(p);
        return solve2(p);
    }

    public boolean solve2(IPuzzle puzzle) {
        if (puzzle.getPieceCount() == 0){
            return true;
        }
        Set<Integer> checked = new HashSet<>();
        checked.add(0);
        Queue<Integer> queue = new LinkedList<>(); //DO NOT USE PRIORITY QUEUE
        for (int i = 1; i < puzzle.getPieceCount(); i++){
            queue.add(i);
        }
        int x = 0;
        int maxIterations = 1000000;
        while (!queue.isEmpty() && x < maxIterations)
        {
            x++;
            int next = queue.poll();
            boolean succeeded = false;
            for (int j : checked)
            {
                List<PuzzleConnection> cons = connectionCalculator.getConnections(next, j);
                //System.out.println("next: " + next + ", j: " + j + ", res: " + cons.isEmpty());
                if (!cons.isEmpty()){
                    cons.get(0).tryApply();
                    checked.add(next);
                    succeeded = true;
                    break;
                }
            }
            if (!succeeded){
                queue.add(next);
            }

        }
        if (x >= maxIterations){
            System.out.println("Hit max allowed loop iterations");
        }

        centralize(puzzle);

        System.out.println("Done!");
        return true;
    }

    public boolean solve1(IPuzzle puzzle) {

        int conCount = 0;
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            //System.out.println("i : " + i);
            for (int j = 0; j < puzzle.getPieceCount(); j++){
                for (PuzzleConnection c : connectionCalculator.getConnections(i, j))
                {
                    //System.out.println(connectionCalculator.getConnections(puzzle, i,j).size());
                    c.tryApply();
                    conCount++;
                }
            }
        }
        System.out.println("Done! conCount: " + conCount);
        return true;
    }

    public void centralize(IPuzzle puzzle)
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }


    public boolean solve3(IPuzzle puzzle) {

        int conCount = 0;
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            //System.out.println("i : " + i);
            for (int j = 0; j < puzzle.getPieceCount(); j++){
                boolean b = false;
                for (PuzzleConnection c : connectionCalculator.getConnections(i,j))
                {
                    //System.out.println(connectionCalculator.getConnections(puzzle, i,j).size());
                    c.tryApply();
                    conCount++;
                    b = true;
                }
                if (b)
                    break;
            }
        }
        System.out.println("Done! conCount: " + conCount);
        return true;
    }


    public void atLeastOne(IPuzzle p){
        System.out.println("Checking at least x...");
        for (int i = 0; i < p.getPieceCount(); i++){
            int c = 0;
            for (int j = 0; j < p.getPieceCount(); j++){
                List<PuzzleConnection> cons = connectionCalculator.getConnections(i,j);
                if (!cons.isEmpty()){
                    c++;
                }
            }
            if (!(c>=2)){
                System.out.println("Failed on: " + i);
            }
        }
        System.out.println("Done checking.");
    }
}
/*
public class BasicSolver implements ISolvingAlgorithm {

    public BasicSolver(IConnectionCalculator c)
    {
        connectionCalculator = c;
    }
    private IConnectionCalculator connectionCalculator;

    @Override
    public boolean solve(IPuzzle puzzle) {

        int conCount = 0;
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            //System.out.println("i : " + i);
            for (int j = 0; j < puzzle.getPieceCount(); j++){
                for (PuzzleConnection c : connectionCalculator.getConnections(puzzle, i,j))
                {
                    //System.out.println(connectionCalculator.getConnections(puzzle, i,j).size());
                    c.tryApply();
                    conCount++;
                }
            }
        }
        System.out.println("Done! conCount: " + conCount);
        return true;
    }

    private boolean fitInsideForm(IPuzzle puzzle, PuzzleConnection connection) {
        List<Vertex> formPoints = puzzle.getForm().getVertices();

        connection.printBorderVertices();

        return true;
    }
}

 */
