package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.PolygonUtil;

import java.util.*;

//Class code by Frederik (s194619)
public class BTS2 implements ISolvingAlgorithm {

    public BTS2(IConnectionCalculator connectionCalculator)
    {
        this.connectionCalculator = connectionCalculator;
    }

    private IConnectionCalculator connectionCalculator;

    private Set<ConnectionPairState> checkedStates = new HashSet<>();

    private float marginOfError = 0.01f;

    private long combinations()
    {
        int res = 1;
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            for (int j = 0; j < i; j++){
                int size = connectionCalculator.getConnections(i,j).size();
                if (size != 0)
                    res *= size;
            }
        }
        return res;
    }

    private boolean isValid()
    {
        ConnectionPairState state = new ConnectionPairState(puzzle, stack);
        //System.out.println(checkedStates.contains(state));
        if (checkedStates.contains(state)) {
            //System.out.println("here");
            return false;
        }
        boolean isValid = true;
        List<PuzzlePiece> connected = f(); //HERE
        //System.out.println(connected.size());
        Polygon box = Polygon.boundingBox(connected);

        if (PolygonUtil.rectArea(box) > PolygonUtil.rectArea(puzzle.getForm())*(1+marginOfError)){
            isValid = false;
            //System.out.println(checkedStates.size());
            checkedStates.add(state);
        }
        //System.out.println(isValid + ", b: " + PolygonUtil.rectArea(box) + ", f: " + PolygonUtil.rectArea(puzzle.getForm()));
        return isValid;
    }

    private boolean isValid(PuzzleConnection c)
    {
        stack.push(c);
        boolean isValid = isValid();
        stack.pop();
        return isValid;
    }

    private List<PuzzlePiece> f(){
        List<PuzzlePiece> ret = new ArrayList<>();
        for (int i : che)
            ret.add(puzzle.getPiece(i));
        return ret;
    }

    private IPuzzle puzzle;
    private Queue<Integer> que = new LinkedList();
    private HashSet<Integer> che = new HashSet<>();
    private Stack<PuzzleConnection> stack = new Stack<>();

    private PuzzleConnection next(){
        int count = que.size();
        for (int i = 0; i < count; i++) {
            if (que.isEmpty())
                return null;
            int next = que.poll();
            for (int j : che) {
                List<PuzzleConnection> cons = connectionCalculator.getConnections(next, j);
                for (PuzzleConnection c : cons) {
                    if (isValid(c)) {
                        che.add(next);
                        return c;
                    }
                }
            }
            que.add(next);
        }
        return null;
    }

    private void setup(){
        checkedStates.clear();
        stack.clear();
        che.clear();
        que.clear();

        che.add(0);
        for (int i = 1; i < puzzle.getPieceCount(); i++){
            que.add(i);
        }
        System.out.println("Possible combinations: " + combinations());
    }

    public boolean solve(IPuzzle puzzle){
        this.puzzle = puzzle;
        setup();

        while (true){
            //System.out.println("new it " + (new ConnectionPairState(puzzle, stack)).descriptiveIDToString());
            boolean valid = isValid();
            //System.out.println(valid);

            if (valid && que.isEmpty()){
                centralize();
                return true;
            }

            PuzzleConnection next = next();
            //System.out.println(next);

            if (next == null && stack.isEmpty()) {
                return false;
            }

            //System.out.println(valid + "><" + next);
            if (!valid || next == null) {
                backtrack();
            }
            else {
                advance(next);
            }
        }
    }
    private void advance(PuzzleConnection connection)
    {
        //System.out.println("Advancing");
        stack.push(connection);
        connection.tryApply();
    }
    private void backtrack(){
        //System.out.println("Backtracking");
        checkedStates.add(new ConnectionPairState(puzzle,stack));
        PuzzleConnection pop = stack.pop();
        que.add(pop.outlierID);
        che.remove(pop.outlierID);
        pop.tryUndo();
    }


    public void centralize()
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }
}