package obsolete.solving.solvers;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.PolygonUtil;

import java.util.*;

//Class code by Frederik (s194619)
public class BacktrackingSolver implements ISolvingAlgorithm {

    public BacktrackingSolver(IConnectionCalculator connectionCalculator)
    {
        this.connectionCalculator = connectionCalculator;
    }

    private IConnectionCalculator connectionCalculator;

    public boolean solve(IPuzzle puzzle) {

        if (puzzle.getPieceCount() == 0){
            return true;
        }
        clearChecked();
        stack.clear();
        //System.out.println(getStateID(puzzle));

        List<PuzzlePiece> checkedPieces = new ArrayList<>();
        Set<Integer> checked = new HashSet<>();
        checkedPieces.add(puzzle.getPiece(0));
        System.out.println(puzzle.getPiece(0));
        checked.add(0);

        Queue<Integer> queue = new LinkedList<>();
        for (int i = 1; i < puzzle.getPieceCount(); i++){
            queue.add(i);
        }

        while (true){

            for (int i = 0; i < queue.size(); i++)
            {
                int next = queue.poll();
                boolean succeeded = false;
                for (int j : checked) {
                    if (succeeded)
                        break;
                    List<PuzzleConnection> cons = connectionCalculator.getConnections(next, j);
                    //System.out.println("next: " + next + ", j: " + j + ", res: " + cons.isEmpty());
                    for (PuzzleConnection c : cons) {
                        stack.push(c);
                        if (!isValid(puzzle, checkedPieces)) {
                            stack.pop();
                            continue;
                        }
                        //State needs to save the connection id (eg index in the list)
                        c.tryApply();
                        succeeded = true;
                        break;
                    }
                }
                if (succeeded) {
                    checked.add(next);
                    //System.out.println("uwu1: " + checkedPieces.size());
                    checkedPieces.add(puzzle.getPiece(next));
                    //System.out.println("uwu2: " + checkedPieces.size());
                    i = 0;
                }else{
                    queue.add(next);
                }
            }

            if (queue.isEmpty() && isValid(puzzle,checkedPieces)) {
                System.out.print("Solution found.");
                break;
            }
            closeState(puzzle);
            if (stack.isEmpty()){
                System.out.println("Failed to find a solution.");
                return false;
            }
            PuzzleConnection connection = stack.pop();
            connection.tryUndo();
            queue.add(connection.outlierID);
            checked.remove(connection.outlierID);
            checkedPieces.remove(puzzle.getPiece(connection.outlierID));
            //Queue add
        }


        centralize(puzzle);

        System.out.println("Done!");
        //System.out.println(getStateID(puzzle));
        return true;
    }

    public void centralize(IPuzzle puzzle)
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }

    private Map<String, Boolean> checkedStates = new HashMap<>();
    private Stack<PuzzleConnection> stack = new Stack<>();

    private void closeState(IPuzzle puzzle){
        checkedStates.put(new ConnectionPairState(puzzle, stack).getID(), false);
    }
    private void clearChecked(){
        checkedStates.clear();
    }

    private boolean isValid(IPuzzle puzzle, List<PuzzlePiece> connected)
    {
        ConnectionPairState state = new ConnectionPairState(puzzle, stack);
        if (checkedStates.containsKey(state.getID())) {
            //System.out.println("Already in cache, returning result.");
            return checkedStates.get(state.getID());
        }
        //System.out.println("Map count: " + checkedStates.size());
        /*for (Map.Entry<ConnectionPairState,Boolean> p : checkedStates.entrySet()){
            System.out.println("Cashed HashCode: " + p.getKey().hashCode());
        }*/
        //System.out.println("This HashCode: " + state.hashCode());
        boolean isValid = true;
        Polygon box = Polygon.boundingBox(connected);
        //System.out.println("Connected size: " + connected.size() + ", connected(0): " + connected.get(0) + ", boxArea: " + PolygonUtil.rectArea(box) + ", formArea: " + PolygonUtil.rectArea(puzzle.getForm()));
        if (PolygonUtil.rectArea(box) > PolygonUtil.rectArea(puzzle.getForm())*1.05f){
            isValid = false;
            //System.out.println("Not valid, cashing state.");
            checkedStates.put(state.getID(), isValid);
        }
        return isValid;
    }



    private Queue<Integer> que = new LinkedList();
    private HashSet<Integer> che = new HashSet<>();
    //private Hash
    private PuzzleConnection next(IPuzzle puzzle){
        int count = que.size();
        for (int i = 0; i < count; i++) {
            if (que.isEmpty())
                return null;
            int next = que.poll();
            boolean succeeded = false;
            for (int j : che) {
                List<PuzzleConnection> cons = connectionCalculator.getConnections(next, j);
                for (PuzzleConnection c : cons) {
                    if (true) { //isValid
                        return c;
                    }
                }
            }
            que.add(next);
        }
        return null;
    }

    public boolean sol(IPuzzle puzzle){
        Stack<PuzzleConnection> s = new Stack<>();
        while (true){
            if (true){
                if (que.isEmpty())
                    return true;
            }else{
                closeState(puzzle);
                continue;
            }

            PuzzleConnection next = next(puzzle);
            if (next == null){
                if (stack.isEmpty())
                    return false;
                closeState(puzzle);
                //PuzzleConnection pop = stack.pop();
                //que.add(pop.outlierID);
                //che.remove(pop.outlierID);
            }
            stack.push(next);
        }
    }
}

/*
package main.puzzle.solving.algorithms;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.connecting.IConnectionCalculator;
import main.puzzle.solving.connecting.PuzzleConnection;

import java.util.*;

public class BacktrackingSolver implements ISolvingAlgorithm {

    public BacktrackingSolver(IConnectionCalculator connectionCalculator)
    {
        this.connectionCalculator = connectionCalculator;
    }

    private IConnectionCalculator connectionCalculator;

    private HashSet<Integer> connected = new HashSet<Integer>();
    private Queue<Integer> remaining = new PriorityQueue<>();

    @Override
    public boolean solve(IPuzzle puzzle) {
        if (connected.isEmpty() && isValid(puzzle)){//if (puzzle.isSolved()){
            return true;
        }
        if (!isValid(puzzle))
        {
            return false;
        }
        int p0 = 0; //HSDAopkpkokpkpok
        for (int p : connected)
        {
            for (PuzzleConnection c : connectionCalculator.getConnections(puzzle,p0,p))
            {
                if (c.tryApply())
                {
                    if (solve(puzzle))
                    {
                        return true;
                    }
                    c.tryUndo();
                }
            }
        }
        return false;
    }

    private boolean isValid(IPuzzle puzzle)
    {
        float[] mmc = new float[]{0f,0f,0f,0f};
        for (int i : connected){
            float[] t = puzzle.getPiece(i).XYminMax();
            mmc = new float[]{Math.min(t[0], mmc[0]),Math.max(t[1], mmc[1]),Math.min(t[2], mmc[2]),Math.max(t[3], mmc[3])};
        }
        float[] mmf = puzzle.getForm().XYminMax();
        float wf = mmf[1] - mmf[0];
        float hf = mmf[3] - mmf[2];

        float wc = mmc[1] - mmc[0];
        float hc = mmc[3] - mmc[2];

        float sqdfmax = Math.max(wf,hf)*Math.max(wf,hf) * 2;
        float sqdc = wc*wc+hc*hc;

        float allowedWiggleRoomPercent = 0.05f;
        return sqdc < sqdfmax*(1+allowedWiggleRoomPercent);
    }



    private int pullNextAvailable()
    {
        return 1;
    }

    private List<Integer> lonePieces()
    {
        return null;
    }

    public boolean solveRec(IPuzzle p)
    {
        return false;
    }
}
/*

package main.puzzle.solving.algorithms;

import main.puzzle.logic.IPuzzle;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class BacktrackingSolver implements ISolvingAlgorithm {

    public BacktrackingSolver(IConnectionCalculator connectionCalculator)
    {
        this.connectionCalculator = connectionCalculator;
    }

    private IConnectionCalculator connectionCalculator;

    private HashSet<Integer> connected = new HashSet<Integer>();
    private HashSet<Integer> remaining = new HashSet<Integer>();

    @Override
    public boolean solve(IPuzzle puzzle) {
        if (puzzle.isSolved()){
            return true;
        }
        if (!isValid(puzzle))
        {
            return false;
        }
        int p0 = pullNextAvailable();
        for (int p : lonePieces())
        {
            for (PuzzleConnection c : connectionCalculator.getConnections(puzzle,p0,p))
            {
                if (c.tryApply())
                {
                    if (solve(puzzle))
                    {
                        return true;
                    }
                    c.tryUndo();
                }
            }
        }
        return false;
    }

    private boolean isValid(IPuzzle puzzle)
    {
        float[] mmc = new float[]{0f,0f,0f,0f};
        for (int i : connected){
            float[] t = puzzle.getPiece(i).XYminMax();
            mmc = new float[]{Math.min(t[0], mmc[0]),Math.max(t[1], mmc[1]),Math.min(t[2], mmc[2]),Math.max(t[3], mmc[3])};
        }
        float[] mmf = puzzle.getForm().XYminMax();
        float wf = mmf[1] - mmf[0];
        float hf = mmf[3] - mmf[2];

        float wc = mmc[1] - mmc[0];
        float hc = mmc[3] - mmc[2];

        float sqdfmax = Math.max(wf,hf)*Math.max(wf,hf) * 2;
        float sqdc = wc*wc+hc*hc;

        float allowedWiggleRoomPercent = 0.05f;
        return sqdc < sqdfmax*(1+allowedWiggleRoomPercent);
    }



    private int pullNextAvailable()
    {
        return 1;
    }

    private List<Integer> lonePieces()
    {
        return null;
    }

    public boolean solveRec(IPuzzle p)
    {
        return false;
    }
}

 */
/*

    public boolean solveB(IPuzzle puzzle) {
        if (puzzle.isSolved()){
            return true;
        }
        if (!isValid(puzzle))
        {
            return false;
        }
        int p0 = pullNextAvailable();
        for (int p : lonePieces())
        {
            for (PuzzleConnection c : connectionCalculator.getConnections(puzzle,p0,p))
            {
                if (c.tryApply())
                {
                    if (solve(puzzle))
                    {
                        return true;
                    }
                    c.tryUndo();
                }
            }
        }
        return false;
    }
 */

/*



        while (true) {
            while (!queue.isEmpty() && x < maxIterations) {
                x++;
                int next = queue.poll();
                boolean succeeded = false;
                for (int j : checked) {
                    if (succeeded)
                        break;

                    List<PuzzleConnection> cons = connectionCalculator.getConnections(puzzle, next, j);
                    //System.out.println("next: " + next + ", j: " + j + ", res: " + cons.isEmpty());
                    if (!cons.isEmpty()) {
                        for (PuzzleConnection c : cons) {
                            stack.push(c);
                            if (!isValid(puzzle, checkedPieces)) {
                                stack.pop();
                                continue;
                            }
                            c.tryApply();
                            checked.add(next);
                            checkedPieces.add(puzzle.getPiece(next));
                            succeeded = true;
                            break; //BREAK x2
                        }
                    }
                }
                if (!succeeded) {
                    queue.add(next);
                }
            }
            if (!isValid(puzzle,checkedPieces)){
                closeState(puzzle);
                PuzzleConnection connection = stack.pop();
                connection.tryUndo();
            }
        }
 */
/*

    public String getStateID(IPuzzle puzzle){
        String ret = "";
        Object[] s = stack.toArray();
        for (int i = 0; i < puzzle.getPieceCount(); i++)
        {
            boolean added = false;
            for (int j = 0; j < s.length; j++){
                PuzzleConnection c = (PuzzleConnection) s[j];
                if (c.outlierID == i){
                    ret += c.targetID  + "-";
                    added = true;
                    break;
                }
            }
            if (!added)
                ret += "x-";
        }
        return ret;
    }
 */
/*
import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.PolygonUtil;

import java.util.*;

public class BacktrackingSolver implements ISolvingAlgorithm {

    public BacktrackingSolver(IConnectionCalculator connectionCalculator)
    {
        this.connectionCalculator = connectionCalculator;
    }

    private IConnectionCalculator connectionCalculator;

    public boolean solve(IPuzzle puzzle) {

        if (puzzle.getPieceCount() == 0){
            return true;
        }
        clearChecked();
        stack.clear();
        System.out.println(getStateID(puzzle));

        List<PuzzlePiece> checkedPieces = new ArrayList<>();
        Set<Integer> checked = new HashSet<>();
        checkedPieces.add(puzzle.getPiece(0));
        System.out.println(puzzle.getPiece(0));
        checked.add(0);

        Queue<Integer> queue = new LinkedList<>();
        for (int i = 1; i < puzzle.getPieceCount(); i++){
            queue.add(i);
        }

        int x = 0;
        int maxIterations = 1000000;
        while (true){

            for (int i = 0; i < queue.size(); i++)
            {
                int next = queue.poll();
                boolean succeeded = false;
                for (int j : checked) {
                    if (succeeded)
                        break;
                    List<PuzzleConnection> cons = connectionCalculator.getConnections(puzzle, next, j);
                    //System.out.println("next: " + next + ", j: " + j + ", res: " + cons.isEmpty());
                    for (PuzzleConnection c : cons) {
                        stack.push(c);
                        if (!isValid(puzzle, checkedPieces)) {
                            stack.pop();
                            continue;
                        }
                        //State needs to save the connection id (eg index in the list)
                        c.tryApply();
                        checked.add(next);
                        System.out.println("uwu1: " + checkedPieces.size());
                        checkedPieces.add(puzzle.getPiece(next));
                        System.out.println("uwu2: " + checkedPieces.size());
                        succeeded = true;
                        break;
                    }
                }
                if (succeeded) {
                    i = 0;
                }else{
                    queue.add(next);
                }
            }

            if (queue.isEmpty() && isValid(puzzle,checkedPieces)) {
                System.out.print("HE");
                break;
            }
            closeState(puzzle);
            PuzzleConnection connection = stack.pop();
            connection.tryUndo();
            queue.add(connection.outlierID);
            checked.remove(connection.outlierID);
            checkedPieces.remove(puzzle.getPiece(connection.outlierID));
            //Queue add
        }


        centralize(puzzle);

        System.out.println("Done!");
        System.out.println(getStateID(puzzle));
        return true;
    }

    public void centralize(IPuzzle puzzle)
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }

    private Map<String, Boolean> checkedStates = new HashMap<>();
    private Stack<PuzzleConnection> stack = new Stack<>();

    private void closeState(IPuzzle puzzle){
        checkedStates.put(getStateID(puzzle), false);
    }
    private void clearChecked(){
        checkedStates.clear();
    }

    private boolean isValid(IPuzzle puzzle, List<PuzzlePiece> connected)
    {
        String state = getStateID(puzzle);
        if (checkedStates.containsKey(state)) {
            System.out.println("GG");
            return checkedStates.get(state);
        }

        boolean isValid = true;
        Polygon box = Polygon.boundingBox(connected);
        System.out.println(PolygonUtil.rectArea(box) + "#" + PolygonUtil.rectArea(puzzle.getForm()));
        System.out.println("walid :" +connected.size());
        System.out.println(connected.get(0));
        if (PolygonUtil.rectArea(box) > PolygonUtil.rectArea(puzzle.getForm())*1.05f){
            isValid = false;
            System.out.println("HJAJA");
            checkedStates.put(state, isValid);
        }
        return isValid;
    }
 */