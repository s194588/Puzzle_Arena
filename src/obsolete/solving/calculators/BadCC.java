package obsolete.solving.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.ArrayList;
import java.util.List;

public class BadCC //implements IConnectionCalculator
{
    public List<PuzzleConnection> getConnections(IPuzzle puzzle, int outlierPiece, int targetPiece) {
        List<PuzzleConnection> ret = new ArrayList();
        ret.add(new PuzzleConnection(puzzle,outlierPiece,targetPiece,new Vec(1,1), 30));
        return ret;
    }
}
