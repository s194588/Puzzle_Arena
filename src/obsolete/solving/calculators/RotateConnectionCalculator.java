package obsolete.solving.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.LinkedList;
import java.util.List;

public class RotateConnectionCalculator implements IConnectionCalculator {

    private IPuzzle puzzle;
    public RotateConnectionCalculator(IPuzzle puzzle){
        this.puzzle = puzzle;
    }

    @Override
    public List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID) {
        SimpleConnectionCalculator simpleConnectionCalculator = new SimpleConnectionCalculator(puzzle);

        List<PuzzleConnection> possibleConnections = new LinkedList<PuzzleConnection>();

        for (PuzzleConnection connection : simpleConnectionCalculator.getAllFormations(puzzle, outlierPieceID, targetPieceID)) {

            PuzzlePiece targetPiece = null;
            PuzzlePiece outlierPiece = null;

            Vec deltaPos = connection.deltaPos;
            Vec commonPoint = connection.commonPointBetweenPuzzles;

            float [] rotationFits = rotateToFit(targetPiece, outlierPiece, commonPoint);

            for (Float rotation : rotationFits) {
                PuzzleConnection possibleConnection = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, deltaPos, rotation);
                possibleConnections.add(possibleConnection);
              }

        }
        return possibleConnections;
    }

    @Override
    public IPuzzle getPuzzle() {
        return puzzle;
    }

    ///This version take account that the two sides of the puzzles have the same length
        private float[] rotateToFit (PuzzlePiece target, PuzzlePiece outlier, Vec commonPoint){

        List outlierPosition = outlier.getVertices();
        List targetPosition = target.getVertices();

//            for (Vec ver) {
//
//            }


        float x = 0;

//        Vec newPositions = Matrix2.MVProd(Matrix2.rotMatrix(x), outlierPosition);


//        for(float deltaAngle: rotateToFit(targetPiece, outlierPiece)){
//            PuzzleConnection connection = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, deltaPos, deltaAngle);
//            allPossibleConnections.add(connection);
//        }
            throw new RuntimeException("Not implemented");
//        return new LinkedList<Float>();

    }
}