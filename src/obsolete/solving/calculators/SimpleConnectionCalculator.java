package obsolete.solving.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.LinkedList;
import java.util.List;

public class SimpleConnectionCalculator implements IConnectionCalculator {

    private IPuzzle puzzle;
    public SimpleConnectionCalculator(IPuzzle puzzle){
        this.puzzle = puzzle;
    }

    @Override
    public List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID) {

        List<PuzzleConnection> possibleConnections = new LinkedList<PuzzleConnection>();

        for (PuzzleConnection connection : getAllFormations(puzzle, outlierPieceID, targetPieceID)) {

            PuzzlePiece targetPiece = (PuzzlePiece) puzzle.getPiece(targetPieceID);
            PuzzlePiece outlierPiece = (PuzzlePiece) puzzle.getPiece(outlierPieceID);
            connection.tryApply();
            if (!targetPiece.insideOverlaps(outlierPiece)) {
                possibleConnections.add(connection);
            }
            connection.tryUndo();
        }

        return possibleConnections;

    }

    @Override
    public IPuzzle getPuzzle() {
        return null;
    }

    public List<PuzzleConnection> getAllFormations(IPuzzle puzzle, int outlierPieceID, int targetPieceID) {
        LinkedList<PuzzleConnection> x = new LinkedList();
        x.add(new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, Vec.zero, 0));
        return x;/*

        LinkedList<PuzzleConnection> allPieceFormations = new LinkedList<PuzzleConnection>();

        PU targetPiece = (PU) puzzle.getPiece(targetPieceID);
        PU outlierPiece = (PU) puzzle.getPiece(outlierPieceID);

        for (Vertex targetVertex : targetPiece.getVertices()) {

            for (Vertex outlierVertex : outlierPiece.getVertices()) {
                Vec v1 = Vec.sub(targetVertex.getPosition(), targetPiece.getPosition());
                Vec v2 = Vec.sub(outlierPiece.getPosition(), outlierVertex.getPosition());

                Vec deltaPos = Vec.add(v1, v2);

                PuzzleConnection connection = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, deltaPos, 0);

                allPieceFormations.add(connection);

            }
        }
        return allPieceFormations;*/
    }
}
