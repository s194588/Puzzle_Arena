package tests.puzzle;

import main.puzzle.util.geometry.lines.IFiniteLine;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.approximation.Approx;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Class and tests by Frederik (s194619)
class EdgeTest {

    private Vec p02 = new Vec(0,2), p12 = new Vec(1,2), p22 = new Vec(2,2), p32 = new Vec(3,2), p43 = new Vec(4,3), p53 = new Vec(5,3);
    private Vec p10 = new Vec(1,0), p20 = new Vec(2,0);

    private TestEdge e1 = new TestEdge(p02,p22), e2 = new TestEdge(p12,p32), e3 = new TestEdge(p22,p32), e4 = new TestEdge(p43,p53);
    private TestEdge e5 = new TestEdge(p22,p20), e6 = new TestEdge(p10,p32);

    private TestEdge reve3 = new TestEdge(p32,p22);
    @Test
    void areParallelTest() {
        assertEquals(Approx.areParallel(e1,e2),true);
        assertEquals(Approx.areParallel(e1,e3),true);
        assertEquals(Approx.areParallel(e2,e3),true);
        assertEquals(Approx.areParallel(e1,e4),true);

        assertEquals(Approx.areParallel(e1, e5),false);
        assertEquals(Approx.areParallel(e1, e6),false);
        assertEquals(Approx.areParallel(e5, e6),false);
        assertEquals(Approx.areParallel(e4,e5),false);
        assertEquals(Approx.areParallel(e4,e6),false);

        assertEquals(Approx.areParallel(e3,e1),true);
        assertEquals(Approx.areParallel(e1,reve3),true);
        assertEquals(Approx.areParallel(reve3,e1),true);
    }

    @Test
    void passesThroughTest() {
    }


    @Test
    void pointDistTest(){
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,p02),0), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,p12),0), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,p22),0), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,p32),1), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,p10),2), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e2,p10),2), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e3,p10),(float)Math.sqrt(1*1+2*2)), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e4,p32),(float)Math.sqrt(2)), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e6,p43),(float)Math.sqrt(2)), true);
    }

    @Test
    void intersectsTest() {
        assertEquals(FiniteLine.intersects(e1,e2), true);
        assertEquals(FiniteLine.intersects(e1,e3), true);
        assertEquals(FiniteLine.intersects(e2,e3), true);
        assertEquals(FiniteLine.intersects(e1, e5), true);
        assertEquals(FiniteLine.intersects(e2, e5), true);
        assertEquals(FiniteLine.intersects(e3, e5), true);
        assertEquals(FiniteLine.intersects(e3, e6), true);
        assertEquals(FiniteLine.intersects(e5, e6), true);

        assertEquals(FiniteLine.intersects(e1, e4), false);
        assertEquals(FiniteLine.intersects(e2, e4), false);
        assertEquals(FiniteLine.intersects(e3, e4), false);
        assertEquals(FiniteLine.intersects(e4, e5), false);
        assertEquals(FiniteLine.intersects(e4, e6), false);

        assertEquals(FiniteLine.intersects(e3,e1), true);
        assertEquals(FiniteLine.intersects(e1,reve3), true);
        assertEquals(FiniteLine.intersects(reve3,e1), true);
    }

    @Test
    void triangleTypeTest()
    {
        Vec vec1 = new Vec(0,0);
        Vec vec2 = new Vec(3,0);
        Vec vec3 = new Vec(0,3);
        Vec vec4 = new Vec(1,3);
        Vec vec5 = new Vec(-6,6);
        Vec vec6 = new Vec(-3,0);

        assertEquals(Vec.triangleType(vec1,vec2,vec3),0);
        assertEquals(Vec.triangleType(vec1,vec2,vec4),-1);
        assertEquals(Vec.triangleType(vec1,vec2,vec5),1);
        assertEquals(Vec.triangleType(vec1,vec2,vec6),1);
    }
    @Test
    void angleTest()
    {
        Vec vec1 = new Vec(1,0);
        Vec vec2 = new Vec(0,1);
        Vec vec3 = new Vec(-1,0);
        //Vec vec3 = new Vec(3,0);
        assertEquals(Approx.areEqual(vec1.angle(),0), true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec1,vec1),0),true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec1,vec2),(float)Math.PI/2f),true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec2,vec1),3f*(float)Math.PI/2f),true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec2,vec1),3f*(float)Math.PI/2f),true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec1,vec3),(float)Math.PI),true);
        assertEquals(Approx.areEqual(Vec.angleBetween(vec3,vec1),(float)Math.PI),true);
    }

    class TestEdge implements IFiniteLine {

        TestEdge(Vec p1, Vec p2){
            this.p1 = p1; this.p2 = p2;
        }
        private Vec p1, p2;
        @Override
        public Vec getP1() {
            return p1;
        }

        @Override
        public Vec getP2() {
            return p2;
        }
    }
}
/*
package Test.puzzle;

import main.puzzle.util.geometry.lines.IFiniteLine;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.approximation.Approx;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EdgeTest {

    IFiniteLine e1 = new TestEdge(new Vec(0,0),new Vec(1,0));
    IFiniteLine e2 = new TestEdge(new Vec(0,1),new Vec(1.2f,1f));
    IFiniteLine e3 = new TestEdge(new Vec(0,0),new Vec(2,2));

    IFiniteLine e4 = new TestEdge(new Vec(0,0),new Vec(2,0));
    IFiniteLine e5 = new TestEdge(new Vec(-10,0),new Vec(10,0));

    @Test
    void areParallelTest() {

        assertEquals(Approx.areParallel(Vec.sub(e1.getP1(), e1.getP2()), Vec.sub(e2.getP1(), e2.getP2())), true);
        assertEquals(Approx.areParallel(Vec.sub(e2.getP1(), e2.getP2()), Vec.sub(e3.getP1(), e3.getP2())), false);
        assertEquals(Approx.areParallel(Vec.zero, Vec.zero), true);
    }

    @Test
    void passesThroughTest() {

        assertEquals(Approx.passesThrough(e1, e3), false);
        assertEquals(Approx.passesThrough(e2, e3), true);
        assertEquals(Approx.passesThrough(e1, e1), false);
    }

    @Test
    void intersectsTest() {

        assertEquals(FiniteLine.tryGetIntersection(e1, e2), null);
        assertEquals(Approx.areEqual(FiniteLine.tryGetIntersection(e1, e3), new Vec(0,0)),true);
        assertEquals(Approx.areEqual(FiniteLine.tryGetIntersection(e2, e3), new Vec(1,1)), true);
        assertEquals(FiniteLine.intersects(e1,e3),true);
        assertEquals(FiniteLine.intersects(e1,e1),true);

        assertEquals(FiniteLine.intersects(e4,e5),true);
    }

    @Test
    void pointDistTest(){
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,new Vec(0,0)),0), true);
        //System.out.println(FiniteLine.pointDist(e1,new Vec(-2,1)));
        //System.out.println(FiniteLine.pointDist(e1,new Vec(-1,0)));
        //System.out.println(Vec.triangleType(e1.getP1(), e1.getP2(),new Vec(-1,0)));
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e1,new Vec(-1,0)),1), true);
        assertEquals(Approx.areEqual(FiniteLine.pointDist(e5,e4.getP1()),0),true);
    }

    @Test
    void triangleTypeTest()
    {
        Vec vec1 = new Vec(0,0);
        Vec vec2 = new Vec(3,0);
        Vec vec3 = new Vec(0,3);
        Vec vec4 = new Vec(1,3);
        Vec vec5 = new Vec(-6,6);
        Vec vec6 = new Vec(-3,0);

        assertEquals(Vec.triangleType(vec1,vec2,vec3),0);
        assertEquals(Vec.triangleType(vec1,vec2,vec4),-1);
        assertEquals(Vec.triangleType(vec1,vec2,vec5),1);
        assertEquals(Vec.triangleType(vec1,vec2,vec6),1);
    }

    class TestEdge implements IFiniteLine {

        TestEdge(Vec p1, Vec p2){
            this.p1 = p1; this.p2 = p2;
        }
        private Vec p1, p2;
        @Override
        public Vec getP1() {
            return p1;
        }

        @Override
        public Vec getP2() {
            return p2;
        }
    }
}
 */