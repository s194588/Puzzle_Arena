package tests.puzzle;

import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

//Class and tests by Frederik (s194619)
class PolygonTest {

    private Vec p05 = new Vec(0,5), p45 = new Vec(4,5);
    private Vec p44 = new Vec(4,4);
    private Vec p43 = new Vec(4,3), p63 = new Vec(6,3), p83 = new Vec(8,3);
    private Vec p02 = new Vec(0,2), p32 = new Vec(3,2), p52 = new Vec(5,2), p62 = new Vec(6,2), p72 = new Vec(7,2), p82 = new Vec(8,2);
    private Vec p30 = new Vec(3,0);

    private Polygon s1 = new Polygon(p05,p45,p02), s2 = new Polygon(p05,p45,p44,p02), s3 = new Polygon(p45, p63, p62, p30, p02), s4 = new Polygon(p44,p52,p32), s5 = new Polygon(p43,p52,p32), s6 = new Polygon(p63,p62,p52), s7 = new Polygon(p83,p72,p62), s8 = new Polygon(p83,p82,p72), s9 = new Polygon(p45,p82,p30);

//    @Test
//    void insideTest() {
//        //assertEquals(s4.hasInside(p44), true);
//        assertEquals(s4.hasInside(p32), true);
//        assertEquals(s4.hasInside(p52), true);
//        assertEquals(s4.hasInside(p43), true);
//        assertEquals(s3.hasInside(p52), true);
//        assertEquals(s3.hasInside(p44), true);
//
//        assertEquals(s4.hasInside(p02), false);
//        assertEquals(s5.hasInside(p02), false);
//        assertEquals(s4.hasInside(p05), false);
//        assertEquals(s4.hasInside(p45), false);
//        assertEquals(s4.hasInside(p63), false);
//    }
//    @Test
//    void overlap(){
//        assertEquals(s3.overlaps(s3), true);
//        assertEquals(s3.overlaps(s5), true);
//        assertEquals(s5.overlaps(s3), true);
//        assertEquals(s4.overlaps(s3), true);
//        assertEquals(s3.overlaps(s4), true);
//        assertEquals(s6.overlaps(s3), true);
//        assertEquals(s3.overlaps(s6), true);
//        assertEquals(s3.overlaps(s7), true);
//        assertEquals(s7.overlaps(s3), true);
//        assertEquals(s1.overlaps(s3), true);
//        assertEquals(s3.overlaps(s1), true);
//        assertEquals(s5.overlaps(s6), true);
//        assertEquals(s6.overlaps(s5), true);
//        assertEquals(s9.overlaps(s3), true);
//        assertEquals(s3.overlaps(s9), true);
//        assertEquals(s9.overlaps(s6), true);
//        assertEquals(s6.overlaps(s9), true);
//        assertEquals(s9.overlaps(s7), true);
//        assertEquals(s7.overlaps(s9), true);
//        assertEquals(s9.overlaps(s8), true);
//        assertEquals(s8.overlaps(s9), true);
//        assertEquals(s1.overlaps(s2), true);
//        assertEquals(s2.overlaps(s1), true);
//        assertEquals(s2.overlaps(s4), true);
//        assertEquals(s4.overlaps(s2), true);
//
//
//        assertEquals(s1.overlaps(s6), false);
//        assertEquals(s6.overlaps(s1), false);
//        assertEquals(s5.overlaps(s7), false);
//        assertEquals(s7.overlaps(s5), false);
//        assertEquals(s9.overlaps(s5), false);
//        assertEquals(s5.overlaps(s9), false);
//        assertEquals(s2.overlaps(s5), false);
//        assertEquals(s5.overlaps(s2), false);
//    }

//    @Test
//    void insideOverlap(){
//        assertEquals(s2.insideOverlaps(s2), true);
//        assertEquals(s1.insideOverlaps(s2), true);
//        assertEquals(s2.insideOverlaps(s1), true);
//        assertEquals(s4.insideOverlaps(s5), true);
//        assertEquals(s5.insideOverlaps(s4), true);
//        assertEquals(s3.insideOverlaps(s4), true);
//        assertEquals(s4.insideOverlaps(s3), true);
//        assertEquals(s6.insideOverlaps(s3), true);
//        assertEquals(s3.insideOverlaps(s6), true);
//        assertEquals(s2.insideOverlaps(s3), true);
//        assertEquals(s3.insideOverlaps(s2), true);
//
//        assertEquals(s3.insideOverlaps(s7), false);
//        assertEquals(s7.insideOverlaps(s3), false);
//        assertEquals(s1.insideOverlaps(s3), false);
//        assertEquals(s3.insideOverlaps(s1), false);
//        assertEquals(s3.insideOverlaps(s9), false);
//        assertEquals(s9.insideOverlaps(s3), false);
//        assertEquals(s3.insideOverlaps(s8), false);
//        assertEquals(s8.insideOverlaps(s3), false);
//        assertEquals(s5.insideOverlaps(s8), false);
//        assertEquals(s8.insideOverlaps(s5), false);
//    }

    @Test
    void boundingBox(){
    }


    @Test
    void simplifyEdges(){
        Vertex v1 = new Vertex(0,0), v2 = new Vertex(2,0), v3 = new Vertex(2,2),  v4 = new Vertex(0, 2);
        Vertex vex1 = new Vertex(1,0), vex2 = new Vertex(1.5f,0), vex3 = new Vertex(1.7f,0.2f), vex4 = new Vertex(1.8f,0), vex5 = new Vertex(1.9f,0);
        Polygon x = new Polygon(v1,v2,v3,v4);
        Polygon y = new Polygon(v1,vex1, v2,v3,v4);
        Polygon z = new Polygon(v1, vex1, vex2, v2,v3,v4);
        Polygon w = new Polygon(v1, vex1, vex2, vex3, v2,v3,v4);
        Polygon s = new Polygon(v1, vex1, vex2, vex3, vex4, v2,v3,v4);
        Polygon t = new Polygon(v1, vex1, vex2, vex3, vex4, v2,v3,v4);
        assertEquals(x.simplifyEdges().size(),4);
        assertEquals(y.simplifyEdges().size(),4);
        assertEquals(z.simplifyEdges().size(),4);
        assertEquals(w.simplifyEdges().size(),6);
        assertEquals(s.simplifyEdges().size(),7);
        assertEquals(t.simplifyEdges().size(),7);
    }

    @Test
    void isIdenticalTest() {
        Vertex v1 = new Vertex(0,0), v2 = new Vertex(1,1), v3 = new Vertex(2,0), v4 = new Vertex(5,0);
        Polygon x = new Polygon(v1,v2,v3);
        Polygon y = new Polygon(v2,v1,v3);
        Polygon z = new Polygon(v1,v2,v4);
        Matrix2 rot = Matrix2.rotMatrix(2);
        Polygon w = new Polygon(new Vertex(Matrix2.MVProd(rot,v2.getPosition())),new Vertex(Matrix2.MVProd(rot,v1.getPosition())),new Vertex(Matrix2.MVProd(rot,v3.getPosition())));
        assertEquals(x.areIdentical(x),true);
        assertEquals(x.areIdentical(y),true);
        assertEquals(x.areIdentical(z),false);
        assertEquals(x.areIdentical(w),true);
        assertEquals(y.areIdentical(w),true);
        assertEquals(z.areIdentical(w),false);

    }

}

/*
package Test.puzzle;

import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {

    @Test
    void isIdenticalTest() {
        Vertex v1 = new Vertex(0,0), v2 = new Vertex(1,1), v3 = new Vertex(2,0), v4 = new Vertex(5,0);
        Polygon x = new Polygon(v1,v2,v3);
        Polygon y = new Polygon(v2,v1,v3);
        Polygon z = new Polygon(v1,v2,v4);
        Matrix2 rot = Matrix2.rotMatrix(2);
        Polygon w = new Polygon(new Vertex(Matrix2.MVProd(rot,v2.getPosition())),new Vertex(Matrix2.MVProd(rot,v1.getPosition())),new Vertex(Matrix2.MVProd(rot,v3.getPosition())));
        assertEquals(Polygon.areIdentical(x,x),true);
        assertEquals(Polygon.areIdentical(x,y),true);
        assertEquals(Polygon.areIdentical(x,z),false);
        assertEquals(Polygon.areIdentical(x,w),true);
        assertEquals(Polygon.areIdentical(y,w),true);
        assertEquals(Polygon.areIdentical(z,w),false);

    }

    @Test
    void insideTest() {
        Vertex v1 = new Vertex(0,0), v2 = new Vertex(1,1), v3 = new Vertex(2,0), v4 = new Vertex(5,0);
        Polygon x = new Polygon(v1,v2,v3);
        //assertEquals(x.inside(v1),true);
        //assertEquals(x.inside(v2),true);
        //assertEquals(x.inside(v3),true);
        assertEquals(x.hasInside(v4),false);
        assertEquals(x.hasInside(new Vertex(1.1f,1f)),false);
        assertEquals(x.hasInside(new Vertex(1f,0.5f)),true);
        //assertEquals(x.hasInside(new Vertex(1,0)),true);
        assertEquals(x.hasInside(new Vertex(0,0)),true);
        assertEquals(o2.hasInside(ov7), false);
    }


    Vertex ov1 = new Vertex(0,0), ov2 = new Vertex(0,1), ov3 = new Vertex(1,0), ov4 = new Vertex(1,1), ov5 = new Vertex(-1,1), ov6 = new Vertex(-1,3), ov7 = new Vertex(-2,0);
    Polygon o1 = new Polygon(ov1,ov2,ov3);
    Polygon o2 = new Polygon(ov2,ov3,ov4);
    Polygon o3 = new Polygon(ov1,ov3,ov4);
    Polygon o4 = new Polygon(ov7,ov5,ov6);

    @Test
    void overlap(){
        //assertEquals(o1.overlaps(o2), true);
        //assertEquals(o3.overlaps(o1), true);
        assertEquals(o2.overlaps(o4),false);
    }

    @Test
    void insideOverlap(){
        assertEquals(o1.insideOverlaps(o1), true);
        assertEquals(o1.insideOverlaps(o2), false);
        assertEquals(o3.insideOverlaps(o1), true);
        assertEquals(o2.insideOverlaps(o4),false);
    }
    @Test
    void boundingBox(){

        Polygon w = new Polygon(new Vertex(0,0), new Vertex(0,1), new Vertex(1,1), new Vertex(1,0));
        Polygon q = new Polygon(new Vertex(0,1), new Vertex(0,2), new Vertex(1,2), new Vertex(1,1));
        List<Polygon> l = new LinkedList<>();
        l.add(w);
        Polygon b = Polygon.boundingBox(l);
        assertEquals(Approx.areEqual(Vec.average(b.getVerticesAsVecs()), new Vec(0.5f,0.5f)), true);
        assertEquals(Approx.areEqual(Vec.average(b.getVerticesAsVecs()), Vec.average(w.getVerticesAsVecs())), true);
        l.add(q);
        Polygon g = Polygon.boundingBox(l);
        System.out.println(Vec.average(g.getVerticesAsVecs()));
        System.out.println(Vec.average(g.getVerticesAsVecs()));
        System.out.println(Vec.average(g.getVerticesAsVecs()));
        System.out.println(Vec.average(g.getVerticesAsVecs()));
        assertEquals(Approx.areEqual(Vec.average(g.getVerticesAsVecs()), new Vec(0.5f,1f)), true);
    }

}
 */