package tests.puzzle;

import main.puzzle.util.general.listandarrays.ListAndArrayUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Class and tests by Frederik (s194619)
class UtilTest {

    @Test
    void hasAlignmentTest(){
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[0], new Float[0]),true);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{1f}, new Float[0]),true);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[0], new Float[]{1f}),false);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{1f,2f}, new Float[]{2f,1f,3f}),false);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{1f,3f}, new Float[]{2f,1f,3f}),false);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{2f,1f,3f}, new Float[]{1f,3f}),true);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{3f,2f,1f}, new Float[]{3f,2f,1f}),true);
        assertEquals(ListAndArrayUtil.hasAlignment(new Float[]{2f,1f,3f}, new Float[]{3f,2f,1f}),true);
    }


}