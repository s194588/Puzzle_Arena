package main.gui;

import javafx.scene.Group;
import javafx.scene.paint.Paint;
import main.puzzle.util.interfaces.IObserver;
import main.puzzle.logic.IFigure;
import main.puzzle.logic.IPuzzle;
import javafx.scene.shape.Polygon;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.util.geometry.linearalgebra.Vec;


//Class code by Frederik (s194619)
public class PuzzleView extends Group implements IObserver {

    public static boolean enableObserve = true;
    public IFigure fig;

    private int pieceID;
    private IPuzzle puzzle;
    private Polygon polygon;

    public PuzzleView(IPuzzle puzzle, int id, Paint paint, Paint stroke) {
        this.puzzle = puzzle;
        this.fig = puzzle.getPiece(id);
        this.polygon = new Polygon();

        polygon.setStroke(stroke);
        polygon.setStrokeWidth(1);

        pieceID = id;
        redraw();

        //Puts image on polygon
        polygon.setFill(paint);
        getChildren().add(polygon);
        ((PuzzlePiece)fig).addObserver(this);

    }

    /**
     * Redraws the objects according to the puzzle piece in the puzzle that this draws.
     */
    public void redraw(){
        polygon.getPoints().clear();
        for (int i = 0; i < fig.getVertices().size(); i++) {
            Vec pos = fig.getVertices().get(i).getPosition();
            polygon.getPoints().addAll((double) pos.x * GameScene.scaleConstant + GameScene.windowX/2f,(double)-pos.y * GameScene.scaleConstant + GameScene.windowY/2f);
        }
    }

    public int getPieceID() {
        return pieceID;
    }


    private static Object lock = new Object();

    /**
     * Runs when the observed object has notified this.
     */
    @Override
    public void update() {
        synchronized (lock) {
            if (enableObserve)
                redraw();
        }
    }
}
