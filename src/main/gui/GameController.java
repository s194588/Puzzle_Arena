package main.gui;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.general.GeneralUtil;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class GameController {

    boolean solvedShown = false;
    boolean snapEnabled = true;

    public PuzzleView puzzleView;
    private Vec offSet = Vec.zero;
    private boolean dragging;
    private Group group;
    private IPuzzle puzzle;
    private Scale scaleTransform;
    private final double scaleValue = 0.02;
    public static int scaleRestrictionCounter = 0;
    private static final int scaleRestriction = 50;

    public void setPuzzle(IPuzzle puzzle) {
        this.puzzle = puzzle;
    }

    public void setGroup(Group group) {
        this.group = group;
    };

    Audiohandler button = new Audiohandler();

    // convert position on the screen into actual position on the scene
    // Frederik (s194619)
    public Vec screenToWorld(Vec screenCoords) {
        return new Vec((screenCoords.x - GameScene.windowX / (2f)) / GameScene.scaleConstant,
                -(screenCoords.y - GameScene.windowY/ (2f)) / GameScene.scaleConstant);
    }

    // Jonas (s194599)
    public EventHandler<MouseEvent> mousePressed = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {

            // deselect selected puzzle piece if any is selected
            if (puzzleView != null) {
                puzzleView.setEffect(null);
            }

            // select puzzle piece pressed on with mouse
            puzzleView = (PuzzleView) mouseEvent.getSource();

            // convert mouse position to correct position
            Vec mousePos = screenToWorld(new Vec((float) mouseEvent.getSceneX(), (float) mouseEvent.getSceneY()));
            offSet = Vec.sub(puzzleView.fig.getPosition(),mousePos);
            dragging = true;

            // highlight selected puzzle piece
            Glow glow = new Glow();
            glow.setLevel(0.4);
            puzzleView.setEffect(glow);
        }
    };

    // Jonas (s194599)
    // deselect selected puzzle piece if user mouse pressed the background
    public EventHandler<MouseEvent> mousePressedBackground = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {

            if (puzzleView != null) {
                puzzleView.setEffect(null);
            }

            puzzleView = null;
        }
    };

    // Jonas (s194599) and Frederik (s194619)
    public EventHandler<MouseEvent> mouseReleased = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {

            dragging = false;
            if (puzzleView != null) {
//                System.out.println("Position: " + pv.fig.getPosition());

                // calibrate puzzle piece position
                Vec mouseScreenPos = new Vec(GeneralUtil.clamp((float) mouseEvent.getSceneX(), 0, GameScene.windowX),
                        GeneralUtil.clamp((float) mouseEvent.getSceneY(), 0, GameScene.windowY));
                puzzleView.fig.setPosition(Vec.add(offSet, screenToWorld(mouseScreenPos)));
            }
            /*if (!solvedShown) {
                if (puzzle.isSolved()) {
                    try {

                        GameScene.greenStroke();

                        // Open a "puzzle solved" window
                        URL url = JFXScene.class.getResource("/resources/fxml/SolvedMenu.fxml");
                        Parent root = (Parent) FXMLLoader.load(url);
                        Scene solvedScene = new Scene(root, 600, 400);
                        Stage stage = new Stage();
                        stage.getIcons().add(new Image("/resources/images/jigsaw.png"));
                        stage.setTitle("PuzzleArena");
                        stage.setResizable(false);
                        stage.setScene(solvedScene);
                        stage.show();
                        solvedShown = true;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }*/
            
            Vec mousePos = screenToWorld(new Vec((float) mouseEvent.getSceneX(), (float) mouseEvent.getSceneY()));

            // snap to nearby puzzle piece
            if (snapEnabled) {
                snapToPlace(puzzleView.getPieceID(), mousePos);
            }



            button.playAudio("sounds/soundEffects/Button-click.mp3");

            checkSolved(!solvedShown);
        }
    };

    // Jonas (s194599)
    public void checkSolved(boolean showPopUp)
    {
        if (!puzzle.isSolved()){
            GameScene.redStroke();

        }
        else{
            GameScene.greenStroke();
            if (showPopUp){
                try {
                    //URL url = JFXScene.class.getResource("/resources/fxml/SolvedMenu.fxml");
                    URL url = ResourceLoader.getURL("fxml/SolvedMenu.fxml");
                    Parent root = (Parent) FXMLLoader.load(url);
                    Scene solvedScene = new Scene(root, 600, 400);
                    Stage stage = new Stage();
                    stage.getIcons().add(ResourceLoader.loadImage("images/jigsaw.png"));
                    stage.setTitle("PuzzleArena");
                    stage.setResizable(false);
                    stage.setScene(solvedScene);
                    stage.show();
                    solvedShown = true;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Jonas (s194599)
    public void snapToPlace(int pieceID, Vec mousePos){

        PuzzlePiece pp1 = puzzle.getPiece(pieceID);

        // find a nearby puzzle piece
        for (int i=0; i< puzzle.getPieceCount(); i++){
            PuzzlePiece pp2 = puzzle.getPiece(i);
            float distance = Vec.sub(pp1.getPosition(),pp2.getPosition()).length();

            // Check if close distance between pieces is small enough
    
            if (i!=pieceID && distance < 3f*(pp2.maxRadius()+pp1.maxRadius())){

                List<PuzzleConnection> connections = GameScene.cachingCalculator.getConnections(pieceID, i);
                // if there are any connections between the pieces:
                if (!connections.isEmpty()){
                    Vec[] connectionVecs = new Vec[connections.size()];

                    // get connection positions
                    for (int j=0; j< connectionVecs.length; j++){
                        connectionVecs[j] = connections.get(j).resultingPos();//Previously: Vec.add(connections.get(j).deltaPos, pp2.getPosition());
                    }

                    // Get closest connection
                    Vec closest = Vec.closestVec(pp1.getPosition(), connectionVecs);
                    int index=0;
                    for (int k=0; k<connectionVecs.length; k++){
                        if (closest.equals(connectionVecs[k])){
                            index = k;
                        }
                    }
                    PuzzleConnection connection = connections.get(index);

                    // Check if close enough to connection
                    if (Vec.sub(pp1.getPosition(),closest).length() < 0.3f*pp1.maxRadius() ){

                        // snap to place
                        connection.tryApply();
                        break;

                    }
                }
            }
        }
    }

    // Jonas (s194599) and Oliver (s194588)
    public EventHandler<MouseEvent> mouseDrag = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {

            if (puzzleView != null && dragging) {

                // move puzzle piece along mouse when dragging
                Vec mousePos = screenToWorld(new Vec((float) mouseEvent.getSceneX(), (float) mouseEvent.getSceneY()));
                puzzleView.fig.setPosition(Vec.add(mousePos,offSet));

                Glow glow = new Glow();
                glow.setLevel(0.4);
                puzzleView.setEffect(glow);
            }
        }
    };


    float scale = 1;
    // Key events
    // Jonas (s194599), Christian (s194597) and Oliver (s194588)
    public EventHandler<KeyEvent> arrowClick = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent keyEvent) {

            // zoom in
            if (keyEvent.getCode() == KeyCode.I) {
                if (scaleTransform == null) {
                    scaleTransform = new Scale();
                    scaleTransform.setPivotX(Main.windowX/2d);
                    scaleTransform.setPivotY(Main.windowY/2d);
                    group.getTransforms().add(scaleTransform);
                }
                    //scaleTransform.get
                scale = GeneralUtil.clamp(scale + 0.2f, 1, 4);
                    scaleTransform.setX(scale);
                    scaleTransform.setY(scale);

                button.playAudio("sounds/soundEffects/Click.mp3");
            }

            // zoom out
            if (keyEvent.getCode() == KeyCode.O) {
                    if (scaleTransform == null) {
                        scaleTransform = new Scale();
                        scaleTransform.setPivotX(Main.windowX/2d);
                        scaleTransform.setPivotY(Main.windowY/2d);
                        group.getTransforms().add(scaleTransform);
                    }
                    //scaleTransform.get
                    scale = GeneralUtil.clamp(scale - 0.2f, 1, 4);
                    scaleTransform.setX(scale);
                    scaleTransform.setY(scale);
                    button.playAudio("sounds/soundEffects/Click.mp3");
            }

//            if (keyEvent.getCode() == KeyCode.K) {
//                    scaleTransform = new Scale(1, 1);
//                    group.getTransforms().add(scaleTransform);
//                    button.playAudio("src/resources/sounds/soundEffects/Click.mp3");
//                    scaleRestrictionCounter++;
//            }


            // rotate selected puzzle piece
            if (puzzleView != null) {

                // rotate slowly
                if (keyEvent.getCode() == KeyCode.RIGHT) {
                    puzzleView.fig.setAngle((float) (puzzleView.fig.getAngle() - (3.14 / 360)));
                    button.playAudio("sounds/soundEffects/Button-click.mp3");
                }

                if (keyEvent.getCode() == KeyCode.LEFT) {
                    puzzleView.fig.setAngle((float) (puzzleView.fig.getAngle() + (3.14 / 360)));
                    button.playAudio("sounds/soundEffects/Button-click.mp3");

                }
                // rotate faster
                if (keyEvent.getCode() == KeyCode.UP) {
                    puzzleView.fig.setAngle((float) (puzzleView.fig.getAngle() - (3.14 / 360) * 30));
                    button.playAudio("sounds/soundEffects/Button-click.mp3");

                }
                if (keyEvent.getCode() == KeyCode.DOWN) {
                    puzzleView.fig.setAngle((float) (puzzleView.fig.getAngle() + (3.14 / 360) * 30));
                    button.playAudio("sounds/soundEffects/Button-click.mp3");

                }
            }
        }
    };

    // Jonas (s194599) and Oliver (s194588)
    // return to main menu event
    public void returnEvent(Button btnReturn){

        btnReturn.setOnAction(actionEvent -> {

            GameScene.stopTimer();

            button.playAudio("sounds/music/Button-click.mp3");
            Scene SMScene2 = null;
            try {
                SMScene2 = JFXScene.loadFXML("mainMenu.fxml");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        Main.changeScene(SMScene2);
        });
    }

    // Jonas (s194599)
    // alert if any pieces are identical
    public void identicalEvent(Button btnIdentical){
        btnIdentical.setOnAction(actionEvent ->  {
                button.playAudio("sounds/soundEffects/Scramble.mp3");
                Alert a;


                if (puzzle.containsIdenticalPieces()) {
                    a = new Alert(Alert.AlertType.WARNING);
                    a.setContentText("At least two pieces are identical!");

                } else {
                    a = new Alert(Alert.AlertType.CONFIRMATION);
                    a.setContentText("No pieces are identical");
                }
                a.show();

        });
    }
    // Jonas (s194599)
    public void muteEvent(ToggleButton tbMute){
        tbMute.setOnAction(actionEvent ->  {
            if (tbMute.isSelected()) {
                button.playAudio("sounds/music/Button-click.mp3");
                Main.music.pauseAudio();
            } else {
                button.playAudio("sounds/music/Button-click.mp3");
                Main.music.unPauseAudio();
            }

        });
    }
    // Jonas (s194599)
    // enable snap to place
    public void snapEvent(ToggleButton tbSnap){
        tbSnap.setOnAction(actionEvent ->  {
            if (tbSnap.isSelected()) {
                button.playAudio("sounds/music/Button-click.mp3");
                snapEnabled = true;
            } else {
                button.playAudio("sounds/music/Button-click.mp3");
                snapEnabled = false;
            }

        });
    }

    public void solveEvent(Button btnSolve, ISolvingAlgorithm solvingAlgorithm){
        btnSolve.setOnAction(actionEvent ->  {
            long startMillis = System.currentTimeMillis();
            boolean output = solvingAlgorithm.solve(puzzle);
            long endMillis = System.currentTimeMillis();
            long deltaMillis = (endMillis - startMillis);
            //System.out.println("Took " + deltaMillis + "ms (" + deltaMillis / 1000f + "s) to run the solving algorithm.");
            float score = 4f * ((float) deltaMillis) / (puzzle.getPieceCount() * puzzle.getPieceCount());
            float t1 = 1, t2 = 2, t3 = 3, t4 = 4, t5 = 5;
            //System.out.println("Score: " + (score < t1 ? "(ﾉ◕ヮ◕)"
            //        : score < t2 ? ":D" : score < t3 ? ":)" : score < t4 ? ":|" : score < t5 ? ":(" : ">:("));
            System.out.println("Solver output: " + output);
            /*
             * boolean b = false; for (int i = 0; i < puzzle.getPieceCount(); i++) { for
             * (int j = 0; j < puzzle.getPieceCount(); j++) { if
             * (puzzle.getPiece(i).insideOverlaps(puzzle.getPiece(j))) { b = true; } } }
             */
            //System.out.println("Status -> Solved: " + puzzle.isSolved());
            button.playAudio("sounds/soundEffects/algo.mp3");
            checkSolved(false);

        });
    }




}
