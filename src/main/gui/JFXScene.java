package main.gui;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.net.URI;

public abstract class JFXScene {
    //private static final String FXML_PATH = "/resources/fxml/";

    public abstract Scene onLoad() throws IOException;

    // Jonas (s194599)
    // create scene with fxml
    protected static Scene loadFXML(String fileName) throws IOException {
        //URL url = JFXScene.class.getResource(FXML_PATH  + fileName);
        //Parent root = FXMLLoader.load(url);
        Parent root = null;
        try {
            root = FXMLLoader.load((new URI(ResourceLoader.getPathStringFile() + "fxml/" + fileName)).toURL());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        Scene scene = new Scene(root,1280,720);

        return scene;
    }

    //protected static

}
