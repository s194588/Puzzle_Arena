package main.gui;

import javafx.scene.image.Image;
import javafx.scene.media.Media;

import java.io.File;
import java.net.URI;
import java.net.URL;

//Class by Frederik (s194619)
public class ResourceLoader {

    private static String RESOURCE_PATH = "file:/C:/Users/frede/IdeaProjects/Puzzle_Arena/src/resources/";//"file:/C:/Users/frede/IdeaProjects/resources/";

    public static Media loadMedia(String path){
        //System.out.println(getURL(path).toString());
        //return new Media(getURL(path).toString());
        //System.out.println(new File(getURL("").toString()).getParentFile().toString());
        //System.out.println(getPathString());
        //System.out.println(getPathString() + path);
        //System.out.println(getURL(path).toString());
        return new Media(getPathStringFile()+path);
        //return new Media(getURL(path).toString());//getPathString() + path);
    }
    public static Image loadImage(String path){
        return new Image(getPathStringFile()+path);
    }

    public static URL getURL(String path){
        URL ret = null;
        try{
            ret = (new URI(getPathStringFile() + path)).toURL();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return ret;
    }
    public static String getPathStringFile()
    {
        //File f = new File(ResourceLoader.class.getResource("").toString()).getParentFile().getParentFile().getParentFile().getParentFile().getParentFile().getParentFile();
        File f = new File(ResourceLoader.class.getResource("").toString()).getParentFile().getParentFile();

        String ret = null;
        try {
            ret = f.toString();
            ret = ret.replace('\\','/');
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        //System.out.println("xd" + ret);
        //System.out.println("xd2" + RESOURCE_PATH);
        return ret + "/resources/"; //RESOURCE_PATH;
                //File().toString()).getParentFile().getParentFile().getParentFile().toString() + RESOURCE_PATH;
    }
    public static String getPathString()
    {
        return getPathStringFile().replaceFirst("file:/","");
    }
}
