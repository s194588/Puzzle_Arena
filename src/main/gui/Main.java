package main.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    public static Stage window;
    static Audiohandler music = new Audiohandler();
    public static int windowX = 1280, windowY = 720;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    // Jonas (s194599)
    public void start(Stage primaryStage) throws Exception {
        System.out.println("RESOURCE PATH: " + ResourceLoader.getPathString());
        music.playAudio("sounds/music/jazz_bg_m.mp3");
        //music.playAudio("src/resources/sounds/music/medieval.mp3");
        //primaryStage.setFullScreen(true);
        // open start menu scene
        createWindow(primaryStage);

        Scene SMScene = JFXScene.loadFXML("startMenu.fxml");
        changeScene(SMScene);
        //playMusic("src/resources/sounds/music/curve fever pro soundtrack #1.mp3");
        //Image image = new Image("https://i.imgur.com/1jwlaws.png");

    }

    // Jonas (s194599)
    public void createWindow(Stage primaryStage) {
        // Set scene color
        window = primaryStage;
        window.getIcons().add(ResourceLoader.loadImage("images/jigsaw.png"));
        primaryStage.setTitle("PuzzleArena");
        window.setResizable(false);

    }

    // Jonas (s194599)
    protected static void changeScene(Scene scene) {
        window.setScene(scene);
        window.show();
    }

}
