package main.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Scene;

//class code by Jonas (s194599)
public class StartController {

    Audiohandler button = new Audiohandler();

    // Start menu controls
    public void Start(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");
        Scene MMScene = JFXScene.loadFXML("mainMenu.fxml");
        Main.changeScene(MMScene);
    }

    public void ControlsMenu(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");
        Scene CScene = JFXScene.loadFXML("ControlsMenu.fxml");
        Main.changeScene(CScene);
        /*        CScene.addEventFilter(KeyEvent.KEY_PRESSED, arrowClick2);*/
    }

    public void ExitGame(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");
        Platform.exit();
    }


    //This button is in the controls menu
    public void BacktoMenu(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");
        Scene SMScene2 = JFXScene.loadFXML("startMenu.fxml");
        Main.changeScene(SMScene2);
    }
}
