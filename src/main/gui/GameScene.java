package main.gui;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;
import main.puzzle.dataloading.IPuzzleLoader;
import main.puzzle.dataloading.JSONPuzzleLoader;
import main.puzzle.logic.IFigure;
import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.Puzzle;
import main.puzzle.solving.algorithms.*;
import main.puzzle.solving.algorithms.strategies.PriorityStrategy;
import main.puzzle.solving.algorithms.skeleton.PuzzleBacktrackSolver;
import main.puzzle.solving.connecting.calculators.*;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.io.IOException;
import java.util.*;


//Class code by Jonas (s194599), Oliver (s194588), Christian (s194597) and Frederik (s194619)
public class GameScene extends JFXScene {

    public static String timerString;
    public static final int controlsSizeY = 48;
    int seconds;
    int minutes;
    public static int windowX = Main.windowX, windowY = Main.windowY;
    public static float scaleConstant = Main.windowX / 8;
    public static List<Vertex> form;
    public static Polygon puzzleForm;

    public static Timer timer;
    public static TimerTask timerTask;
    public static List<PuzzleView> puzzleViewList = new ArrayList<>();

    public static CachingCalculator cachingCalculator;

    public static Paint piecePaint;
    public static Rectangle background;
    public static Paint bgPaint;

    GameController gameController = new GameController();
    Audiohandler button = new Audiohandler();

    @Override
    public Scene onLoad() throws IOException {

        GameController.scaleRestrictionCounter = 0;

        //In-game screen layout
        Group root = new Group();
        HBox navbar = new HBox(10);
        Group zoomGroup = new Group();
        Group contentPane = new Group();
        Scene scene = new Scene(root, windowX, windowY);

        //Manual spacing between certain buttons
        HBox spacing2 = new HBox();


        String puzzleSetPath = MainController.puzzleSetPath;


        //Layout of different scenes
        navbar.setMinSize(windowX/2+165, controlsSizeY);
  /*      gameRoot.setMinSize(windowX,windowY);
        gameRoot.setLayoutY(controlsSizeY);*/

        //Manual setting space between buttons
        spacing2.setMinWidth(5);

        zoomGroup.autosize();

        //Scroll settings

   /*     game.setPrefSize(windowX,windowY);

        game.fitToWidthProperty().set(true);
        game.fitToHeightProperty().set(true);*/

//        game.setOnKeyPressed(event-> {
//            if(event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT){
//               System.out.println("Consumed");
//                event.consume();
//            }
//        });

        puzzleForm = new Polygon();
        //Background
        background = new Rectangle(windowX,windowY);
//        background.setY(controlsSizeY);
        zoomGroup.getChildren().add(background);


        //Theme variables - Oliver (s194588)
        piecePaint = Color.GREEN;
        Paint formPaint = Color.BLUE;
        bgPaint = Color.GREY;
        Paint strokePaint = Color.BLACK;
        Image image;
        float scaling;
        //Puzzle form effect
        puzzleForm.setStroke(Color.RED);
        puzzleForm.setStrokeWidth(4);
        zoomGroup.getChildren().add(puzzleForm);

        IPuzzleLoader loader = new JSONPuzzleLoader(puzzleSetPath);

        IPuzzle puzzle = new Puzzle(loader);

        scaleConstant = calculateIdealScaleConstant(puzzle);

        gameController.setPuzzle(puzzle);
        zoomGroup.prefWidth(windowX);
        zoomGroup.prefHeight(windowY);
        contentPane.prefHeight(windowX);
        contentPane.prefWidth(windowY);
        gameController.setGroup(zoomGroup);

        String theme = MainController.theme;
        // puzzle.getPieceFigure(1).setPosition(new Vec(20f, 20f));

        //Creates the theme that is chosen - Oliver (s194588)
        switch (theme) {
            case "Classic":
                piecePaint = Color.TEAL;
                formPaint = Color.DARKORANGE;
                bgPaint = Color.ORANGE;
                break;
            case "Art":
                image = ResourceLoader.loadImage("images/art.png");
                scaling = (float) Math.max(windowX/image.getWidth(), (windowY)/image.getHeight());
                piecePaint = new ImagePattern(image, 0,0, image.getWidth()*scaling, image.getHeight()*scaling, false);
                bgPaint = Color.MOCCASIN;
                formPaint = Color.ROYALBLUE;
                break;
            case "Nyan":
                formPaint = Color.DEEPPINK;
                bgPaint = Color.CYAN;
                strokePaint = Color.AQUAMARINE;
                image = ResourceLoader.loadImage("gifs/nyancat.gif");
                scaling = (float) Math.max(windowX/image.getWidth(), (windowY)/image.getHeight());
                piecePaint = new ImagePattern(image, 0,0, image.getWidth()*scaling, image.getHeight()*scaling, false);
                break;
            case "Chinese":
                formPaint = Color.GOLDENROD;
                bgPaint = Color.FIREBRICK;
                strokePaint = Color.BLACK;
                image = ResourceLoader.loadImage("images/wudragon.jpg");
                scaling = (float) Math.max(windowX/image.getWidth(), windowY/image.getHeight());
                piecePaint = new ImagePattern(image, 0,0, image.getWidth()*scaling, image.getHeight()*scaling, false);
                bgPaint = Color.KHAKI;
                break;
            case "Fade":
                formPaint = Color.BLACK;
                bgPaint = Color.ROYALBLUE;
                strokePaint = Color.BLACK;
                image = ResourceLoader.loadImage("images/fade.jpeg");
                scaling = (float) Math.max(windowX/image.getWidth(), windowY/image.getHeight());
                piecePaint = new ImagePattern(image, 20,0, image.getWidth()*scaling, image.getHeight()*scaling, false);
                break;
        }
        //background and form color - Oliver (s194588)
        navbar.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        puzzleForm.setFill(formPaint);
        background.setFill(bgPaint);
        background.addEventFilter(MouseEvent.MOUSE_PRESSED, gameController.mousePressedBackground);
        puzzleForm.addEventFilter(MouseEvent.MOUSE_PRESSED, gameController.mousePressedBackground);


        Vec average = Vec.average(Vertex.toVecList(puzzle.getForm().getVertices()));

        // Loads form
        for (Vertex v : puzzle.getForm().getVertices()) {
            Vec vec = Vec.mul(Vec.sub(v.getPosition(), average), scaleConstant);

            puzzleForm.getPoints().addAll((double) (vec.x + windowX / 2f), (double) (vec.y + windowY / 2f));
        }

        // Loads the individual puzzle pieces
        for (int i = 0; i < puzzle.getPieceCount(); i++) {
            PuzzleView puzzleView = new PuzzleView(puzzle, i, piecePaint, strokePaint);
            puzzleViewList.add(puzzleView);

            // add mouse events to puzzle pieces
            puzzleView.addEventFilter(MouseEvent.MOUSE_PRESSED, gameController.mousePressed);
            puzzleView.addEventFilter(MouseEvent.MOUSE_RELEASED, gameController.mouseReleased);
            puzzleView.addEventFilter(MouseEvent.MOUSE_DRAGGED, gameController.mouseDrag);
            zoomGroup.getChildren().add(puzzleView);
        }

        //Manuel styling of timer
        Background ingame = new Background(new BackgroundFill(Color.DARKCYAN, new CornerRadii(5.0), new Insets(-5.0)));

        // Timer - Jonas (s194599)
        Label lblTimer = new Label();
        controlLayout(lblTimer, 1);
        lblTimer.setBackground(ingame);
        seconds = 0;
        minutes = 0;

        // update time spent label every second
        timer = new Timer(true);
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        seconds++;
                        if (seconds == 60) {
                            seconds = 0;
                            minutes++;
                        }
                        if (seconds < 10 && minutes < 10) {
                            timerString = "0" + minutes + ":0" + seconds;
                        } else if (seconds < 10) {
                            timerString = minutes + ":0" + seconds;
                        } else if (minutes < 10) {
                            timerString = "0" + minutes + ":" + seconds;
                        } else {
                            timerString = minutes + ":" + seconds;
                        }
                        lblTimer.setText(timerString);

                    }
                });
            }
        };

        timer.scheduleAtFixedRate(timerTask, 0, 1000);

        //Chooses which algorithm you want to use
        IConnectionCalculator calc = null;
        if (MainController.Algo.equals("Classic")) {
            calc = new ClassicCalculator(puzzle);
            System.out.println("Using ClassicCalculator");
        }else{
            calc = new PolygonCalculator(puzzle);
            System.out.println("Using PolygonCalculator");
        }
            PuzzleView.enableObserve = false;
            cachingCalculator = new CachingCalculator(calc);
            cachingCalculator.initCache();
            ISolvingAlgorithm solvingAlgorithm = new PuzzleBacktrackSolver(new PriorityStrategy(cachingCalculator));//new PrioritySolver(calculator);
            PuzzleView.enableObserve = true;



        // Creation of in-game buttons - Jonas (s194599) and Oliver (s194588)
        Button btnSolve = new Button("SOLVE");
        controlLayout(btnSolve,1);
        gameController.solveEvent(btnSolve, solvingAlgorithm);

        Button btnIdentical = new Button("IDENTICAL?");
        controlLayout(btnIdentical,1);
        gameController.identicalEvent(btnIdentical);


        Button btnScramble = new Button("SCRAMBLE");
        controlLayout(btnScramble,1);

        btnScramble.setOnAction(actionEvent -> {
            scramble(puzzle);
        });

        Button btnIsSolved = new Button("Solved?");
        controlLayout(btnIsSolved,1);


        Button btnRotate = new Button("ROTATE");
        controlLayout(btnRotate,1);
        btnRotate.setOnAction(actionEvent -> {
            button.playAudio("sounds/soundEffects/Button-click.mp3");
            rotateScramble(puzzle);
        });

        ToggleButton tbMute = new ToggleButton("");
        controlLayout(tbMute, 2);
        tbMute.setId("mute");
        gameController.muteEvent(tbMute);

        if (Main.music.isPaused()) {
            tbMute.setSelected(true);
        }

        ToggleButton tbSnap = new ToggleButton("SNAP TO PLACE");
        controlLayout(tbSnap, 1);
        tbSnap.setId("tbSnap");
        gameController.snapEvent(tbSnap);

        if (gameController.snapEnabled){
            tbSnap.setSelected(true);
        }



        Button btnReturn = new Button("");
        controlLayout(btnReturn, 2);
        btnReturn.setId("RETURN");
        gameController.returnEvent(btnReturn);


//        Main.window.setOnCloseRequest(event -> {
//            timer.cancel();
//            timerTask.cancel();
//        });

        //Adding the layers for the scene together
        contentPane.getChildren().add(zoomGroup);
     /*   game.setContent(contentPane);
        gameRoot.getChildren().add(game);*/


        //Adding the buttons to the ingame screen
        navbar.getChildren().addAll(spacing2, btnReturn, tbMute, tbSnap, btnIdentical, btnSolve, btnScramble,
                btnRotate, lblTimer);

        root.getChildren().addAll(contentPane, navbar);

        scene.addEventFilter(KeyEvent.KEY_PRESSED, gameController.arrowClick);

        scramble(puzzle);

        return scene;


    }

    //Frederik (s194619)
    private float calculateIdealScaleConstant(IPuzzle puzzle) {
        float[] minMax = puzzle.getPiece(0).XYminMax();
        float lx = minMax[1] - minMax[0];
        float ly = minMax[3] - minMax[2];

        return 1/2f * ((windowX+(windowY))/2f) * 1f/((float)Math.sqrt(puzzle.getPieceCount()) * (lx+ly)/2f);
    }

    // Jonas (s194599) and Oliver (s194588)
    //Function that creates the layout of the buttons
    public void controlLayout(Labeled ctrl, int type) {
        ctrl.setId(ctrl.getText());
        //Either a small or big button
        if (type == 2) {
            ctrl.setPrefSize(48, 48);
        }
        else {
            ctrl.setPrefSize(100, 48);
        }
        ctrl.setLayoutY(8);
        ctrl.setFocusTraversable(false);
        /*ctrl.getStylesheets()
                .add(this.getClass().getResource("/resources/stylesheets/inGameMenu.css").toExternalForm());*/
        ctrl.getStylesheets()
                .add(ResourceLoader.getURL("stylesheets/inGameMenu.css").toExternalForm());
        ctrl.setTextAlignment(TextAlignment.CENTER);
        ctrl.setAlignment(Pos.CENTER);
    }

    //Function code by Frederik (s194619)
    public void scramble(IPuzzle puzzle) {
        java.util.Random random = new java.util.Random();
        int result = random.nextInt(2) + 1;
        if (result == 1 ) {
            button.playAudio("sounds/soundEffects/shuffle.mp3");
        }
        else {
            button.playAudio("sounds/soundEffects/Scramble.mp3");
        }

        Random rand = new Random();
        float w = windowX / scaleConstant;
        float h = windowY / scaleConstant;

        for (int i = 0; i < puzzle.getPieceCount(); i++) {
            IFigure fig = puzzle.getPiece(i);
            // fig.setPosition(Vec.zero);
            fig.setPosition(new Vec(w * (rand.nextFloat() - 0.5f), h * (rand.nextFloat() - 0.5f)));
//            fig.setAngle(rand.nextFloat() * 2*(float)Math.PI);
            // System.out.println(fig.getPosition());
        }

        redrawAll();

        redStroke();

    }

    //Function code by Jonas (s194599) and Frederik (s194619)
    public void rotateScramble(IPuzzle puzzle) {
        Random rand = new Random();
        float w = windowX / scaleConstant;
        float h = windowY / scaleConstant;

        for (int i = 0; i < puzzle.getPieceCount(); i++) {
            IFigure fig = puzzle.getPiece(i);
            // fig.setPosition(Vec.zero);
            fig.setAngle(rand.nextFloat() * 2*(float)Math.PI);
            // System.out.println(fig.getPosition());
        }

        redrawAll();
        redStroke();

    }

    //Function by Frederik (s194619)
    public static void redrawAll() {
        for (PuzzleView pv : puzzleViewList) {
            pv.redraw();
        }
    }

    double calcOffSet(double offSet, String dim) {

        if (dim.equals("x")) {
            return windowX / 2 - offSet;
        }

        else {
            return windowY / 2 - offSet;
        }
    }

    //Function by Frederik (s194619)
    public void startSolve(ISolvingAlgorithm algorithm, IPuzzle puzzle) {
        PuzzleView.enableObserve = false;
        algorithm.solve(puzzle);
        PuzzleView.enableObserve = true;
        redrawAll();
    }

    // Jonas (s194599)
    public static void greenStroke(){
        puzzleForm.setStroke(Color.GREEN);
        puzzleForm.setStrokeWidth(6);
        background.setFill(piecePaint);
    }

    // Jonas (s194599)
    public static void redStroke(){
        puzzleForm.setStroke(Color.RED);
        puzzleForm.setStrokeWidth(4);
        background.setFill(bgPaint);
    }


    public static void stopTimer(){
        timer.cancel();
        timerTask.cancel();
    }

}
