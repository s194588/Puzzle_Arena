package main.gui;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;

import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    public static String dataSet = "Select set...";//"Puzzles_set_1";
    public static String puzzleSet = "Select puzzle...";//"Puzzle-1r-2c-0995.json";
    public static String puzzleSetPath = ""; //ResourceLoader.getPathString() + "puzzles/Puzzles_set_1/Puzzle-1r-2c-0995.json";
    //= "src/resources/puzzles/Puzzles_set_1/Puzzle-1r-2c-0995.json";
    public static String theme = "Art";
    public static String Algo = "Polygon";
    @FXML
    private Button btnPuzzle;
    @FXML
    private Slider volumeSlider;
    @FXML
    private ComboBox cboxDataSet;
    @FXML
    private ComboBox cboxTheme;
    @FXML
    private  ComboBox cboxAlgo;
    Audiohandler button = new Audiohandler();

    // Jonas (s194599)
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // set slider value to current volume
        volumeSlider.setValue(Main.music.getVolume() * 100);
        volumeSlider.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                Main.music.setVolume(volumeSlider.getValue() / 100);
            }
        });

        btnPuzzle.setText(puzzleSet);
        cboxTheme.setValue(theme);
        cboxAlgo.setValue(Algo);

        if (!dataSet.equals("")) {
            cboxDataSet.setPromptText(dataSet);
        } else {
            cboxDataSet.setPromptText("144Solutions");
        }
    }

    //Christian (s194597)
    public void DataSet(ActionEvent event) {
        switch (cboxDataSet.getValue().toString()) {
            case "Classic":
                dataSet = "Classic";
                puzzleSet = "Classic-003-005-1331.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/Classic/Classic-003-005-1331.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "GeneralPuzzles":
                dataSet = "GeneralPuzzles";
                puzzleSet = "GeneralType01.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/GeneralPuzzles/GeneralType01.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "PieceToCompare_01":
                dataSet = "PieceToCompare_01";
                puzzleSet = "PieceList01.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/PieceToCompare_01/PieceList01.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "PieceToCompare_02":
                dataSet = "PieceToCompare_02";
                puzzleSet = "PieceList03.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/PieceToCompare_02/PieceList03.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "Puzzles_set_1":
                dataSet = "Puzzles_set_1";
                puzzleSet = "Puzzle-1r-2c-0995.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/Puzzles_set_1/Puzzle-1r-2c-0995.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "144Solutions":
                dataSet = "";
                puzzleSet = "144Solutions.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/144Solutions.json";
                btnPuzzle.setText(puzzleSet);
                break;
            case "TestPuzzles":
                dataSet = "TestPuzzles";
                puzzleSet = "checkIdentical.json";
                puzzleSetPath = ResourceLoader.getPathStringFile() + "puzzles/TestPuzzles/checkIdentical.json";
                btnPuzzle.setText("checkIdentical.json");
                break;

        }
    }

    // Jonas (s194599)
    // Choose puzzle
    public void choosePuzzleSet() throws Exception {

        // Open file explorer in data set folder
        //System.out.println(ResourceLoader.getPathString() + "puzzles");
        //System.out.println("C:\\Users\\frede\\IdeaProjects\\resources\\puzzles");
        String path = "";//ResourceLoader.class.getResource("puzzles") + dataSet;
                //(ResourceLoader.getPathString() + "puzzles/" + dataSet).replace("/","\\");//"";//"C:\\Users\\frede\\IdeaProjects\\resources\\puzzles";//ResourceLoader.getPathString() + "puzzles";//ResourceLoader.getPathString() + "puzzles/" + dataSet;
        //System.out.println(path);
        //System.out.println(ResourceLoader.getPathString() + "puzzles");
        //"C:\\Users\\frede\\IdeaProjects\\resources\\puzzles";//
        File file = new File(path);
        String absolutePath = file.getAbsolutePath();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(absolutePath));
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Json files", "*.json"));
        File selectedFile = fc.showOpenDialog(null);
        // update selected puzzle set
        if (selectedFile != null) {
            puzzleSet = selectedFile.getName();
            btnPuzzle.setText(puzzleSet);
            puzzleSetPath = selectedFile.getAbsolutePath();
        }
    }

    // Oliver (194588)
    // Choose theme
    public void Theme() throws Exception {

        switch (cboxTheme.getValue().toString()) {
            case "Classic":
                theme = "Classic";
                break;
            case "Art":
                theme = "Art";
                break;
            case "Nyan":
                theme = "Nyan";
                break;
            case "Chinese":
                theme = "Chinese";
                break;
            case "Fade":
                theme = "Fade";
                break;
        }

    }

    public void StartGame(ActionEvent event) throws Exception {
        button.playAudio("sounds/soundEffects/Scramble-hard.mp3");
        GameScene x = new GameScene();
        Scene gameScene = x.onLoad();;
        Main.changeScene(gameScene);
    }

    // Jonas (s194599)
    public void BacktoMenu(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");
        Scene SMScene2 = JFXScene.loadFXML("startMenu.fxml");
        Main.changeScene(SMScene2);
    }

    // Oliver (194588)
    public void AlgorithmChooser(ActionEvent event) throws  Exception {
        if (cboxAlgo.getValue().toString().equals("Polygon")) {
            Algo = "Polygon";
        }
        else {
            Algo = "Classic";
        }
    }

    // In game controls



}
