package main.gui;

//import javafx.scene.media.Media;
//import javafx.scene.media.MediaPlayer;


import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

// Oliver (s194588)
//AudioHandler class to whenever a new sound is needed
public class Audiohandler {

    private MediaPlayer mediaPlayerMusic;

    public void playAudio(String path) {
        //String p = "src/resources/";
        String p = "/../../resources/";
        Media media;
        //media = new Media(new File(p+path).toURI().toString());
        media = ResourceLoader.loadMedia(path);
        mediaPlayerMusic = new MediaPlayer(media);
        mediaPlayerMusic.play();

    }

    //Basic audio functions
    public void pauseAudio() { mediaPlayerMusic.pause(); }
    public void unPauseAudio() {
        mediaPlayerMusic.play();
    }
    public double getVolume(){ return mediaPlayerMusic.getVolume(); }
    public void setVolume(double volume){ mediaPlayerMusic.setVolume(volume); }
    public boolean isPaused() { return mediaPlayerMusic.getStatus().equals(MediaPlayer.Status.PAUSED); }
}
