package main.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


// class code by Jonas (s194599)
public class SolvedController implements Initializable {

    @FXML
    private Label lblTime;

    Audiohandler button = new Audiohandler();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // time spent solving the puzzle
        lblTime.setText(GameScene.timerString);

    }

    public void Return(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");

        GameScene.stopTimer();


        Scene SMScene2 = null;
        try {
            SMScene2 = JFXScene.loadFXML("mainMenu.fxml");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        // Close current stage
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();

        Main.changeScene(SMScene2);
    }


    public void Continue(ActionEvent event) throws Exception {
        button.playAudio("sounds/music/Button-click.mp3");

        // Close current stage
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();

    }

    public void PlayAgain(ActionEvent event) throws Exception {
        button.playAudio("sounds/soundEffects/Scramble-hard.mp3");

        GameScene.stopTimer();
        GameScene.redStroke();


        GameScene x = new GameScene();
        Scene gameScene = x.onLoad();
        Main.changeScene(gameScene);

        // Close current stage
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();

    }

    private void closeState(ActionEvent event){

    }
}
