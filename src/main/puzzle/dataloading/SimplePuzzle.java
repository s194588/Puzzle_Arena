package main.puzzle.dataloading;

import main.puzzle.util.geometry.linearalgebra.Vec;

import java.io.IOException;

//Class code by Frederik (s194619)
public class SimplePuzzle implements IPuzzleLoader {
    @Override
    public Vec[][] loadPuzzle() {
        Vec[][] arr = new Vec[2][3];
        arr[0] = new Vec[] { new Vec(0, 0), new Vec(100, 200), new Vec(200, 0) };
        arr[1] = new Vec[] { new Vec(200, 200), new Vec(100, 200), new Vec(200, 0) };
        return arr;
    }

    @Override
    public Vec[] loadForm() throws IOException {
        throw new RuntimeException("Not implemented");
    }
}
