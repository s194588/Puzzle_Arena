package main.puzzle.dataloading;

import main.puzzle.util.geometry.linearalgebra.Vec;

import java.io.IOException;

public interface IPuzzleLoader {
        Vec[] loadForm()  throws IOException;
        Vec[][] loadPuzzle()  throws IOException;
}
