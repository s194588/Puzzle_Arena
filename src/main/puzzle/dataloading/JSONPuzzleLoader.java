package main.puzzle.dataloading;

import main.puzzle.util.geometry.linearalgebra.Vec;
import org.json.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

//Class code by Christian (s194597)
public class JSONPuzzleLoader implements IPuzzleLoader {
    private String JSONFilePath;
    private JSONObject obj;

    public JSONPuzzleLoader(String filePath) throws IOException {
        try {
            this.JSONFilePath = filePath;
            String content = new String(Files.readAllBytes(Paths.get(JSONFilePath)));
            obj = new JSONObject(content);
        } catch (Error e) {
            System.out.println(e);
        }
    }

    public Vec[] loadForm() {
        JSONObject puzzle;
        JSONArray form;

        try {
            puzzle = (JSONObject) obj.get("puzzle");
            form = puzzle.getJSONArray("form");
        } catch (Exception e) {
            form = new JSONArray();
        }

        Vec[] allFormCoordinates;

        if(form.isEmpty()){
            allFormCoordinates = new Vec[4];
            float min = -10;
            float max = 10;

            Vec upperLeft = new Vec(min, max);
            Vec upperRight = new Vec(max, max);
            Vec lowerLeft = new Vec(min,min);
            Vec lowerRight = new Vec(min,max);

            allFormCoordinates[0] = upperLeft;
            allFormCoordinates[1] = upperRight;
            allFormCoordinates[2] = lowerRight;
            allFormCoordinates[3] = lowerLeft;

        } else {
            allFormCoordinates = new Vec[form.length()];
            int pointCount = 0;
            for(Object point : form) {
                JSONObject coordinate = (JSONObject) ((JSONObject) point).get("coord");
                float x = coordinate.getFloat("x");
                float y = coordinate.getFloat("y");
                allFormCoordinates[pointCount] = new Vec(x,y);
                pointCount++;
            }
        }
        //System.out.println(Arrays.toString(allFormCoordinates));

        return allFormCoordinates;
    }

    @Override
    public Vec[][] loadPuzzle() throws IOException {
        JSONArray pieces = (JSONArray) obj.get("pieces");

        int numberOfPieces = obj.getInt("no. of pieces");

        Vec[][] allPieceCoordinates = new Vec[numberOfPieces][];

        int pointCount = 0;
        for (Object piece : pieces) {

            JSONArray corners = ((JSONObject) piece).getJSONArray("corners");
            int nmbCorners = corners.length();

            allPieceCoordinates[pointCount] = new Vec[nmbCorners];

            int cornerCount = 0;
            for (Object coordinate : corners) {
                JSONObject coordinateX = (JSONObject) ((JSONObject) coordinate).get("coord");
                float x = coordinateX.getFloat("x");
                float y = coordinateX.getFloat("y");

//                System.out.println("x: " + x + " y: "+ y);
                allPieceCoordinates[pointCount][cornerCount] = new Vec(x, y);

                cornerCount++;
            }
            pointCount++;
        }

        return allPieceCoordinates;
    }

    public <T> String printArr(T[] arr){
        String ret = "";
        for (T e : arr){
            if (e == null){
                ret += "null, ";
            } else {
                ret += e.toString() + ", ";
            }
        }
        return ret;
    }

    public static void main(String[] args) throws IOException {
        IPuzzleLoader x = new JSONPuzzleLoader("Puzzle-1r-2c-0995.json");
        x.loadPuzzle();
    }
}
