package main.puzzle.util.algorithms.backtracking;

import java.util.List;

//Interface code by Frederik (s194619)
public interface IBackTracker<StackType> {
    /**
     * Calculates the possible steps (ways the the backtracking algorithm can advance)
     */
    List<StackType> possibleSteps();
    void backtrack(StackType pop);
    void advance(StackType push);
}
