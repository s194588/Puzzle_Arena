package main.puzzle.util.algorithms.backtracking;

import java.util.HashSet;
import java.util.Stack;

//Class code by Frederik (s194619)
public abstract class BacktrackAlgorithm<StackType>{

    public BacktrackAlgorithm(IBackTracker<StackType> backTracker)
    {
        this.backTracker = backTracker;
    }

    public IBackTracker<StackType> backTracker;

    private Stack<StackType> stack = new Stack<>();

    public Stack<StackType> getStack() {
        return stack;
    }

    /**
     * Sets up and resets the algorithm.
     */
    protected void setup(){
        closed.clear();
        stack.clear();
    }

    /**
     * Runs the algorithm. Returns true if the algorithm succeeded and false if it failed.
     */
    public boolean run(){
        setup();

        while (true){

            if (done()){
                return true;
            }

            //Finds the next possible and valid step. Next is null if there is none.
            StackType next = null;
            for (StackType s : backTracker.possibleSteps()){

                //Checks if the candidate step will lead to a valid state.
                stack.push(s);
                boolean closed = isClosed();
                stack.pop();

                if (!closed) {
                    next = s;
                    break;
                }
            }

            //Every state has been checked
            if (next == null && stack.isEmpty()) {
                return false;
            }

            //The current state is not valid or has no possible steps that lead to valid states
            if (!isValid() || next == null) {
                closeState();
                backtrack();
            }
            else {
                advance(next);
            }
        }
    }

    /**
     * Peaks at the top of the stack.
     */
    public StackType latest()
    {
        if (getStack().isEmpty())
            return null;
        return getStack().peek();
    }

    /**
     * Advances to the next state where the argument push is applied.
     */
    private void advance(StackType push){
        //System.out.println("Advancing");
        stack.push(push);
        onAdvance(push);
        backTracker.advance(push);
    }

    /**
     * Backtracks to the previous state.
     */
    private void backtrack(){
        //System.out.println("Backtracking");
        //closedState(getState()); //must come before pop
        StackType pop = stack.pop();
        onBacktrack(pop);
        backTracker.backtrack(pop);
    }

    protected void onAdvance(StackType push){}
    protected void onBacktrack(StackType pop){}

    private HashSet<HashSet<StackType>> closed = new HashSet<HashSet<StackType>>();

    /**
     * Closes the current state.
     */
    private void closeState(){
        closed.add(getState());
    }

    /**
     * Checks whether the current state is closed.
     */
    private boolean isClosed(){
        return closed.contains(getState());
    }


    //IMPORTANT NOTE:
    //Currently the order of the stack is ignored.
    /**
     * Returns the current state.
     * The order in which the algorithm has advanced is not taking into consideration
     * (ie. the order of the elements on the stack is ignored)
     */
    private HashSet<StackType> getState(){
        return new HashSet<>(stack);
    }

    /**
     * Checks whether the current state is valid.
     */
    protected abstract boolean isValid();

    /**
     * Checks the conditions for terminating a successful run of the algorithm.
     */
    protected abstract boolean done();
}
