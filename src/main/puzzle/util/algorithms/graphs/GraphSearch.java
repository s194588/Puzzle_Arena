package main.puzzle.util.algorithms.graphs;

import main.puzzle.util.interfaces.ISingleArgumentFunction;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Queue;

//Class code by Frederik (s194619)
public class GraphSearch {

    /**
     * Breadth first search that applies the given function to each node in the graph.
     */
    public static void bfs(INode start, ISingleArgumentFunction applyFunction){
        HashSet<INode> marked = new HashSet<>();
        Queue<INode> queue = new PriorityQueue<>();
        queue.add(start);

        while (!queue.isEmpty())
        {
            INode current = queue.remove();
            for(INode n : current.getNeighbors()){
                if (!marked.contains(n)){
                    queue.add(n);
                    marked.add(n);
                }
            }
            applyFunction.run(current);
        }
    }
}
