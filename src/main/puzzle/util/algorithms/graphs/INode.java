package main.puzzle.util.algorithms.graphs;

import java.util.List;

//Interface code by Frederik (s194619)
public interface INode {
    /**
     * The adjacent nodes connected by an edge.
     */
    <T extends INode> List<T> getNeighbors();
}
