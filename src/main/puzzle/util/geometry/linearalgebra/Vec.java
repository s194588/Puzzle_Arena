package main.puzzle.util.geometry.linearalgebra;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

//Class code by Frederik (s194619)
/**
 * Implementation of a 2-dimensional vector
 */
public class Vec {
    public final float x;
    public final float y;

    public static final Vec zero = new Vec(0,0);

    public Vec(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public static Vec polarVec(float angle, float length){
        return new Vec((float)Math.cos(angle)*length,(float)Math.sin(angle)*length);
    }

    //Vector operations:
    public static Vec copyOf(Vec v){
        return new Vec(v.x,v.y);
    }
    public static Vec add(Vec v1, Vec v2){
        return new Vec(v1.x + v2.x, v1.y + v2.y);
    }
    public static Vec sub(Vec v1, Vec v2){
        return new Vec(v1.x - v2.x, v1.y - v2.y);
    }
    public static Vec neg(Vec v){
        return new Vec(-v.x, -v.y);
    }
    public static Vec mul(Vec v, float k){
        return new Vec(v.x * k, v.y * k);
    }
    public static float dot(Vec v1, Vec v2)
    {
        return v1.x*v2.x + v1.y * v2.y;
    }

    public float length() {
        return (float) Math.sqrt(sqMag());
    }
    public float sqMag(){
        return x * x + y * y;
    }

    /**
     * The counter-clockwise angle between a vector and the vector (1,0).
     */
    public float angle() {
        float a = (float) Math.acos(x/length());
        if (y >= 0){
            return a;
        }
        return (float) (2*Math.PI-a);
    }
    /**
     * The counter-clockwise angle between two vectors.
     */
    public static float angleBetween(Vec from, Vec to){
        float fromAng = from.angle();
        float toAng = to.angle();

        if (toAng < fromAng){
            toAng += 2*Math.PI;
        }
        //System.out.println("to: " + toAng + ", from: " + fromAng);
        return toAng - fromAng;
    }

    /**
     * Returns the average the givens vectors.
     */
    public static Vec average(Vec... vecs){
        Vec sum = Vec.zero;
        for (Vec v : vecs){
            sum = Vec.add(sum, v);
        }
        return Vec.mul(sum, 1f/vecs.length);
    }
    public static Vec average(List<Vec> vecs){
        Vec sum = Vec.zero;
        for (Vec v : vecs){
            sum = Vec.add(sum, v);
        }
        return Vec.mul(sum, 1f/vecs.size());
    }

    /**
     * Returns -1 if acute, 0 if right-angled and 1 if obtuse.
     */
    public static int triangleType(Vec p1, Vec p2, Vec p3)
    {
        float l1 = Vec.sub(p1,p2).length();
        float l2 = Vec.sub(p1,p3).length();
        float l3 = Vec.sub(p2,p3).length();

        float smallestSum;
        float largest;


        if (l1 >= l2 && l1 >= l3)
        {
            smallestSum = l2*l2 + l3*l3;
            largest = l1*l1;
        }else if (l2 >= l1 && l2 >= l3)
        {
            smallestSum = l1*l1 + l3*l3;
            largest = l2*l2;
        } else //if (l3 >= l1 && l3 >= l2)
        {
            smallestSum = l1*l1 + l2*l2;
            largest = l3*l3;
        }
        //System.out.println(largest);
        //System.out.println(smallestSum);
        if (Math.abs(largest - smallestSum) <= 0.001f){
            return 0;
        }
        if (largest < smallestSum){
            return -1;
        }
        else
            return 1;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /**
     * Returns the point closest to the target point.
     */
    public static Vec closestVec(Vec target, Vec... vecs)
    {
        if (vecs.length == 0)
            throw new IllegalArgumentException("Must contain at least one vec.");
        Vec ret = vecs[0];
        float sqMag = sub(vecs[0],target).sqMag();
        for (Vec v : vecs){
            float candidate = sub(v,target).sqMag();
            if (candidate < sqMag){
                ret = v;
                sqMag = candidate;
            }
        }
        return ret;
    }

    /**
     * Returns the point closest to the target point.
     */
    public static Vec closestVec(Vec target, Collection<Vec> vecs)
    {
        Vec[] arr = new Vec[vecs.size()];
        return closestVec(target, vecs.toArray(arr));
    }

    /**
     * Returns the point furthest away from the target point.
     */
    public static Vec furthestVec(Vec target, Vec... vecs)
    {
        if (vecs.length == 0)
            throw new IllegalArgumentException("Must contain at least one vec.");
        Vec ret = vecs[0];
        float sqMag = sub(vecs[0],target).sqMag();
        for (Vec v : vecs){
            float candidate = sub(v,target).sqMag();
            if (candidate > sqMag){
                ret = v;
                sqMag = candidate;
            }
        }
        return ret;
    }

    /**
     * Returns the point furthest away from the target point.
     */
    public static Vec furthestVec(Vec target, Collection<Vec> vecs)
    {
        Vec[] arr = new Vec[vecs.size()];
        return furthestVec(target, vecs.toArray(arr));
    }
}
