package main.puzzle.util.geometry.linearalgebra;


//Class code by Frederik (s194619)
/**
 * Implementation of a 2x2 matrix.
 */
public class Matrix2 {

    public final float x1, x2, y1, y2;

    public static final Matrix2 identity = new Matrix2(1,0,0,1);
    public static final Matrix2 zero = new Matrix2(0,0,0,0);

    public Matrix2(float x1, float x2, float y1, float y2){
        this.x1 = x1; this.x2 = x2;
        this.y1 = y1; this.y2 = y2;
    }

    /**
     * Creates the matrix [v1|v2]
     */
    public Matrix2(Vec v1, Vec v2){
        x1 = v1.x; x2 = v2.x;
        y1 = v1.y; y2 = v2.y;
    }

    //Matrix operations:
    public static Matrix2 copyOf(Matrix2 m){
        return new Matrix2(m.x1 , m.x2, m.y1, m.y2);
    }
    public static Matrix2 add(Matrix2 m1, Matrix2 m2){
        return new Matrix2(m1.x1 + m2.x1, m1.x2 + m2.x2, m1.y1 + m2.y1, m1.y2 + m2.y2);
    }
    public static Matrix2 sub(Matrix2 m1, Matrix2 m2){
        return new Matrix2(m1.x1 - m2.x1, m1.x2 - m2.x2, m1.y1 - m2.y1, m1.y2 - m2.y2);
    }
    public static Matrix2 neg(Matrix2 m){
        return new Matrix2(-m.x1 , -m.x2, -m.y1, -m.y2);
    }
    public static Matrix2 mul(Matrix2 m, float k){
        return new Matrix2(m.x1 * k, m.x2 * k, m.y1 * k, m.y2 * k);
    }
    public static Vec MVProd(Matrix2 m, Vec v){
        return new Vec(m.x1* v.x+ m.x2* v.y, m.y1* v.x+ m.y2* v.y);
    }

    /**
     * Returns the rotation matrix of the given angle.
     */
    public static Matrix2 rotMatrix(float angRad){
        return new Matrix2(
                (float) Math.cos(angRad), (float) -Math.sin(angRad),
                (float) Math.sin(angRad), (float) Math.cos(angRad)
        );
    }
    public float determinant(){
        return x1*y2-y1*x2;
    }
    public static Matrix2 transpose(Matrix2 m){
        return new Matrix2(m.y2, -m.x2, -m.y1, m.x1);
    }
    public static Matrix2 inverse(Matrix2 m){
        return mul(transpose(m), 1f/m.determinant());
    }

    @Override
    public String toString() {
        return "|" + x1 + " " + x2 + "|\n" + "|" + y1 + " " + y2 + "|";
    }
}
