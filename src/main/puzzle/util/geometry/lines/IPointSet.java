package main.puzzle.util.geometry.lines;

import main.puzzle.util.geometry.linearalgebra.Vec;

//Interface code by Frederik (s194619)
public interface IPointSet {

    boolean contains(Vec v);
}
