package main.puzzle.util.geometry.lines;

import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.approximation.Approx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//Class code by Frederik (s194619)

public class FiniteLine {

    /**
     * Checks if the two lines intersect.
     */
    public static boolean intersects(IFiniteLine line1, IFiniteLine line2) {
        if (tryGetIntersection(line1,line2) == null) {
            Vec AB = Vec.sub(line1.getP2(), line1.getP1());
            Vec CD = Vec.sub(line2.getP2(), line2.getP1());
            if (Approx.areParallel(AB,CD)){
                if (Approx.onLine(line2, line1.getP1()) || Approx.onLine(line2,line1.getP2())
                || Approx.onLine(line1, line2.getP1()) || Approx.onLine(line1,line2.getP2())
                ){
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /** Returns the point of intersection.
     * If there the lines do not have a point of intersection then null is returned.
     */
    public static Vec tryGetIntersection(IFiniteLine line1, IFiniteLine line2) {
        Vec AB = Vec.sub(line1.getP2(), line1.getP1());
        Vec CD = Vec.sub(line2.getP2(), line2.getP1());
        if (Approx.areParallel(AB,CD)) {
            return null;
        }
        Matrix2 G = new Matrix2(AB, Vec.sub(Vec.zero, CD));
        Vec sol = Matrix2.MVProd(Matrix2.inverse(G), Vec.sub(line2.getP1(), line1.getP1()));
        if (sol.x > 1 || sol.x < 0 || sol.y > 1 || sol.y < 0) {
            return null;
        }
        return Vec.add(line1.getP1(), Vec.mul(AB, sol.x));
    }

    /*public static IPointSet getOverlap(IFiniteLine line1, IFiniteLine line2){
        Vec AB = Vec.sub(line1.getP2(), line1.getP1());
        Vec CD = Vec.sub(line2.getP2(), line2.getP1());
        if (Approx.areParallel(AB,CD)) {
            return (Vec v) -> ;
        }
        Matrix2 G = new Matrix2(AB, Vec.sub(Vec.zero, CD));
        Vec sol = Matrix2.MVProd(Matrix2.inverse(G), Vec.sub(line2.getP1(), line1.getP1()));
        if (sol.x > 1 || sol.x < 0 || sol.y > 1 || sol.y < 0) {
            return null;
        }
        return Vec.add(line1.getP1(), Vec.mul(AB, sol.x));
    }*/

    public static float longestDist(IFiniteLine line1, IFiniteLine line2){
        float opt1 = Vec.sub(line1.getP1(),line2.getP1()).length();
        float opt2 = Vec.sub(line1.getP1(),line2.getP2()).length();
        float opt3 = Vec.sub(line1.getP2(),line2.getP2()).length();
        return Math.max(Math.max(opt1,opt2),opt3);
    }

    public static float lengthOf(IFiniteLine line)
    {
        return Vec.sub(line.getP1(), line.getP2()).length();
    }

    /**
     * Returns the distance from a point to a line.
     */
    public static float pointDist(IFiniteLine line, Vec point){
        Vec d1 = Vec.sub(point,line.getP1());
        Vec d2 = Vec.sub(point,line.getP2());

        float l = FiniteLine.lengthOf(line);
        float l1 = d1.sqMag();
        float l2 = d2.sqMag();


        if (l+l1 > l2 && l+l2 > l1){
            Vec v = Vec.sub(line.getP2(), line.getP1());
            return Math.abs(v.x*(line.getP1().y - point.y) - (line.getP1().x - point.x)*v.y)/v.length();
        }
        //System.out.println(d1.sqMag());
        //System.out.println(d1.length() + " : " + d2.length());
        return Math.min(d1.length(),d2.length());

    }

    /*
     public static float pointDist(IFiniteLine line, Vec point){
        Vec d1 = Vec.sub(point,line.getP1());
        Vec d2 = Vec.sub(point,line.getP2());

        float l = FiniteLine.lengthOf(line);
        float l1 = d1.length();
        float l2 = d2.length();

        //If obtuse triangle
        System.out.println("T: " + Vec.triangleType(line.getP1(),line.getP2(),point));
        if (Vec.triangleType(line.getP1(),line.getP2(),point) >= 0)
        {
            return Math.min(d1.length(),d2.length());
        }
        Vec v = Vec.sub(line.getP2(), line.getP1());
        return Math.abs(v.x*(line.getP1().y - point.y) - (line.getP1().x - point.x)*v.y)/v.length();
}
     */

    public static <T extends IFiniteLine> List<Vec> getIntersections(List<T> lines1, List<T> lines2){
        List<Vec> ret = new ArrayList<>();
        for (T l1 : lines1){
            for (T l2 : lines2){
                Vec intersection = FiniteLine.tryGetIntersection(l1,l2);
                if (intersection != null){
                    ret.add(intersection);
                }
            }
        }
        return ret;
    }
    public static <T extends IFiniteLine> List<Vec> getIntersections(T[] lines1, T[] lines2){
        return getIntersections(Arrays.asList(lines1), Arrays.asList(lines2));
    }

    public static boolean hasProperOverlap(IFiniteLine line1, IFiniteLine line2){
        Vec AB = Vec.sub(line1.getP2(), line1.getP1());
        Vec CD = Vec.sub(line2.getP2(), line2.getP1());
        if (Approx.areParallel(AB,CD)) {
            Vec AC = Vec.sub(line2.getP1(), line1.getP2());
            if (Approx.areParallel(AB,AC)){
                return true;
            }
            return false;
        }
        return false;
    }
}
