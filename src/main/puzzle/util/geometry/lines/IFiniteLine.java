package main.puzzle.util.geometry.lines;

import main.puzzle.util.geometry.linearalgebra.Vec;

//Interface code by Frederik (s194619)
public interface IFiniteLine {
    /**
     * The first end point of the line.
     */
    Vec getP1();

    /**
     * The second end point of the line.
     */
    Vec getP2();
}
