package main.puzzle.util.geometry.approximation;

import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.lines.IFiniteLine;

//Class code by Frederik (s194619)
/**
 * Utilities for approximating and comparing vectors since vectors use floating-point values.
 */
public class Approx {

    //The margin of error used in most calculations.
    private static float MARGIN_OF_ERROR = 0.0001f; //BEST VALUE 0.0001f. TODO: Up precision to double from float everywhere

    public static boolean areEqual(float v1, float v2){
        return Math.abs(v1-v2) <= MARGIN_OF_ERROR;
    }

    public static boolean areEqual(Vec v1, Vec v2){
        if (v1 == null || v2 == null)
            throw new RuntimeException("Vectors cannot be null");
        return areEqual(Vec.sub(v1,v2).length(), 0);
    }

    /**
     * Checks if two vectors are parallel.
     */
    public static boolean areParallel(Vec v1, Vec v2){
        if (v2.x == 0 && v1.x != 0 || v2.y == 0 && v1.y != 0){
            return false;
        }
        if (v2.x == 0){
            if (v1.x == 0)
                return true;
            return false;
        }
        if (v2.y == 0){
            if (v1.y == 0)
                return true;
            return false;
        }
        float t1 = v1.x/v2.x;
        float t2 = v1.y/v2.y;
        if (areEqual(t1,t2)){
            return true;
        }
        return false;
    }

    public static boolean areParallel(IFiniteLine line1, IFiniteLine line2){
        return areParallel(Vec.sub(line1.getP2(),line1.getP1()), Vec.sub(line2.getP2(),line2.getP1()));
    }

    public static boolean areSameLength(Vec v1, Vec v2){
        return Math.abs(v1.length() - v2.length()) <= MARGIN_OF_ERROR;
    }

    /**
     * Checks if a point is on a line.
     */
    public static boolean onLine(IFiniteLine line, Vec point)
    {
        return FiniteLine.pointDist(line, point) <= MARGIN_OF_ERROR;
    }

    /**
     * Checks if the two lines pass through each other.
     * Returns false if the intersection happens at one of the edges end points.
     */
    public static boolean passesThrough(IFiniteLine line1, IFiniteLine line2) {
        //TODO: Fix to account for parallel lines
        if (FiniteLine.tryGetIntersection(line1,line2) != null) {
            Vec intersection = FiniteLine.tryGetIntersection(line1, line2);
            Vec[] endPoints = {line1.getP1(), line1.getP2(), line2.getP1(), line2.getP2()};
            for (Vec v : endPoints) {
                if (Approx.areEqual(v,intersection)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
