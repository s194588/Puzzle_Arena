package main.puzzle.util.geometry.polygon;

import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.lines.IFiniteLine;

//Class code by Frederik (s194619)
public class Edge implements IFiniteLine {
    private Vertex vertex1, vertex2;

    public Edge(Vertex v1, Vertex v2){
        vertex1 = v1; vertex2 = v2;
    }

    public Vertex getVertex1() {
        return vertex1;
    }

    public Vertex getVertex2() {
        return vertex2;
    }

    @Override
    public Vec getP1() {
        return vertex1.getPosition();
    }

    @Override
    public Vec getP2() {
        return vertex2.getPosition();
    }

}
