package main.puzzle.util.geometry.polygon;

import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.List;

//Class code by Frederik (s194619)
public class Vertex {
    private Vec position;

    public Vec getPosition() {
        return position;
    }

    public Vertex(Vec v)
    {
        position = v;
    }

    public Vertex(float x, float y){
        this.position = new Vec(x,y);
    }


    public void setPosition(Vec v)
    {
        if (v == null)
            throw new IllegalArgumentException("Position cannot be null.");
        position = v;
    }

    public static Vec[] toVecList(List<Vertex> vertices)
    {
        Vec[] ret = new Vec[vertices.size()];
        for (int i = 0; i < ret.length; i++){
            ret[i] = vertices.get(i).position;
        }
        return ret;
    }
}
