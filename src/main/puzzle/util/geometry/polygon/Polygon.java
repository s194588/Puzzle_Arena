package main.puzzle.util.geometry.polygon;

import main.puzzle.util.general.tuples.Duo;
import main.puzzle.util.general.GeneralUtil;
import main.puzzle.util.general.listandarrays.ListAndArrayUtil;
import main.puzzle.util.general.tuples.Trio;
import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.lines.IFiniteLine;

import java.util.*;

//Class code by Frederik (s194619)
public class Polygon {

    /**
     * Create a polygon using the given vertices. Each vertex if connected to the
     * subsequent by an edge. The last vertex is connected to the first by an edge.
     */
    public Polygon(Vertex... vertices) {
        if (vertices.length < 3) {
            throw new IllegalArgumentException("Invalid polygon. The polygon must have at least three vertices.");
        }
        for (Vertex v : vertices) {
            this.vertices.add(v);
        }
    }

    public Polygon(Vec... points) {
        if (points.length < 3) {
            throw new IllegalArgumentException("Invalid polygon. The polygon must have at least three vertices.");
        }
        for (Vec v : points) {
            this.vertices.add(new Vertex(v));
        }
    }

    private List<Vertex> vertices = new LinkedList<>();

    public final List<Vertex> getVertices() {
        return vertices;
    }

    public final List<Vec> getVerticesAsVecs()
    {
        ArrayList<Vec> ret = new ArrayList<>();
        for(Vertex v : vertices){
            ret.add(v.getPosition());
        }
        return ret;
    }

    /*public void addVertex(Vertex v, Vertex neighbor1, Vertex neighbor2) {
        int index1 = vertices.indexOf(neighbor1);
        int index2 = vertices.indexOf(neighbor2);
        if (index1 != -1 && index2 != -1) {
            if (Math.abs(index1 - index2) == 1) {
                vertices.add(Math.min(index1, index2), v);
                return;
            } else if (Math.abs(index1 - index2) == vertices.size() - 1) {
                vertices.add(v);
                return;
            }
        }
        throw new RuntimeException("Neighboring edges must be connected.");
    }

    public void addVertex(Vertex v, Edge e) {
        addVertex(v, e.getVertex1(), e.getVertex2());
    }

    public void removeVertex(Vertex v) {
        vertices.remove(v);
    }*/

    private Edge[] ecache = null;

    public Edge[] getEdges() {
        if (ecache == null) {
            Edge[] ret = new Edge[vertices.size()];
            for (int i = 0; i < vertices.size(); i++) {
                ret[i] = new Edge(vertices.get(i), vertices.get((i + 1) % vertices.size()));
            }
            ecache = ret;
        }
        return ecache;
    }


    //Idea: check if there are any side that are not parallel which intersect and which do not share an endpoint.
    /**
     * Returns true if this and the given polygon overlap.
     */
    public boolean overlaps(Polygon p) {
        System.out.println("---");
        for (Vertex v : getVertices()){
            if (p.hasInside(v)){
                System.out.println("x");
                return true;
            }
        }
        for (Vertex v : p.getVertices()){
            if (hasInside(v)){
                System.out.println("y" + v.getPosition());
                return true;
            }
        }
        for (Edge e1 : getEdges()){
            for (Edge e2 : p.getEdges()){
                if (FiniteLine.intersects(e1,e2)){
                    System.out.println("z");
                    return true;
                }
            }
        }
        return false;

        //return !getIntersections(p).isEmpty();
    }

    private boolean onBorder(Vec v)
    {
        for (Edge e : getEdges())
        {
            if (Approx.onLine(e,v)){
                return true;
            }
        }
        return false;
    }

    public boolean insideOverlaps(Polygon p)
    {
        if (overlaps(p)){
            return true;
        }



        Edge[] edges = getEdges();
        for (Vertex v : getVertices()){
            if (p.hasInside(v)){
                if (!onBorder(v.getPosition())){
                    return true;
                }
            }
        }
        /*
        for (Edge e : edges)
        {
            for (Edge pe : p.getEdges())
            {
                if (FiniteLine.intersects(e,pe))
                {
                    Vec i = FiniteLine.tryGetIntersection(e,pe);
                    if (i != null &&
                            !(Approx.areEqual(i,e.getP1())
                            || Approx.areEqual(i,e.getP2())
                            ||Approx.areEqual(i,pe.getP1())
                            || Approx.areEqual(i,pe.getP2()))
                    )
                    {
                        return false;
                    }
                }
            }
        }*/
        return false;
    }

    /**
     * Returns the intersections between this and the given polygon.
     */
    public List<Vec> getIntersections(Polygon p) {
        Edge[] edges1 = getEdges();
        Edge[] edges2 = p.getEdges();
        return FiniteLine.getIntersections(edges1, edges2);
    }

    public boolean hasInside(Vertex v)
    {
        return hasInside(v.getPosition());
    }

    public boolean hasInside(Vec v) {
        Vec p1 = v, pinf = new Vec(-10000,v.y);
        IFiniteLine ray = new IFiniteLine() {
            public Vec getP1() { return pinf; }
            public Vec getP2() { return p1; }
        };

        int count = 0;
        Edge[] edges = getEdges();
        Edge previous = edges[edges.length-1];
        Vertex vert = edges[edges.length-1].getVertex2();

        for (Edge e : edges) {
            if (FiniteLine.intersects(e,ray)){
                if (previous != null && previous.getVertex2() == e.getVertex1()){
                    Vec v1 = previous.getVertex1().getPosition();
                    Vec v2 = previous.getVertex2().getPosition();
                    Vec v3 = e.getVertex2().getPosition();
                    //System.out.println("Yes! v1: " + v1 + ", v2: " + v2 + ", v3: " + v3);
                    if (v1.y == v2.y && v3.y == v2.y){
                        continue;
                    }

                    if (v1.y <= v2.y && v3.y >= v2.y || v1.y >= v2.y && v3.y <= v2.y){
                        count++;
                    }
                }
                count++;
                previous = null;
                if (Approx.onLine(ray,e.getP2()))
                {
                    previous = e;
                    count--;
                }
            }else{
                previous = null;
            }
        }
        System.out.println("count: " + count);
        return count % 2 != 0;
    }

    public boolean hasInside(Polygon p) {
        for (Vertex v : p.getVertices()){
            if (!hasInside(v)){
                return false;
            }
        }
        return true;
    }

    public static Polygon copyOf(Polygon p) {
        Vertex[] vertices = new Vertex[p.vertices.size()];
        for (int i = 0; i < p.vertices.size(); i++) {
            vertices[i] = new Vertex(p.vertices.get(i).getPosition());
        }
        return new Polygon(vertices);
    }

    /**
     * Returns the slimmest rectangle (whose sides are parallel with the x and y
     * axis) that this polygon can fit inside.
     */
    public Polygon boundingBox() {
        float maxh = 0, minh = 0, maxw = 0, minw = 0;
        for (Vertex v : getVertices()) {
            maxh = Math.max(maxh, v.getPosition().y);
            minh = Math.min(minh, v.getPosition().y);
            maxw = Math.max(maxw, v.getPosition().x);
            minw = Math.min(minw, v.getPosition().x);
        }
        //Dont swap around maxw and maxh in (x,y) >:(
        return new Polygon(new Vertex(maxw, maxh), new Vertex(maxw, minh), new Vertex(minw, minh),
                new Vertex(minw, maxh));
    }

    public static <T extends Polygon> Polygon boundingBox(Collection<T> polygons)
    {
        if (polygons.isEmpty()){
            throw new IllegalArgumentException("Must contain at least one polygon");
        }
        float maxh = -Float.MAX_VALUE, minh = Float.MAX_VALUE, maxw = -Float.MAX_VALUE, minw = Float.MAX_VALUE;
        for (Polygon p : polygons) {
            for (Vertex v : p.getVertices()) {
                maxh = Math.max(maxh, v.getPosition().y);
                minh = Math.min(minh, v.getPosition().y);
                maxw = Math.max(maxw, v.getPosition().x);
                minw = Math.min(minw, v.getPosition().x);
            }
        }
        //Dont swap around maxw and maxh in (x,y) >:(
        return new Polygon(new Vertex(maxw, maxh), new Vertex(maxw, minh), new Vertex(minw, minh),
                new Vertex(minw, maxh));
    }

    public static <T extends Polygon> Polygon tightBox(List<T> polygons)
    {
        Vertex top = polygons.get(0).getVertices().get(0), right = polygons.get(0).getVertices().get(0), left = polygons.get(0).getVertices().get(0), bot = polygons.get(0).getVertices().get(0);
        boolean rot = true;
        float me = 0.001f;
        for (Polygon p : polygons) {
            for (Vertex v : p.getVertices()) {
                if (Math.abs(v.getPosition().x - right.getPosition().x) < me || Math.abs(v.getPosition().x - left.getPosition().x) < me
                        || Math.abs(v.getPosition().y - top.getPosition().y) < me || Math.abs(v.getPosition().y - bot.getPosition().y) < me) {
                    rot = false;
                    continue;
                }

                if (v.getPosition().y > top.getPosition().y) {
                    rot = true;
                    top = v;
                }
                if (v.getPosition().y < bot.getPosition().y) {
                    rot = true;
                    bot = v;
                }
                if (v.getPosition().x < left.getPosition().x) {
                    rot = true;
                    left = v;
                }
                if (v.getPosition().x > right.getPosition().x) {
                    rot = true;
                    right = v;
                }
            }
        }
        if (!rot){
            return boundingBox(polygons);
        }
        return new Polygon(new Vertex(top.getPosition()),new Vertex(right.getPosition()),new Vertex(bot.getPosition()),new Vertex(left.getPosition()));
    }

    /*public static Polygon rb(List<Polygon> polygons)
    {
        PriorityQueue<Trio<Float, Integer, Vec>> heap = new PriorityQueue<>(new Comparator<Trio<Float, Integer, Vec>>() {
            @Override
            public int compare(Trio<Float, Integer, Vec> o1, Trio<Float, Integer, Vec> o2) {
                return -Float.compare(o1.v1, o2.v2);
            }
        });
        for (Polygon p : polygons){
            for (Vertex v : p.getVertices()){
                heap.add(new Trio<>())
            }
        }
    }*/

    /**
     * Checks whether the given polygon is identical to this one. This is done as follows.
     * First, the edges corresponding to the equivalent polygon of minimal number of sides is calculated.
     * Next, it checks if the lengths of the edges occurring in both polygons are the same.
     * It also checks if the angles between neighboring edges occurring in both
     * polygons are the same. If these two requirements are met, the function
     * returns true.
     */
    public boolean areIdentical(Polygon p2) {
        Polygon p1 = this;
        //Edge[] edges1 = p1.getEdges();
        //Edge[] edges2 = p2.getEdges();
        List<Edge> se1 = p1.simplifyEdges();
        List<Edge> se2 = p2.simplifyEdges();
        Edge[] edges1 = se1.toArray(new Edge[se1.size()]);
        Edge[] edges2 = se2.toArray(new Edge[se2.size()]);
        //System.out.println("l1: " + edges1.length + ", l2: " + edges2.length);
        if (edges1.length == edges2.length) {
            int n = edges1.length;
            Duo<Float, Float>[] vals1 = new Duo[edges1.length];
            Duo<Float, Float>[] vals2 = new Duo[edges2.length];

            int decimalPlaces = 4;

            for (int i = 0; i < n; i++) {
                Vec thisEdge1 = Vec.sub(edges1[i].getP1(), edges1[i].getP2());
                Vec nextEdge1 = Vec.sub(edges1[(i + 1) % n].getP1(), edges1[(i + 1) % n].getP2());
                float minAng1 = GeneralUtil.roundTo(
                        Math.min(Vec.angleBetween(thisEdge1, nextEdge1), Vec.angleBetween(nextEdge1, thisEdge1)),
                        decimalPlaces);

                Vec thisEdge2 = Vec.sub(edges2[i].getP1(), edges2[i].getP2());
                Vec nextEdge2 = Vec.sub(edges2[(i + 1) % n].getP1(), edges2[(i + 1) % n].getP2());
                float minAng2 = GeneralUtil.roundTo(
                        Math.min(Vec.angleBetween(thisEdge2, nextEdge2), Vec.angleBetween(nextEdge2, thisEdge2)),
                        decimalPlaces);

                vals1[i] = new Duo(GeneralUtil.roundTo(FiniteLine.lengthOf(edges1[i]), decimalPlaces), minAng1);
                vals2[i] = new Duo(GeneralUtil.roundTo(FiniteLine.lengthOf(edges2[i]), decimalPlaces), minAng2);
            }
            //System.out.println(Arrays.toString(vals1));
            //System.out.println(Arrays.toString(vals2));
            //System.out.println("-----");

            return ListAndArrayUtil.hasAlignment(vals1, vals2) //;
                    //Line below needed?
                    || ListAndArrayUtil.hasAlignment(vals1, ListAndArrayUtil.reverseOf(vals1,new Duo[vals2.length]));
        }
        return false;
    }

    /**
     * Returns the edges corresponding to the equivalent polygon of minimal number of sides.
     */
    public List<Edge> simplifyEdges()
    {
        List<Edge> ret = new ArrayList<>();
        Edge start = getEdges()[0];
        Edge end = getEdges()[0];

        //To make sure that it starts on an edge that is not parallel with the previous.
        boolean ready = false;

        for (int i = 0; true; i++){
            Edge next = getEdges()[(i+1) % getEdges().length];
            if (!Approx.areParallel(end, next)) {
                if (ready) {
                    ret.add(new Edge(new Vertex(start.getP1()), new Vertex(end.getP2())));
                }else{
                    ready = true;
                }
                start = next;
                if (i >= getEdges().length){
                    break;
                }
            }
            end = next;
        }
        return ret;
    }

    /**
     * Returns the union between two polygons using the same vertex references when
     * possible.
     */
    public static Polygon union(Polygon p1, Polygon p2) {
        List<Vec> intersections = p1.getIntersections(p2);
        if (intersections.isEmpty()) {
            throw new RuntimeException("Union cannot be applied. Polygons must intersect.");
        }

        throw new RuntimeException("Not Implemented.");
    }

    /**
     * Returns (minX, maxX, minY, maxY) of the vertices.
     */
    public float[] XYminMax()
    {
        Vec v0pos = getVertices().get(0).getPosition();
        float minX = v0pos.x, maxX = v0pos.x;
        float minY = v0pos.y, maxY = v0pos.x;
        for (int i = 1; i < getVertices().size(); i++){
            Vec pos = getVertices().get(i).getPosition();
            minX = Math.min(pos.x, minX);
            maxX = Math.max(pos.x, maxX);
            minY = Math.min(pos.y, minY);
            maxY = Math.max(pos.y, maxY);
        }
        return new float[]{minX,maxX,minY,maxY};
    }
}
/*
    public boolean hasInside(Vertex v) {
        Vec pinf1 = new Vec(Float.MAX_VALUE,v.getPosition().y), pinf2 = new Vec(-Float.MAX_VALUE,v.getPosition().y);
        IFiniteLine ray = new IFiniteLine() {
            public Vec getP1() { return pinf1; }
            public Vec getP2() { return pinf2; }
        };

        Edge[] internalEdges = new Edge[vertices.size()];
        Edge[] edges = getEdges();
        for (int i = 0; i < vertices.size(); i++) {
            internalEdges[i] = new Edge(v, vertices.get(i));
            if (Approx.areEqual(FiniteLine.pointDist(edges[i], v.getPosition()), 0)) {
                return true;
            }
        }
        for (Edge e1 : edges) {
            for (Edge e2 : internalEdges) {
                Vec intersection = FiniteLine.tryGetIntersection(e1, e2);

                if (intersection != null) {
                    if (!Approx.areEqual(intersection, e1.getP1()) && !Approx.areEqual(intersection, e1.getP2()))
                        return false;
                }
            }
        }
        return true;
    }


    public boolean insideOverlaps(Polygon p) {

        //TODO: What if the polygon is fully inside (nothing sticking out). Then no edges overlap.
        for (Edge e1 : getEdges()){
            for (Edge e2 : p.getEdges()){
                if (FiniteLine.hasProperOverlap(e1,e2)){
                    continue;
                }
                else if (FiniteLine.tryGetIntersection(e1,e2) != null)
                {
                    return true;
                }
            }
        }
        return false;


        //for (Vertex v : p.getVertices()){
        //    if (hasInside(p)){
        //        boolean b = true;
        //        for (Edge e : getEdges())
        //        {
        //            if (FiniteLine.pointDist(e,v.getPosition()) <= 0.01f){
        //                b = false;
        //            }
        //        }
        //        if (b){
        //            return true;
        //        }
        //    }
        //}
        //return false;
    }
 */


