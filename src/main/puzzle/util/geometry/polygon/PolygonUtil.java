package main.puzzle.util.geometry.polygon;

import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.linearalgebra.Vec;

//Class code by Frederik (s194619)
public class PolygonUtil{

    public static float rectArea(Polygon p){
        if (p.getVertices().size() != 4){
            throw new IllegalArgumentException("Must have 4 vertices.");
        }
        float sqa = Vec.sub(p.getVertices().get(0).getPosition(), p.getVertices().get(1).getPosition()).sqMag();
        float sqb = Vec.sub(p.getVertices().get(0).getPosition(), p.getVertices().get(3).getPosition()).sqMag();
        float sqc = Vec.sub(p.getVertices().get(2).getPosition(), p.getVertices().get(1).getPosition()).sqMag();
        float sqd = Vec.sub(p.getVertices().get(2).getPosition(), p.getVertices().get(3).getPosition()).sqMag();
        if (!Approx.areEqual(sqc*sqd, sqa*sqb)){
            //throw new IllegalArgumentException("Not a rectangle.");
            return 0;
        }
        return (float ) Math.sqrt(sqa*sqb);
    }
    public static float pgArea(Polygon p)
    {
        return 0;
    }
}
