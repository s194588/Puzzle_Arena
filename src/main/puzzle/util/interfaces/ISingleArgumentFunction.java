package main.puzzle.util.interfaces;


//Interface code by Frederik (s194619)
/**
 * A delegate for functions with a single argument.
 */
public interface ISingleArgumentFunction<ArgType> {

    void run(ArgType arg);
}
