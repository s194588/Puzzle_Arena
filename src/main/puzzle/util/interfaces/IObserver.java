package main.puzzle.util.interfaces;


//Interface code by Frederik (s194619)
public interface IObserver {

    void update();
}
