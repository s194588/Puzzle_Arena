package main.puzzle.util.general;


//Class code by Frederik (s194619)
public class GeneralUtil {
    /**
     * Rounds a floating point value rounded to the nearest given decimal places.
     */
    public static float roundTo(float x, int decimalPlaces)
    {
        float mul = (float) Math.pow(10,decimalPlaces);
        return Math.round(x*mul)/mul;
    }

    /**
     * If the value is below from, returns from. If the value is above to, returns to. Otherwise returns the value.
     */
    public static float clamp(float value, float from, float to){
        if (value < from) return from;
        if (value > to) return to;
        return value;
    }
    /**
     * If the value is below from, returns from. If the value is above to, returns to. Otherwise returns the value.
     */
    public static int clamp(int value, int from, int to){
        if (value < from) return from;
        if (value > to) return to;
        return value;
    }
    /**
     * Intuitively, returns the value such that it is between from in a cyclic sense.
     */
    public static int loop(int value, int from, int to)
    {
        int r = (value - from) % (to - from);
        if (r < 0)
            return to + r;
        return from + r;
    }
}
