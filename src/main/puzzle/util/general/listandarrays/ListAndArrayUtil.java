package main.puzzle.util.general.listandarrays;

import java.util.*;

//Class code by Frederik (s194619)
public class ListAndArrayUtil {

    /**
     * Slow algorithm for checking alignment in cyclic lists. Checks if the second list can fit in the first (not the other way around).
     */
    public static <T> boolean hasAlignment(List<T> l1, List<T> l2) {
        if (l2.size() == 0) {
            return true;
        }
        for (int i = 0; i < l1.size(); i++) {
            for (int j = 0; j < l2.size(); j++) {
                //System.out.println("("+l1.get((i + j)%l1.size())+"; " + l2.get(j) + ")");
                if (!l1.get((i + j) % l1.size()).equals(l2.get(j))) {
                    break;
                }
                if (j == l2.size()-1){
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * Slow algorithm for checking alignment in cyclic arrays. Checks if the second array can fit in the first (not the other way around).
     */
    public static <T> boolean hasAlignment(T[] l1, T[] l2) {
        return hasAlignment(Arrays.asList(l1),Arrays.asList(l2));
    }

    /**
     * Returns the reversal of an array placed into "target".
     */
    public static <T> T[] reverseOf(T[] arr, T[] target)
    {
        for (int i = 0; i < arr.length; i++){
            target[arr.length - 1 - i] = arr[i];
        }
        return target;
    }

    public static <T> boolean arrayContains(T[] arr, T item)
    {
        for (T t : arr){
            if (t.equals(item)){
                return true;
            }
        }
        return false;
    }
}




/*
    @Override
    public E get(int index) {
        return super.get(index % size());
    }

    @Override
    public void add(int index, E element) {
        super.add(index % size(), element);
    }

    @Override
    public E remove(int index) {
        return super.remove(index % size());
    }

    Returns the index of the element where the index is in the interval [0; size()[.
    NOTE: Returns -1 if not found. This is not -1 in the cyclic sense.
@Override
public int indexOf(Object o) {
    return super.indexOf(o);
}
 */