package main.puzzle.util.general.tuples;

//Class code by Frederik (s194619)

/**
 * A tuple for storing exactly three elements.
 */
public class Trio<T1,T2,T3> extends Duo<T1, T2> {
    public T1 v1;
    public T2 v2;
    public T3 v3;

    public Trio(T1 v1, T2 v2, T3 v3)
    {
        super(v1, v2);
        this.v3 = v3;
    }

    @Override
    public boolean equals(Object o) {
        Trio t = (Trio) o;
        return super.equals((Duo)o) && v3.equals(t.v3);
    }

    @Override
    public String toString() {
        return "(" + v1 + ", " + v2 + ", " + v3 + ')';
    }
}
