package main.puzzle.util.general.tuples;

//Class code by Frederik (s194619)

/**
 * A tuple for storing exactly two elements.
 */
public class Duo<T1,T2> {
    public T1 v1;
    public T2 v2;

    public Duo(T1 v1, T2 v2){
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public boolean equals(Object o) {
        super.equals(o);
        Duo t = (Duo) o;
        return v1.equals(t.v1) && v2.equals(t.v2);
    }

    @Override
    public String toString() {
        return "(" + v1 + ", " + v2 + ')';
    }
}
