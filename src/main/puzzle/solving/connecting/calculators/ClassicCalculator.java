package main.puzzle.solving.connecting.calculators;

import main.puzzle.dataloading.IPuzzleLoader;
import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.Puzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.general.GeneralUtil;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.Vertex;

import java.io.IOException;
import java.util.*;

//Class code by Frederik (s194619)
public class ClassicCalculator implements IConnectionCalculator {

    public ClassicCalculator(IPuzzle puzzle){
        this.puzzle = puzzle;
        mirror = new Puzzle(loader);
        //System.out.println("Done loading mirror");
        for (int i = 0; i < mirror.getPieceCount(); i++){
            mirror.getPiece(i).setPositionAnchor(puzzle.getPiece(i).getPosition());
        }
        //System.out.println("Done creating mirror");
        /*try {
            mirror.reload();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }**/
        //System.out.println("c " + mirror.getPieceCount() + " " +puzzle.getPieceCount());
        polygonCalculator = new PolygonCalculator(mirror);
    }
    private IPuzzle puzzle;
    private IPuzzle mirror;
    private PolygonCalculator polygonCalculator;

    IPuzzleLoader loader = new IPuzzleLoader() {
        @Override
        public Vec[] loadForm() throws IOException {
            Vec[] arr = new Vec[puzzle.getForm().getVertices().size()];
            return puzzle.getForm().getVerticesAsVecs().toArray(arr);
        }

        @Override
        public Vec[][] loadPuzzle() throws IOException {
            Vec[][] ret = new Vec[puzzle.getPieceCount()][];
            for (int i = 0; i < ret.length; i++)
            {
                //ret[i] = puzzle.getPiece(i).getVerticesAsVecs().toArray(new Vec[puzzle.getPiece(i).getVertices().size()]);
                //System.out.println(Arrays.toString(ret[i]));
                ret[i] = getCorners(i);
            }

            return ret;
        }
    };

    private List<PuzzleConnection> reduce4(List<PuzzleConnection> cons){
        List<PuzzleConnection> ret = new ArrayList<>();
        for (int i = 0; i < 4 && i < cons.size(); i++){
            ret.add(cons.get(i));
        }
        return ret;
    }

    @Override
    public List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID) {
        List<PuzzleConnection> cons = polygonCalculator.getConnections(outlierPieceID,targetPieceID);
        for (PuzzleConnection c : cons){
            c.setPuzzle(puzzle);
            //Vec dout = Vec.sub(puzzle.getPiece(c.outlierID).getPosition(),mirror.getPiece(c.outlierID).getPosition());
            //Vec dtar = Vec.sub(mirror.getPiece(c.targetID).getPosition(),puzzle.getPiece(c.targetID).getPosition());
            //Vec r = Vec.add(dout, dtar);
            //c.deltaPos = Vec.add(c.deltaPos,r);
        }
        return cons;
    }

    public ClassicRep getClassicRep()
    {
        return null;
    }

    public Vec[] getCorners(int id)
    {
        //return new Vec[]{new Vec(0,0), new Vec(1,0), new Vec(1,1), new Vec(0,1)};

        Set<Vec> vecs = new HashSet<>();
        for (Vertex v : getPuzzle().getPiece(id).getVertices()){
            vecs.add(v.getPosition());
        }
        //System.out.println(vecs.size());
        PriorityQueue<Object[]> sortedMap = new PriorityQueue<>(new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                return -Float.compare((float)o1[0],(float)o2[0]);
            }
        });

        for (int i = 0; i < getPuzzle().getPiece(id).getVertices().size(); i++){
            Vertex v = getPuzzle().getPiece(id).getVertices().get(i);
            vecs.remove(v.getPosition());
            float fd = Vec.sub(Vec.closestVec(v.getPosition(), vecs),v.getPosition()).sqMag();
            //System.out.println(fd);
            sortedMap.add(new Object[]{fd,v,i});
            vecs.add(v.getPosition());
        }
        List<Object[]> temp = new ArrayList<>();
        //System.out.println(sortedMap.size());
        /*for (Object[] o : sortedMap){
            System.out.print(((float)o[0]) + " ");
        }*/
        for (int i = 0; i < 4; i++){
            temp.add(sortedMap.poll());
            //System.out.println("v: " + ret[i]);
        }
        temp.sort(new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                return Integer.compare((int)(o1[2]), (int)(o2[2]));
            }
        });
        List<Vec> ret = new ArrayList<>();
        Set<Vec> checker = new HashSet<>();
        int size = getPuzzle().getPiece(id).getVertices().size();
        for (int i = 0; i < 4; i++){
            int x = (int)(temp.get(i)[2]);
            checker.add(getPuzzle().getPiece(id).getVertices().get(GeneralUtil.loop(x,0,size)).getPosition());
        }

        for (int i = 0; i < 4; i++){
            int x = (int)(temp.get(i)[2]);
            Vec v1 = getPuzzle().getPiece(id).getVertices().get(GeneralUtil.loop(x-2,0,size)).getPosition();
            Vec v2 = getPuzzle().getPiece(id).getVertices().get(GeneralUtil.loop(x-1,0,size)).getPosition();
            Vec v3 = getPuzzle().getPiece(id).getVertices().get(GeneralUtil.loop(x+1,0,size)).getPosition();
            Vec v4 = getPuzzle().getPiece(id).getVertices().get(GeneralUtil.loop(x+2,0,size)).getPosition();
            if (!checker.contains(v1) && !checker.contains(v2))
                ret.add(Vec.average(v1,v2));
            ret.add(getPuzzle().getPiece(id).getVertices().get(x).getPosition());
            if (!checker.contains(v3) && !checker.contains(v4))
                ret.add(Vec.average(v3,v4));
        }
        
        //System.out.println("xxxxxxxxxxxxxxxx");
        /*return
                new Vec[]{((Vertex)(temp.get(0)[1])).getPosition(),
                        ((Vertex)(temp.get(1)[1])).getPosition(),
                        ((Vertex)(temp.get(2)[1])).getPosition(),
                        ((Vertex)(temp.get(3)[1])).getPosition()
        };*/
        return ret.toArray(new Vec[ret.size()]);
    }

    public Vec closestPoint(Vec v1, Vec v2, Vec target)
    {
        if (Vec.sub(v1,target).sqMag() < Vec.sub(v2,target).sqMag())
            return v1;
        return v2;
    }

    @Override
    public IPuzzle getPuzzle() {
        return puzzle;
    }

    private class ClassicRep extends PuzzlePiece
    {
        private int up;
        private int down;
        private int left;
        private int right;
    }

}
/*
        Vec[] vertexPositions = new Vec[4];
        PuzzlePiece piece = getPuzzle().getPiece(ID);

        vertexPositions[0] = piece.getVertices().get(0).getPosition();
        vertexPositions[1] = piece.getVertices().get(0).getPosition();
        vertexPositions[2] = piece.getVertices().get(0).getPosition();
        vertexPositions[3] = piece.getVertices().get(0).getPosition();

        Polygon boundingBox = piece.boundingBox();

        for (Vertex v : piece.getVertices())
        {
            vertexPositions[0] = closestPoint(v.getPosition(),vertexPositions[0],boundingBox.getVertices().get(0).getPosition());
            vertexPositions[1] = closestPoint(v.getPosition(),vertexPositions[1],boundingBox.getVertices().get(1).getPosition());
            vertexPositions[2] = closestPoint(v.getPosition(),vertexPositions[2],boundingBox.getVertices().get(2).getPosition());
            vertexPositions[3] = closestPoint(v.getPosition(),vertexPositions[3],boundingBox.getVertices().get(3).getPosition());
        }

        return null;
 */

