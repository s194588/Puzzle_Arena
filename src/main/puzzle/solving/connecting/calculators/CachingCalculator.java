package main.puzzle.solving.connecting.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;


//Class code by Frederik (s194619)
public class CachingCalculator implements IConnectionCalculator {

    protected Map<Long, List<PuzzleConnection>> cache;
    protected IPuzzle cachedPuzzle;
    protected IConnectionCalculator calculator;

    public CachingCalculator(IConnectionCalculator calculator)
    {
        this.calculator = calculator;
        cache = new HashMap<>();
    }

    /**
     * Initializes the cache.
     */
    public void initCache()
    {
        cachedPuzzle = getPuzzle();
        cache.clear();
        for (int i = 0; i < getPuzzle().getPieceCount(); i++) {
            for (int j = 0; j < getPuzzle().getPieceCount(); j++) {
                List<PuzzleConnection> cons = calculator.getConnections(j,i);
                cache.put(((long)j)*Integer.MAX_VALUE + i, cons);
            }
        }
        /*for (int i = 0; i < getPuzzle().getPieceCount(); i++) {
            for (int j = 0; j <= i; j++) {
                List<PuzzleConnection> cons = calculator.getConnections(j,i);
                cache.put(((long)j)*Integer.MAX_VALUE + i, cons);
            }
        }
        for (int i = 0; i < getPuzzle().getPieceCount(); i++) {
            for (int j = 0; j < i; j++) {
                List<PuzzleConnection> cons = new ArrayList<>();
                for (PuzzleConnection c : getConnections(j,i)){
                    cons.add(mirrorOf(c));
                }
                cache.put(((long)i)*Integer.MAX_VALUE + j, cons);
            }
        }*/
    }

    /**
     * Returns the list of puzzle connections. Also initializes the cache if it has not been done yet.
     */
    @Override
    public List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID) {
        if (cachedPuzzle == null)
        {
            initCache();
        }
        /*int largest = outlierPieceID;
        int smallest = targetPieceID;
        if (outlierPieceID < targetPieceID){
            largest = targetPieceID;
            smallest = outlierPieceID;
        }
        return cache.get(((long)smallest)*Integer.MAX_VALUE + largest);*/
        return cache.get(((long)outlierPieceID)*Integer.MAX_VALUE + targetPieceID);
    }

    @Override
    public IPuzzle getPuzzle() {
        return calculator.getPuzzle();
    }



    /*private PuzzleConnection mirrorOf(PuzzleConnection c){
        PuzzleConnection ret = new PuzzleConnection(c.getPuzzle(), c.targetID, c.outlierID, c.getPlacementID(), Vec.neg(c.deltaPos), c.deltaAngle+(float)Math.PI);
        ret.setTotalCount(c.getTotalCount());
        return ret;
    }*/
}
