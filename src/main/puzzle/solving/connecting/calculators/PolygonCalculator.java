package main.puzzle.solving.connecting.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.polygon.Edge;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;

//Class code by Frederik (s194619)
public class PolygonCalculator implements IConnectionCalculator{

    public PolygonCalculator(IPuzzle puzzle){
        this.puzzle = puzzle;
    }

    private IPuzzle puzzle;

    private boolean ignoreRotation;

    public void ignoreRotation(boolean ignoreRotation) {
        this.ignoreRotation = ignoreRotation;
    }
    private boolean ignoreOverlap;

    public void ignoreOverlap(boolean ignoreOverlap) {
        this.ignoreOverlap = ignoreOverlap;
    }

    @Override
    public List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID) {
        List<PuzzleConnection> cons = new LinkedList<>();
        if (outlierPieceID != targetPieceID){
            PuzzlePiece outlier = puzzle.getPiece(outlierPieceID);
            PuzzlePiece target = puzzle.getPiece(targetPieceID);

            //float originalAngleO = outlier.getAngle();
            //float originalAngleT = target.getAngle();


            int placementID = 0;

            for (Edge oe : outlier.getEdges()){
                for (Edge te : target.getEdges()){
                    Vec vec1 = Vec.sub(oe.getP2(),oe.getP1());
                    Vec vec2 = Vec.sub(te.getP2(),te.getP1());
                    if (Approx.areSameLength(vec1,vec2)){ //if (Approx.areEqual(vec1,vec2)){
//Approx.areParallel(vec1,vec2)

                        float angleA = Vec.angleBetween(vec2,vec1) + (float)Math.PI;

                        Vec v1A = Vec.average(Vec.sub(te.getVertex1().getPosition(), target.getPosition()), Vec.sub(te.getVertex2().getPosition(), target.getPosition()));
                        Vec v2A = Vec.average(Vec.sub(outlier.getPosition(), oe.getVertex1().getPosition()),Vec.sub(outlier.getPosition(), oe.getVertex2().getPosition()));


                        Vec deltaPosA = Vec.add(v1A, Matrix2.MVProd(Matrix2.rotMatrix(-(angleA-(float)Math.PI)),Vec.neg(v2A)));

                        /*System.out.println("-New-");
                        System.out.println("targetang: " + target.getAngle() + " disang: " + outlier.getAngle());
                        System.out.println(vec1 + ", " + vec2);
                        System.out.println(Vec.angleBetween(vec2,vec1));
                        System.out.println(Approx.areParallel(vec1,vec2));
                        System.out.println("-End-");*/

                        /*Vec deltaPosB = Vec.add(v1A, Vec.neg(v2A));
                        PuzzleConnection b = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, placementID, deltaPosB, angle+(float)Math.PI);
                        if (isValid(b)) {
                            //System.out.println("Added Mirror");
                            cons.add(b);
                            placementID++;
                        }*/

                        PuzzleConnection a = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, placementID, deltaPosA, angleA);
                        if (isValid(a))
                        {
                            cons.add(a);
                            placementID++;
                        }
                        //else{
                            //System.out.println("Excluded");
                        //}

                        //float angleB = Vec.angleBetween(vec2,vec1);
                        //Vec deltaPosB = Vec.add(v1A, Matrix2.MVProd(Matrix2.rotMatrix(-angleB+(float)Math.PI),Vec.neg(v2A)));
                        //PuzzleConnection b = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, placementID, deltaPosB, angleB);
                        //if (isValid(b))
                        //{
                            //cons.add(b);
                        //    placementID++;
                        //}
                    }
                }

            }
        }
        return reduce(cons);
    }

    private boolean isValid(PuzzleConnection con){
        return true; //overlapEst2(con);// !overlapEst(con);
    }

    @Override
    public IPuzzle getPuzzle() {
        return puzzle;
    }

    public List<PuzzleConnection> reduce(List<PuzzleConnection> cons)
    {
        List<Object[]> temp = new ArrayList<>();
        for (PuzzleConnection c : cons){
            boolean done = false;
            for (Object[] e : temp){
                PuzzleConnection ec = (PuzzleConnection) e[0];
                if (Approx.areEqual(c.deltaPos, ec.deltaPos) && Approx.areEqual(c.deltaAngle, ec.deltaAngle)){
                    done = true;
                    e[1] = ((Integer)(e[1]))+1;
                    break;
                }
            }
            if (!done)
                temp.add(new Object[]{c,1});
        }
        temp.sort(new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                int i1 = (int)(o1[1]);
                int i2 = (int)(o2[1]);
                return -Integer.compare(i1,i2);
            }
        });
        List<PuzzleConnection> ret = new ArrayList<>();
        for (Object[] e : temp) {
            PuzzleConnection c = (PuzzleConnection) e[0];
            c.setTotalCount((int)e[1]);
            //System.out.println("mc: " + c.totalCount());
            ret.add(c);
        }
        //System.out.println(cons.size() + ", " + ret.size());
        return ret;
    }

    private boolean overlapEst2(PuzzleConnection con)
    {
        con.tryApply();
        boolean ret = false;
        if (puzzle.getPiece(con.outlierID).fastIOE1(puzzle.getPiece(con.targetID))){
            ret = true;
        }
        con.tryUndo();
        return ret;
    }
}
/*
    @Override
    public List<PuzzleConnection> getConnections(IPuzzle puzzle, int outlierPieceID, int targetPieceID) {
        List<PuzzleConnection> cons = new LinkedList<>();
        if (outlierPieceID != targetPieceID){
            PU outlier = puzzle.getPiece(outlierPieceID);
            PU target = puzzle.getPiece(targetPieceID);

            for (Edge oe : outlier.getEdges()){
                for (Edge te : target.getEdges()){
                    Vec vec1 = Vec.sub(oe.getP2(),oe.getP1());
                    Vec vec2 = Vec.sub(te.getP2(),te.getP1());
                    if (Approx.areSameLength(vec1,vec2) && Approx.areParallel(vec1,vec2)){

                        float angle = 0; //Vec.angleBetween(vec1,vec2);

                        Vec v1A = Vec.sub(te.getVertex1().getPosition(), target.getPosition());
                        Vec v2A = Vec.sub(outlier.getPosition(), oe.getVertex2().getPosition()); //on B change to vert2 for oe
                        Vec deltaPosA = Vec.add(v1A, v2A);

                        PuzzleConnection a = new PuzzleConnection(puzzle, outlierPieceID, targetPieceID, deltaPosA, angle);
                        //a.tryApply();
                        //if (!target.insideOverlaps(outlier)){
                        cons.add(a);
                        //}
                        //a.tryUndo();
                    }
                }

            }
        }
        return cons;
    }
 */

/*

    private boolean overlapEst(PuzzleConnection con){
        PuzzlePiece p = getPuzzle().getPiece(con.targetID);
        if (0.5f*radius(p.getVertices(),p.getPosition()) < con.deltaPos.length()){
            return false;
        }
        return true;
    }

    private boolean overlapEst2(PuzzleConnection con)
    {
        con.tryApply();
        boolean ret = false;
        if (overlapIntersect(puzzle.getPiece(con.outlierID),puzzle.getPiece(con.targetID))){
            ret = true;
        }
        con.tryUndo();
        return ret;
    }

    private boolean overlapIntersect(PuzzlePiece p1, PuzzlePiece p2)
    {
        for (Edge e1 : p1.getEdges()){
            for (Edge e2 : p2.getEdges()){
                if (FiniteLine.intersects(e1,e2) && !Approx.areParallel(e1,e2))
                {
                    Vec point = FiniteLine.tryGetIntersection(e1,e2);
                    if (!(Approx.areEqual(point,e1.getP1()) && Approx.areEqual(point,e1.getP2())
                    && Approx.areEqual(point,e2.getP1()) && Approx.areEqual(point,e2.getP2()))){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private float radius(Collection<Vertex> vertices, Vec pos){
        float sqMag = 0;
        for (Vertex v : vertices){
            float sm = Vec.sub(v.getPosition(),pos).sqMag();
            if (sm > sqMag)
                sqMag = sm;
        }
        return (float)Math.sqrt(sqMag);
    }
 */

