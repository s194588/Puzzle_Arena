package main.puzzle.solving.connecting.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.connecting.PuzzleConnection;

import java.util.List;

//Interface code by Frederik (s194619)
public interface IConnectionCalculator {

    /**
     * Returns a list of possible ways to connect two pieces.
     */
    List<PuzzleConnection> getConnections(int outlierPieceID, int targetPieceID);
    IPuzzle getPuzzle();
}
