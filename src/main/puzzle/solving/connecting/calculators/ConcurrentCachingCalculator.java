package main.puzzle.solving.connecting.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.general.GeneralUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

//Class code by Frederik (s194619)
public class ConcurrentCachingCalculator extends CachingCalculator {

    public ConcurrentCachingCalculator(IConnectionCalculator calculator, int threadCount)
    {
        super(calculator);
        cache = new ConcurrentHashMap<>();
        this.threadCount = threadCount;
    }

    private int threadCount;

    @Override
    public void initCache() {
        cachedPuzzle = getPuzzle();
        cache.clear();

        Object lock = new Object();
        Set<Integer> lockedJ = new ConcurrentSkipListSet<>();

        int T = (getPuzzle().getPieceCount()* getPuzzle().getPieceCount() + getPuzzle().getPieceCount())/2;

        List<Thread> threads = new ArrayList<>();

        int s = 0;

        for (int c = 0; c < threadCount; c++){
            int e = GeneralUtil.clamp(f(T,threadCount,c+1), 0, getPuzzle().getPieceCount());
            int finalS = s;
            Runnable fun = new Runnable() {
                @Override
                public void run() {
                    for (int i = finalS; i < e; i++){
                        for (int j = 0; j <= i; j++){
                            PuzzlePiece outlier = null;
                            PuzzlePiece target = null;
                            if (lockedJ.contains(j)){
                                synchronized (lockedJ) { //DOES THIS AND FURTHER DOWN LOCK?
                                    try {
                                        lockedJ.wait();

                                    } catch (InterruptedException interruptedException) {
                                        interruptedException.printStackTrace();
                                    }
                                }
                            }
                            lockedJ.add(j);
                            synchronized (lock) {
                                outlier = getPuzzle().getPiece(i);
                                target = getPuzzle().getPiece(j);
                            }

                            List<PuzzleConnection> cons = calculator.getConnections(i,j);
                            lockedJ.remove(j);
                            synchronized (lockedJ) { //DOES THIS AND FURTHER UP LOCK?
                                lockedJ.notifyAll();
                            }

                            cache.put(((long)j)*Integer.MAX_VALUE + i, cons);
                        }
                    }
                }
            };
            Thread x = new Thread(fun);
            threads.add(x);
            x.start();
            s=e;
        }
        try {
            for (int i = 0; i < threads.size(); i++) {
                if (threads.get(i).isAlive()) {
                    i--;
                    Thread.sleep(100);
                }
            }
        }catch (Exception e){
            System.out.println("Error in concurrent calculator: " + e.getMessage());
        }
    }
    private int f(float T, float n, float c)
    {
        return (int)Math.round(n+Math.sqrt(8*n*c*T+n*n)/(2*n));
    }
}
/*
package main.puzzle.solving.connecting.calculators;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.general.GeneralUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

public class ConcurrentCachingCalculator extends CachingCalculator {

    public ConcurrentCachingCalculator(IConnectionCalculator calculator, int threadCount)
    {
        super(calculator);
        cache = new ConcurrentHashMap<>();
        this.threadCount = threadCount;
    }

    private int threadCount;

    @Override
    public void initCache() {
        cachedPuzzle = getPuzzle();
        cache.clear();

        Object lock = new Object();
        Set<Integer> lockedJ = new ConcurrentSkipListSet<>();

        int T = (getPuzzle().getPieceCount()* getPuzzle().getPieceCount() + getPuzzle().getPieceCount())/2;

        List<Thread> threads = new ArrayList<>();

        int s = 0;

        for (int c = 0; c < threadCount; c++){
            int e = GeneralUtil.clamp(f(T,threadCount,c+1), 0, getPuzzle().getPieceCount());
            int finalS = s;
            Runnable fun = new Runnable() {
                @Override
                public void run() {
                    for (int i = finalS; i < e; i++){
                        for (int j = 0; j <= i; j++){
                            PuzzlePiece outlier = null;
                            PuzzlePiece target = null;
                            if (lockedJ.contains(j)){
                                synchronized (lockedJ) { //DOES THIS AND FURTHER DOWN LOCK?
                                    try {
                                        lockedJ.wait();

                                    } catch (InterruptedException interruptedException) {
                                        interruptedException.printStackTrace();
                                    }
                                }
                            }
                            lockedJ.add(j);
                            synchronized (lock) {
                                outlier = getPuzzle().getPiece(i);
                                target = getPuzzle().getPiece(j);
                            }

                            List<PuzzleConnection> cons = calculator.getConnections(i,j);
                            lockedJ.remove(j);
                            synchronized (lockedJ) { //DOES THIS AND FURTHER UP LOCK?
                                lockedJ.notifyAll();
                            }

                            cache.put(((long)j)*Integer.MAX_VALUE + i, cons);
                        }
                    }
                }
            };
            Thread x = new Thread(fun);
            threads.add(x);
            x.start();
            s=e;
        }
        try {
            for (int i = 0; i < threads.size(); i++) {
                if (threads.get(i).isAlive()) {
                    i--;
                    Thread.sleep(100);
                }
            }
        }catch (Exception e){
            System.out.println("Error in concurrent calculator: " + e.getMessage());
        }
    }
    private int f(float T, float n, float c)
    {
        return (int)Math.round(n+Math.sqrt(8*n*c*T+n*n)/(2*n));
    }
}

 */