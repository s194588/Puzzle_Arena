package main.puzzle.solving.connecting;

import main.puzzle.logic.IFigure;
import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;


//Class code by Frederik (s194619)

public class PuzzleConnection {

    /**
     *
     * @param deltaPos The positional offset of the outlier piece relative to the target piece.
     * @param deltaAngle The angle of the outlier piece relative to the target piece.
     */
    public PuzzleConnection(IPuzzle puzzle, int outlierPieceID, int targetPieceID, int placementID, Vec deltaPos, float deltaAngle)
    {
        init(puzzle,outlierPieceID,targetPieceID,placementID,deltaPos,deltaAngle);
    }
    public PuzzleConnection(IPuzzle puzzle, int outlierPieceID, int targetPieceID, Vec deltaPos, float deltaAngle)
    {
        init(puzzle,outlierPieceID,targetPieceID,0,deltaPos,deltaAngle);
    }
    private void init(IPuzzle puzzle, int outlierPieceID, int targetPieceID, int placementID, Vec deltaPos, float deltaAngle){
        this.puzzle = puzzle;
        this.outlierID = outlierPieceID; this.targetID = targetPieceID;
        this.deltaPos = deltaPos; this.deltaAngle = deltaAngle;
        this.placementID = placementID;

        this.prevPosOutlier = puzzle.getPiece(outlierID).getPosition();
        this.prevAngOutlier = puzzle.getPiece(outlierID).getAngle();
    }

    private int totalCount = 0;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    private IPuzzle puzzle;

    public int outlierID, targetID;
    private int placementID;

    public int getPlacementID(){
        return placementID;
    }

    public Vec commonPointBetweenPuzzles;

    public  Vec deltaPos;
    public  float deltaAngle;

    public void setPuzzle(IPuzzle puzzle) {
        init(puzzle,outlierID,targetID,placementID,deltaPos,deltaAngle);
    }

    public IPuzzle getPuzzle() {
        return puzzle;
    }

    public boolean tryApply()
    {
        PuzzlePiece figOutsider = puzzle.getPiece(outlierID);
        PuzzlePiece figTarget = puzzle.getPiece(targetID);

        Vec pos = figOutsider.getPosition();
        float ang = figOutsider.getAngle();
        Matrix2 rot = Matrix2.rotMatrix(figTarget.getAngle());
        Vec rotatedDeltaPos = Matrix2.MVProd(rot, deltaPos);

        figOutsider.setPosition(Vec.add(figTarget.getPosition(), rotatedDeltaPos));
        //System.out.println(figTarget.getAngle() + " Delta: " + deltaAngle);
        figOutsider.setAngle(figTarget.getAngle() - deltaAngle); //TODO: account for outlierGetAngle and targetGetAngle
        //System.out.println(figTarget.getAngle());

        if (puzzleOverlap(puzzle)) {
            figOutsider.setPosition(pos);
            figOutsider.setAngle(ang);
            return false;
        }

        return true;

    }

    public Vec resultingPos()
    {
        PuzzlePiece figOutsider = puzzle.getPiece(outlierID);
        PuzzlePiece figTarget = puzzle.getPiece(targetID);
        Matrix2 rot = Matrix2.rotMatrix(figTarget.getAngle());
        Vec rotatedDeltaPos = Matrix2.MVProd(rot, deltaPos);
        return Vec.add(figTarget.getPosition(), rotatedDeltaPos);
    }

    public float resultingAngle()
    {
        PuzzlePiece figOutsider = puzzle.getPiece(outlierID);
        PuzzlePiece figTarget = puzzle.getPiece(targetID);
        return figTarget.getAngle() - deltaAngle;
    }

    public boolean eqv(PuzzleConnection c)
    {
        return Approx.areEqual(resultingPos(),c.resultingPos())
                && Approx.areEqual(resultingAngle(), c.resultingAngle());
    }

    private Vec prevPosOutlier, prevPosTarget;
    private float prevAngOutlier, prevAngTarget;

    public boolean tryUndo()
    {
        IFigure figOutsider = puzzle.getPiece(outlierID);
        IFigure figTarget = puzzle.getPiece(targetID);
        Vec pos = figOutsider.getPosition();
        float ang = figOutsider.getAngle();

        figOutsider.setPosition(prevPosOutlier);
        figOutsider.setAngle(prevAngOutlier);

        if (puzzleOverlap(puzzle)) {
            figOutsider.setPosition(pos);
            figOutsider.setAngle(ang);
            return false;
        }

        return true;
    }

    private boolean puzzleOverlap(IPuzzle p)
    {
        return false;
        /*for (int i = 0; i < p.getPieceCount(); i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (p.getPiece(i).insideOverlaps(p.getPiece(j))){
                    return true;
                }
            }
        }
        return false;*/
    }


    public static void orderByCount(List<PuzzleConnection> cons){
        Comparator<PuzzleConnection> comparator = new Comparator<PuzzleConnection>() {
            @Override
            public int compare(PuzzleConnection o1, PuzzleConnection o2) {
                return -Integer.compare(o1.getTotalCount(),o2.getTotalCount());
            }
        };
        cons.sort(comparator);
    }
    public static List<PuzzleConnection> reducedEqvCount(List<PuzzleConnection> cons){
        List<Object[]> temp = new ArrayList<>();
        for (PuzzleConnection c : cons){
            boolean b = true;
            for (Object[] o : temp){
                if (c.eqv((PuzzleConnection)o[1])){
                    o[0] = ((int)o[0]) + c.totalCount;
                    b = false;
                    break;
                }
            }
            if (b)
                temp.add(new Object[]{c.totalCount, c});
        }
        temp.sort(new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                return -Integer.compare((int)o1[0],(int)o2[0]);
            }
        });
        List<PuzzleConnection> ret = new ArrayList<>();
        for (Object[] o : temp){
            ret.add((PuzzleConnection) o[1]);
        }
        //System.out.println("ret: " + ret.size() + " cons: " + cons.size());
        return ret;
    }

    public static List<PuzzleConnection> mergeEqv(List<PuzzleConnection> cons)
    {

        Map<PuzzleConnection,Integer> m = new HashMap<>();
        for (PuzzleConnection c : cons){
            Object comp = new Object(){
                @Override
                public boolean equals(Object obj) {
                    return c.eqv((PuzzleConnection) obj);
                }

                @Override
                public int hashCode() {
                    return c.hashCode();
                }
            };
            Integer x = m.remove(comp);
            if (x == null)
                x = 0;

            m.put(c, x + c.totalCount);
        }
        List<PuzzleConnection> ret = new ArrayList<>();
        for (Map.Entry<PuzzleConnection,Integer> entry : m.entrySet()){
            PuzzleConnection c = entry.getKey();
            c.totalCount = entry.getValue();
            ret.add(c);
        }
        return ret;
    }

    public static PuzzleConnection copyOf(PuzzleConnection c){
        return new PuzzleConnection(c.puzzle,c.outlierID,c.targetID,c.placementID,c.deltaPos,c.deltaAngle);
    }

    /*
        public List<Vertex> getBorderVertices (){
        List<Vertex> borderVertices = new ArrayList<Vertex>();
        List<Vertex> targetPieceVertices =  puzzle.getPiece(targetID).getVertices();
        List<Vertex> outlierPieceVertices =  puzzle.getPiece(outlierID).getVertices();

        for(Vertex targetVertex : targetPieceVertices) {
            for(Vertex outlierVertex : outlierPieceVertices) {
                if(targetVertex.getPosition().x == (outlierVertex.getPosition().x - deltaPos.x) &&
                        targetVertex.getPosition().y == (outlierVertex.getPosition().y - deltaPos.y)){
                    continue;
                }

            }
        }
//
//        for(Vertex vertex : outlierPieceVertices) {
//            if (!targetPieceVertices.contains(vertex)){
//                borderVertices.add(vertex);
//            }
//        }
        return borderVertices;
    }
    public void printBorderVertices() {
        System.out.println("Border vertices below:");
        for(Vertex v : this.getBorderVertices()){
            System.out.println("(" + v.getPosition().x + "," + v.getPosition().y + ")");
        }

    }
     */
}
