package main.puzzle.solving.algorithms.strategies;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.algorithms.skeleton.SolvingStrategy;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.polygon.PolygonUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


//Class code by Frederik (s194619)
public class PriorityStrategy extends SolvingStrategy {
    public PriorityStrategy(IConnectionCalculator calculator){
        this.calculator = calculator;
    }
    private IConnectionCalculator calculator;

    private Set<Integer> matched = new HashSet<>();

    @Override
    public void setup(IPuzzle puzzle) {
        super.setup(puzzle);
        matched.clear();
        matched.add(getInitial());
        //calculator.getPuzzle().getPiece(getInitial()).setAngle(-slowNormalizedAngle(getInitial())); //+(float)Math.PI/4f
    }

    @Override
    public List<PuzzleConnection> possibleSteps() {
        List<PuzzleConnection> ret = new ArrayList<>();
        for (int i : getRemaining()) {
            for (int j : matched) {
                List<PuzzleConnection> cons = calculator.getConnections(i, j);
                ret.addAll(cons);
            }
        }
        PuzzleConnection.orderByCount(ret);
        return ret;
        //List<PuzzleConnection> r = PuzzleConnection.mergeEqv(ret);
        //PuzzleConnection.orderByCount(r);
        //return r;
    }

    @Override
    public void backtrack(PuzzleConnection pop) {
        matched.remove(pop.outlierID);
        super.backtrack(pop);
    }

    @Override
    public void advance(PuzzleConnection push) {
        matched.add(push.outlierID);
        super.advance(push);
    }


    public float fastNormalizedAngle(int id)
    {
        List<Float> angles = new ArrayList<>();
        for (int i = 0; i < calculator.getPuzzle().getPieceCount(); i++)
        {
            if (i == id)
                continue;
            for (PuzzleConnection c : calculator.getConnections(i, id))
            {
                float a = Vec.angleBetween(new Vec(1,0), c.deltaPos);
                System.out.println(a);
                int sign = a > 0 ? 1 : (a < 0 ? -1 : 0);
                float am = Math.abs(a) % ((float) Math.PI/2f);
                float res = sign * Math.min(am, Math.abs(am - (float) Math.PI/2f));
                System.out.println(res);
                angles.add(res);
            }
        }
        float ret = 0;
        for (float f : angles)
            ret += f;
        ret /= angles.size();
        return ret;
    }


    public float slowNormalizedAngle(int id)
    {
        PuzzlePiece p = calculator.getPuzzle().getPiece(id);
        float startingAngle = p.getAngle();
        float angle = startingAngle;
        float area = PolygonUtil.rectArea(p.boundingBox());
        for (float a = 0; a < 2*Math.PI; a+= Math.PI/360f){
            calculator.getPuzzle().getPiece(id).setAngle(a);
            float candidate = PolygonUtil.rectArea(p.boundingBox());
            if (candidate < area){
                area = candidate;
                angle = a;
            }
        }
        p.setAngle(startingAngle);
        return -angle;
    }
}

//float a1 = Vec.angleBetween(new Vec(1,0), c.deltaPos);
//float a2 = Vec.angleBetween(new Vec(0,1), c.deltaPos);
//float a3 = Vec.angleBetween(new Vec(-1,0), c.deltaPos);
//float a4 = Vec.angleBetween(new Vec(0,-1), c.deltaPos);
//float m = a1 % (float) Math.PI/2f;
//if (Math.abs(m-(float)Math.PI/4f) < m)
//    m -= (float)Math.PI/2f;
//angles.add(m);
//System.out.println("m: " + m);
//System.out.println("min: " + Math.min(Math.min(a1,a2), Math.min(a3,a4)));
//System.out.println("dp: " + c.deltaPos);

//float a1 = Vec.angleBetween(new Vec(1,0), c.deltaPos);
//float a2 = Vec.angleBetween(c.deltaPos, new Vec(1,0));
//System.out.println((a1) + " / " + (a2));
//System.out.println((a1 % (float) Math.PI/2f) + " // " + (-a2 % (float) Math.PI/2f));
//if (a2 < a1)
//    angles.add(-(a2));
//else
//    angles.add(a1);

