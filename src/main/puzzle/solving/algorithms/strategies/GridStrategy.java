package main.puzzle.solving.algorithms.strategies;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.algorithms.skeleton.SolvingStrategy;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGrid;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGridNode;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;

//Class code by Frederik (s194619)
import java.util.*;

/**
 * Under development - to be completed.
 */
public class GridStrategy extends SolvingStrategy {
    @Override
    public List<PuzzleConnection> possibleSteps() {
        return null;
    }

    @Override
    public void backtrack(PuzzleConnection pop) {
        super.backtrack(pop);

    }

    @Override
    public void advance(PuzzleConnection push) {
        super.advance(push);

    }


    private final Comparator<LoopingGridNode> comparator = new Comparator<LoopingGridNode>() {
        @Override
        public int compare(LoopingGridNode o1, LoopingGridNode o2) {
            return -Integer.compare(count(o1), count(o2));
        }
        private int count(LoopingGridNode o){
            int res = 0;
            for (LoopingGridNode n : o.plusNeighbors()){
                if (n.id != null)
                    res++;
            }
            return res;
        }
    };


    private int width, height;
    private LoopingGrid<LoopingGridNode> grid;
    //Heap invariant: always contains grid nodes where id is null.
    private PriorityQueue<LoopingGridNode> heap = new PriorityQueue<>(comparator);

    private IPuzzle puzzle;
    private IConnectionCalculator calculator;

    private int p0;

    public GridStrategy(IConnectionCalculator calculator){
        this.calculator = calculator;
    }

    public boolean solve(IPuzzle puzzle) {
        this.puzzle = puzzle;
        setup();
        while (!getRemaining().isEmpty()){
            LoopingGridNode node = heap.poll();
            System.out.println("polled: (" +node.x + ", " + node.y + ")");
            if (heap.isEmpty())
                return false;
            PuzzleConnection next = findFit(node);
            System.out.println("fit: " + (next != null?next.outlierID:null));
            if (next == null) {
                /*System.out.println("Failed: " + remaining.size());
                System.out.println(calculator.getConnections(puzzle,7,0).size());
                System.out.println(calculator.getConnections(puzzle,2,0).get(0).deltaPos + " | " + getQuadrant(calculator.getConnections(puzzle,2,0).get(0).deltaPos, (float)Math.PI/4f));
                System.out.println(node.x + ", " + node.y);
                System.out.println(grid.get(2,0).id);
                for (GridNode n : heap){
                    System.out.println("("+n.x+", "+n.y+") -> " + testcount(n));
                }
                System.out.println("****");

                for (GridNode n : node.plusNeighbors()){
                    System.out.println(n.id);
                }
                //return false;*/
            }else {
                next.tryApply();
                put(node, next.outlierID);
            }
        }
        //centralize();
        return true;
    }

    private void setup(){
        p0 = getInitial();
        width = puzzle.getPieceCount() + 1;
        height = puzzle.getPieceCount() + 1;
        heap.clear();
        getRemaining().clear();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            if (i != p0)
                getRemaining().add(i);
        }

        int dimx = width, dimy = height;
        LoopingGridNode[][] g = new LoopingGridNode[dimx][dimy];
        grid = new LoopingGrid<>(g,width,height);
        for (int y = 0; y < dimy; y++){
            for (int x = 0; x < dimx; x++){
                g[x][y] = new LoopingGridNode(grid, x,y);
            }
        }
        put(grid.get(0,0), p0);
    }

    private void put(LoopingGridNode node, int id)
    {
        List<LoopingGridNode> neighbors = node.plusNeighbors();
        for (LoopingGridNode n : neighbors) {
            heap.remove(n);
        }
        node.id = id;
        heap.remove(node);
        getRemaining().remove(id);

        for (LoopingGridNode n : neighbors) {
            if (n.id == null)
                heap.add(n);
        }
    }

    private PuzzleConnection findFit(LoopingGridNode node){
        for (int p : getRemaining()){
            PuzzleConnection fit = getConnectionFit(node, p);
            if (fit != null)
                return fit;
        }
        return null;
    }

    private PuzzleConnection getConnectionFit(LoopingGridNode node, int id)
    {

        LoopingGridNode up = grid.get(node.x, node.y+1);
        LoopingGridNode down = grid.get(node.x, node.y-1);
        LoopingGridNode right = grid.get(node.x+1, node.y);
        LoopingGridNode left = grid.get(node.x-1, node.y);

        boolean x = false;
        if (up.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, up.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 3) {
                    return c;
                }
            }
            return null;
        }
        if (down.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, down.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 1) {
                    return c;
                }
            }
            return null;
        }
        if (right.id != null) {
            x= true;
            List<PuzzleConnection> cons = calculator.getConnections(id, right.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 2) {
                    return c;
                }
            }
            return null;
        }
        if (left.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, left.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 4) {
                    return c;
                }
            }
            return null;
        }
        return null;
    }

    public int getQuadrant(Vec v, float offsetAngle){
        Vec base = Vec.polarVec(offsetAngle,1);
        float angle = Vec.angleBetween(base, v);
        if (angle >= 0 && angle < Math.PI/2)
            return 1;
        if (angle >= Math.PI/2 && angle < Math.PI)
            return 2;
        if (angle >= Math.PI && angle < 3*Math.PI/2)
            return 3;
        if (angle >= 3*Math.PI/2 && angle < 2*Math.PI)
            return 4;
        throw new RuntimeException("Something went wrong");
    }
}

/*
package main.puzzle.solving.algorithms.strategies;

import main.puzzle.logic.IPuzzle;
import main.puzzle.solving.algorithms.skeleton.SolvingStrategy;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGrid;
import main.puzzle.solving.algorithms.strategies.grid.LoopingGridNode;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.solving.connecting.calculators.IConnectionCalculator;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.*;

public class GridStrategy extends SolvingStrategy {
    @Override
    public List<PuzzleConnection> possibleSteps() {
        return null;
    }

    @Override
    public void backtrack(PuzzleConnection pop) {
        super.backtrack(pop);

    }

    @Override
    public void advance(PuzzleConnection push) {
        super.advance(push);

    }


    private final Comparator<LoopingGridNode> comparator = new Comparator<LoopingGridNode>() {
        @Override
        public int compare(LoopingGridNode o1, LoopingGridNode o2) {
            return -Integer.compare(count(o1), count(o2));
        }
        private int count(LoopingGridNode o){
            int res = 0;
            for (LoopingGridNode n : o.plusNeighbors()){
                if (n.id != null)
                    res++;
            }
            return res;
        }
    };


    private int width, height;
    private LoopingGrid<LoopingGridNode> grid;
    //Heap invariant: always contains grid nodes where id is null.
    private PriorityQueue<LoopingGridNode> heap = new PriorityQueue<>(comparator);

    private IPuzzle puzzle;
    private IConnectionCalculator calculator;

    private int p0;

    public GridStrategy(IConnectionCalculator calculator){
        this.calculator = calculator;
    }

    public boolean solve(IPuzzle puzzle) {
        this.puzzle = puzzle;
        setup();
        while (!getRemaining().isEmpty()){
            LoopingGridNode node = heap.poll();
            System.out.println("polled: (" +node.x + ", " + node.y + ")");
            if (heap.isEmpty())
                return false;
            PuzzleConnection next = findFit(node);
            System.out.println("fit: " + (next != null?next.outlierID:null));
            if (next == null) {
                //System.out.println("Failed: " + remaining.size());
                //System.out.println(calculator.getConnections(puzzle,7,0).size());
                //System.out.println(calculator.getConnections(puzzle,2,0).get(0).deltaPos + " | " + getQuadrant(calculator.getConnections(puzzle,2,0).get(0).deltaPos, (float)Math.PI/4f));
                //System.out.println(node.x + ", " + node.y);
                //System.out.println(grid.get(2,0).id);
                //for (GridNode n : heap){
                //    System.out.println("("+n.x+", "+n.y+") -> " + testcount(n));
                //}
                //System.out.println("****");

                //for (GridNode n : node.plusNeighbors()){
                //    System.out.println(n.id);
                //}
                //return false
            }else {
                next.tryApply();
                put(node, next.outlierID);
            }
        }
        //centralize();
        return true;
    }

    private void setup(){
        p0 = getInitial();
        width = puzzle.getPieceCount() + 1;
        height = puzzle.getPieceCount() + 1;
        heap.clear();
        getRemaining().clear();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            if (i != p0)
                getRemaining().add(i);
        }

        int dimx = width, dimy = height;
        LoopingGridNode[][] g = new LoopingGridNode[dimx][dimy];
        grid = new LoopingGrid<>(g,width,height);
        for (int y = 0; y < dimy; y++){
            for (int x = 0; x < dimx; x++){
                g[x][y] = new LoopingGridNode(grid, x,y);
            }
        }
        put(grid.get(0,0), p0);
    }

    private void put(LoopingGridNode node, int id)
    {
        List<LoopingGridNode> neighbors = node.plusNeighbors();
        for (LoopingGridNode n : neighbors) {
            heap.remove(n);
        }
        node.id = id;
        heap.remove(node);
        getRemaining().remove(id);

        for (LoopingGridNode n : neighbors) {
            if (n.id == null)
                heap.add(n);
        }
    }

    private PuzzleConnection findFit(LoopingGridNode node){
        for (int p : getRemaining()){
            PuzzleConnection fit = getConnectionFit(node, p);
            if (fit != null)
                return fit;
        }
        return null;
    }

    private PuzzleConnection getConnectionFit(LoopingGridNode node, int id)
    {

        LoopingGridNode up = grid.get(node.x, node.y+1);
        LoopingGridNode down = grid.get(node.x, node.y-1);
        LoopingGridNode right = grid.get(node.x+1, node.y);
        LoopingGridNode left = grid.get(node.x-1, node.y);

        boolean x = false;
        if (up.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, up.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 3) {
                    return c;
                }
            }
            return null;
        }
        if (down.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, down.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 1) {
                    return c;
                }
            }
            return null;
        }
        if (right.id != null) {
            x= true;
            List<PuzzleConnection> cons = calculator.getConnections(id, right.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 2) {
                    return c;
                }
            }
            return null;
        }
        if (left.id != null) {
            x = true;
            List<PuzzleConnection> cons = calculator.getConnections(id, left.id);
            for (PuzzleConnection c : cons) {
                if (getQuadrant(c.deltaPos, (float) Math.PI / 4f) == 4) {
                    return c;
                }
            }
            return null;
        }
        return null;
    }

    public int getQuadrant(Vec v, float offsetAngle){
        Vec base = Vec.polarVec(offsetAngle,1);
        float angle = Vec.angleBetween(base, v);
        if (angle >= 0 && angle < Math.PI/2)
            return 1;
        if (angle >= Math.PI/2 && angle < Math.PI)
            return 2;
        if (angle >= Math.PI && angle < 3*Math.PI/2)
            return 3;
        if (angle >= 3*Math.PI/2 && angle < 2*Math.PI)
            return 4;
        throw new RuntimeException("Something went wrong");
    }
}

 */
