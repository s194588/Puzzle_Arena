package main.puzzle.solving.algorithms.strategies.grid;

import java.util.ArrayList;
import java.util.List;

//Class code by Frederik (s194619)
public class LoopingGridNode {
    public int x, y;
    private LoopingGrid<LoopingGridNode> grid;
    public LoopingGridNode(LoopingGrid<LoopingGridNode> grid, int x, int y) {
        this.grid = grid;
        this.x = x; this.y = y;
    }

    public List<LoopingGridNode> plusNeighbors(){
        List<LoopingGridNode> ret = new ArrayList<>();

        ret.add(grid.get(x+1,y));
        ret.add(grid.get(x-1,y));
        ret.add(grid.get(x,y+1));
        ret.add(grid.get(x,y-1));

        /*ret.add(grid[GeneralUtil.loop(x+1, 0, w)][y]);
        ret.add(grid[GeneralUtil.loop(x-1, 0, w)][y]);
        ret.add(grid[x][GeneralUtil.loop(y+1, 0, h)]);
        ret.add(grid[x][GeneralUtil.loop(y-1, 0, h)]);*/
        return ret;
    }
    public List<LoopingGridNode> diagNeighbors(){
        List<LoopingGridNode> ret = new ArrayList<>();

        ret.add(grid.get(x+1,y+1));
        ret.add(grid.get(x-1,y+1));
        ret.add(grid.get(x+1,y-1));
        ret.add(grid.get(x-1,y-1));

        return ret;
    }
    public List<LoopingGridNode> sqNeighbors(){
        List<LoopingGridNode> ret = plusNeighbors();
        ret.addAll(diagNeighbors());
        return ret;
    }

    public Integer id;
}
