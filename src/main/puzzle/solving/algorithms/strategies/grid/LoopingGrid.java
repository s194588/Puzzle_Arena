package main.puzzle.solving.algorithms.strategies.grid;

import main.puzzle.util.general.GeneralUtil;

//Class code by Frederik (s194619)
public class LoopingGrid<Type>
{
    private Type[][] grid;
    private int width, height;
    public LoopingGrid(Type[][] grid, int width, int height)
    {
        this.grid = grid;
        this.height = height;
        this.width = width;
    }
    public Type get(int x, int y){
        return grid[GeneralUtil.loop(x,0,width)][GeneralUtil.loop(y,0,height)];
    }
    public void set(int x, int y, Type value){
        grid[GeneralUtil.loop(x,0,width)][GeneralUtil.loop(y,0,height)] = value;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int stretch(int x1, int y1, int x2, int y2){
        int dx = Math.abs(GeneralUtil.loop(x1,0,width) - GeneralUtil.loop(x2,0,width));
        int dy = Math.abs(GeneralUtil.loop(y1,0,height) - GeneralUtil.loop(y2,0,height));
        return Math.max(dx,dy);
    }
}