package main.puzzle.solving.algorithms;
import main.puzzle.logic.IPuzzle;


//Interface code by Frederik (s194619)
public interface ISolvingAlgorithm {
    boolean solve(IPuzzle p);
}
