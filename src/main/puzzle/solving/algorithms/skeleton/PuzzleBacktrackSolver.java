package main.puzzle.solving.algorithms.skeleton;

import main.puzzle.logic.IPuzzle;
import main.puzzle.logic.PuzzlePiece;
import main.puzzle.solving.algorithms.ISolvingAlgorithm;
import main.puzzle.util.algorithms.backtracking.BacktrackAlgorithm;
import main.puzzle.solving.connecting.PuzzleConnection;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.polygon.Edge;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.PolygonUtil;
import main.puzzle.util.geometry.polygon.Vertex;

import java.util.*;

//Class code by Frederik (s194619)
public class PuzzleBacktrackSolver extends BacktrackAlgorithm<PuzzleConnection> implements ISolvingAlgorithm {
    public PuzzleBacktrackSolver(SolvingStrategy solvingStrategy) {
        super(solvingStrategy);
        this.solvingStrategy = solvingStrategy;
        //calculator = connectionCalculator;
        /*backTracker = new IBackTracker<PuzzleConnection>() {
            @Override
            public List<PuzzleConnection> possibleSteps() {
                System.out.println("X: " + remaining.size());
                List<PuzzleConnection> ret = new ArrayList<>();
                for (int i : remaining) {
                    for (int j : che) {
                        List<PuzzleConnection> cons = connectionCalculator.getConnections(puzzle, i, j);
                        ret.addAll(cons);
                    }
                }
                return ret;
            }
        };*/
    }
    private IPuzzle puzzle;
    private SolvingStrategy solvingStrategy;
    private Collection<PuzzlePiece> matched = new HashSet<>();
    //private Set<ConnectionPairState> checkedStates = new HashSet<>();

    private float marginOfError = 0.01f;


    //private Map<Integer, Set<Integer>> connected = new HashMap<>();

    @Override
    protected void setup() {
        super.setup();

        //connected.clear();
        //for (int i = 0; i < puzzle.getPieceCount(); i++){
        //    connected.put(i, new HashSet<>());
        //}

        matched.clear();
        PuzzlePiece initial = puzzle.getPiece(solvingStrategy.getInitial());
        matched.add(initial);

        //initial.setAngle(getNormalizedAngle(solvingStrategy.getInitial()));

        solvingStrategy.setup(puzzle);
    }

    @Override
    protected boolean isValid() {
        return true;//!fastOverlap();//true;//!fastOverlap();//legalOverlapEstimate(); //&& legalArea();
    }

    //private boolean under4()
    //{
    //    if (getStack().isEmpty())
    //        return true;
    //    int last = getStack().peek().targetID;
    //    if (connected.get(last).size() > 4){
    //        return false;
    //    }
    //    return true;
    //}

    protected boolean legalArea(){
        Polygon box = Polygon.boundingBox(matched);
        return PolygonUtil.rectArea(box) <= PolygonUtil.rectArea(puzzle.getForm()) * (1 + marginOfError);
    }

    @Override
    protected boolean done() {
        return isValid() && solvingStrategy.getRemaining().isEmpty();
    }

    @Override
    public boolean solve(IPuzzle p) {
        puzzle = p;
        boolean res = run();
        rotateToFit();
        centralize();
        //printStack();
        return res;
    }
    private boolean fastOverlap()
    {
        if (getStack().isEmpty()){
            return false;
        }
        PuzzleConnection last = getStack().peek();
        PuzzlePiece x = puzzle.getPiece(last.outlierID);
        for (PuzzlePiece p : matched){
            if (x != p && x.fastIOE2(p)){
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onBacktrack(PuzzleConnection pop) {
        matched.remove(puzzle.getPiece(pop.outlierID));
    }

    @Override
    protected void onAdvance(PuzzleConnection push) {
        matched.add(puzzle.getPiece(push.outlierID));
        //for (PuzzleConnection c : getStack()){
        //    if (c != push){
        //        List<PuzzleConnection> cons =
        //    }
        //}
    }

    public void centralize()
    {
        List<Polygon> s = new LinkedList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            s.add(puzzle.getPiece(i));
        }
        Polygon boundingBox = Polygon.boundingBox(s);
        Vec bpos = Vec.average(boundingBox.getVerticesAsVecs());
        Vec v = new Vec(0,0);
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            Vec offset = Vec.sub(puzzle.getPiece(i).getPosition(), bpos);
            puzzle.getPiece(i).setPosition(Vec.add(v,offset));
        }
    }

    public void printStack(){
        for (PuzzleConnection c : getStack())
            System.out.println(c.outlierID + " and " + c.targetID);
    }

    public void rotateToFit()
    {
        List<Polygon> pieces = new ArrayList<>();
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            pieces.add(puzzle.getPiece(i));
        }
        Polygon box = Polygon.tightBox(pieces);
        Edge be1 = box.getEdges()[0];
        Edge be2 = box.getEdges()[1];
        Edge bmax = FiniteLine.lengthOf(be1) > FiniteLine.lengthOf(be2) ? be1 : be2;

        Edge fe1 = puzzle.getForm().getEdges()[0];
        Edge fe2 = puzzle.getForm().getEdges()[1];
        Edge fmax = FiniteLine.lengthOf(fe1) > FiniteLine.lengthOf(fe2) ? fe1 : fe2;

        //System.out.println(Vec.sub(bmax.getP2(), bmax.getP1()));
        float angle = Vec.angleBetween(Vec.sub(bmax.getP2(), bmax.getP1()),Vec.sub(fmax.getP2(), fmax.getP1()));
        //System.out.println("Angle: " + angle);
        Vec pointOfRot = Vec.zero;//Vec.average(box.getVerticesAsVecs());
        //System.out.println(Arrays.toString(box.getVerticesAsVecs().toArray()));
        //System.out.println(Matrix2.rotMatrix(0 - (float)Math.PI));
        //System.out.println(Matrix2.rotMatrix(0));
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            puzzle.getPiece(i).rotateAround(angle, pointOfRot);
            //puzzle.getPiece(i).rotateAround((float)Math.PI/2f, pointOfRot);
            //System.out.println(puzzle.getPiece(i).getAngle());
            //System.out.println(Arrays.toString(puzzle.getPiece(i).getVerticesAsVecs().toArray()));
        }
    }

    private boolean legalOverlapEstimate(){
        if (getStack().isEmpty()){
            return true;
        }
        PuzzleConnection last = getStack().peek();
        PuzzlePiece p = puzzle.getPiece(last.outlierID);

        float sqMag = 0;
        for (Vertex v : p.getVertices()){
            float sm = Vec.sub(v.getPosition(),p.getPosition()).sqMag();
            if (sm > sqMag)
                sqMag = sm;
        }
        for (PuzzlePiece other : matched){
            if (other == p)
                continue;
            if (Vec.sub(other.getPosition(),p.getPosition()).sqMag() < sqMag){
                return false;
            }
        }
        return true;
/*
        for (PuzzlePiece other : matched){
            if (other == p)
                continue;
            for (Edge e1 : p.getEdges()){
                for (Edge e2 : other.getEdges()){
                    if (FiniteLine.intersects(e1,e2) && !Approx.areParallel(e1,e2))
                    {
                        Vec point = FiniteLine.tryGetIntersection(e1,e2);
                        if (!(Approx.areEqual(point,e1.getP1()) || Approx.areEqual(point,e1.getP2())
                                || Approx.areEqual(point,e2.getP1()) || Approx.areEqual(point,e2.getP2()))){
                            return false;
                        }
                        //System.out.println("invalid " + FiniteLine.intersects(e1,e2) + " " + !Approx.areParallel(e1,e2));
                        //return true;
                    }
                }
            }
        }
        return true;*/

        /*float r1 =

        for (PuzzlePiece other : matched) {
            if (other == p)
                continue;
            for ()
        }*/
        //return true;
    }

}
/*

    private List<PuzzlePiece> f(){
        List<PuzzlePiece> ret = new ArrayList<>();
        for (PuzzleConnection x : getStack()){
            ret.add(puzzle.getPiece(x.outlierID));
        }
        ret.add(puzzle.getPiece(0));
        return ret;
    }

    private List<PuzzlePiece> f() {
        List<PuzzlePiece> ret = new ArrayList<>();
        for (int i : solvingStrategy.getMatched())
            ret.add(puzzle.getPiece(i));
        return ret;
    }
 */