package main.puzzle.solving.algorithms.skeleton;

import main.puzzle.logic.IPuzzle;

import java.util.Collection;
import java.util.List;


//Class code by Frederik (s194619)
public interface IPossibilityCalculator<Type> {

    List<Type> possibleSteps(Collection<Type> matched, Collection<Type> candidates);
}
