package main.puzzle.solving.algorithms.skeleton;

import main.puzzle.logic.IPuzzle;
import main.puzzle.util.algorithms.backtracking.IBackTracker;
import main.puzzle.solving.connecting.PuzzleConnection;

import java.util.HashSet;

//Class code by Frederik (s194619)
public abstract class SolvingStrategy implements IBackTracker<PuzzleConnection> {

    private HashSet<Integer> remaining = new HashSet<>();
    //private HashSet<Integer> matched = new HashSet<>();

    public HashSet<Integer> getRemaining() {
        return remaining;
    }

    //public HashSet<Integer> getMatched() {
    //    return matched;
    //}

    public void setup(IPuzzle puzzle) {
        remaining.clear();
        //matched.clear();

        //matched.add(getInitial());
        for (int i = 0; i < puzzle.getPieceCount(); i++){
            if (i != getInitial()) {
                remaining.add(i);
            }
        }
    }

    @Override
    public void backtrack(PuzzleConnection pop) {
        getRemaining().add(pop.outlierID);
        //getMatched().remove(pop.outlierID);
        pop.tryUndo();
    }

    @Override
    public void advance(PuzzleConnection push) {
        getRemaining().remove(push.outlierID);
        //getMatched().add(push.outlierID);
        push.tryApply();
    }

    public int getInitial()
    {
        return 0;
    }
}
