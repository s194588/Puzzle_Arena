package main.puzzle.logic;

import main.puzzle.dataloading.IPuzzleLoader;
import main.puzzle.util.general.tuples.Duo;
import main.puzzle.util.geometry.linearalgebra.Vec;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.polygon.Edge;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.PolygonUtil;
import main.puzzle.util.geometry.polygon.Vertex;

import java.io.IOException;
import java.util.*;

//Class code by Frederik (s194619)
public class Puzzle implements IPuzzle {

    public Puzzle(IPuzzleLoader puzzleLoader){
        this.puzzleLoader = puzzleLoader;
        try {
            reload();
        }catch(Exception e){
            System.out.println("Error loading puzzle");
        }
    }

    private IPuzzleLoader puzzleLoader;

    private Polygon form;
    private Map<Integer, PuzzlePiece> pieceMap = new HashMap<>();

    /**
     * Returns the form of the puzzle (as a polygon).
     */
    public Polygon getForm() {
        return form;
    }

    //Old note: other solution than synchronized on getPiece and getPieceCount?

    /**
     * Returns the puzzle pieces associated with the given id. The range of ids is [0; getPieceCount()[.
     */
    public PuzzlePiece getPiece(int id){
        return pieceMap.get(id);
    }

    /**
     * Returns the total number of pieces.
     */
    public int getPieceCount() {
        return pieceMap.size();
    }

    public void reload() throws IOException {
        Vec[] formPoints = puzzleLoader.loadForm();
        form = new Polygon(formPoints);
        pieceMap.clear();
        //System.out.println("xd");
        Vec[][] allPoints = puzzleLoader.loadPuzzle();
        //System.out.println("xxxdddd");
        int i = 0;
        for (Vec[] v : allPoints) {
            PuzzlePiece p = new PuzzlePiece(v);
            pieceMap.put(i, p);
            i++;
        }
    }

    /**
     * Checks if the puzzle contains identical pieces.
     */
    @Override
    public boolean containsIdenticalPieces()
    {
        List<Duo<Integer,Integer>> failed = new ArrayList<>();
        //System.out.println(getPieceCount());
        for (int i = 0; i < getPieceCount(); i++){
            for (int j = 0; j < i; j++){
                //if (i != j)
                //System.out.println("i, j: " + i + ", " + j);
                if (getPiece(i).areIdentical(getPiece(j))){
                    failed.add(new Duo<>(i,j));
                }
            }
        }
        /*if (!failed.isEmpty()) {
            System.out.println("Failed on (IDs are based on the backend, not necessarily what is in the JSON): ");
            for (Duo d : failed) {
                System.out.print(d + ", ");
            }
            System.out.println();
        }*/

        return !failed.isEmpty();
    }


    private List<PuzzlePiece> puzzlePieces;

    /**
     * Checks whether the puzzle is solved.
     */
    public boolean isSolved() {
        if (puzzlePieces == null){
            puzzlePieces = new ArrayList<>();
            for (Map.Entry<Integer, PuzzlePiece> entry : pieceMap.entrySet()){
                puzzlePieces.add(entry.getValue());
            }
        }
        //System.out.println(!hasInsideOverlap(puzzlePieces) + " and " + allInForm(puzzlePieces));
        return !hasInsideOverlap(puzzlePieces) && allInForm(puzzlePieces);
    }

    private boolean areaFit(List<PuzzlePiece> pieces)
    {
        Polygon sol = Polygon.boundingBox(pieces);
        float areaSol = PolygonUtil.rectArea(sol);
        float areaForm = PolygonUtil.rectArea(getForm());

        //System.out.println("area form: " + areaForm);
        //System.out.println("v1: " + getForm().getVertices().get(0).getPosition() + ", v2: " + getForm().getVertices().get(1).getPosition() + ", v3: " + getForm().getVertices().get(2).getPosition() + ", v4: " + getForm().getVertices().get(3).getPosition());
        //System.out.println("area sol: " + areaSol);
        //System.out.println("v1: " + sol.getVertices().get(0).getPosition() + ", v2: " + sol.getVertices().get(1).getPosition() + ", v3: " + sol.getVertices().get(2).getPosition() + ", v4: " + sol.getVertices().get(3).getPosition());

        return areaSol <= areaForm * 1.05f;
    }

    private boolean hasInsideOverlap(List<PuzzlePiece> pieces)
    {
        for (int i = 0; i < pieces.size(); i++){
            for (int j = 0; j < i; j++){
                if (pieces.get(i).fastIOE2(pieces.get(j))){
                    //System.out.println("Failed at i="+i + ", j="+j);
                    return true;
                }
            }
        }
        return false;
    }

    private float width = -1;
    private float height = -1;

    public float getFormWidth()
    {
        if (width == -1){
            width = FiniteLine.lengthOf(getForm().boundingBox().getEdges()[1]);
        }
        return width;

    }

    public float getFormHeight(){
        if (height == -1){
            height = FiniteLine.lengthOf(getForm().boundingBox().getEdges()[0]);
        }
        return height;
    }

    private boolean allInForm(List<PuzzlePiece> pieces){
        for (PuzzlePiece p : pieces){
            if (!inForm(p)){
                return false;
            }
        }
        return true;
    }

    private boolean inForm(PuzzlePiece p)
    {
        float c = 0.1f;
        float w = getFormWidth() * (1+c);
        float h = getFormHeight() * (1+c);
        for (Vertex v : p.getVertices())
        {
            if (v.getPosition().x > w/2f || v.getPosition().x < -w/2f
            || v.getPosition().y > h/2f || v.getPosition().y < -h/2f)
                return false;
        }
        return true;
    }

    /*
        public boolean isSolved() {
        //TODO: Make more generous.
        //System.out.println("Checking if puzzle is solved...");

        form.getVertices().toString();

        for (Map.Entry<Integer, PuzzlePiece> entry : pieceMap.entrySet()){
            if (!form.hasInside(entry.getValue()))
                //System.out.println("The puzzle is not solved.");
                return false;
        }
        //System.out.println("The puzzle is solved.");
        return true;
    }
     */
}
