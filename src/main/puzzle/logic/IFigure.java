package main.puzzle.logic;

import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.List;

public interface IFigure {
    Vec getPosition();

    void setPosition(Vec v);

    float getAngle();

    void setAngle(float a);

    List<Vertex> getVertices();
}
