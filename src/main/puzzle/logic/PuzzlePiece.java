package main.puzzle.logic;

import main.puzzle.util.interfaces.IObserver;
import main.puzzle.util.geometry.approximation.Approx;
import main.puzzle.util.geometry.lines.FiniteLine;
import main.puzzle.util.geometry.polygon.Edge;
import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.LinkedList;
import java.util.List;

//Class code by Frederik (s194619)
public class PuzzlePiece extends Polygon implements IFigure
{
    public PuzzlePiece(Vec... v){
        super(v);
        position = Vec.average(getVerticesAsVecs());
        /*float sqMag = 0;
        for (Vertex v1 : getVertices()){
            for (Vertex v2 : getVertices()){
                Vec vec = Vec.sub(v2.getPosition(),v1.getPosition());
                if (vec.sqMag() > sqMag) {
                    position = Vec.add(v1.getPosition(), Vec.mul(vec,0.5f));
                    sqMag = vec.sqMag();
                }
            }
        }*/
    }

    private Vec position = Vec.zero;
    private Matrix2 rotation;
    private float angle;

    public Vec getPosition() {
        return position;
    }

    /**
     * Sets the position of the puzzle piece. Cannot be null.
     */
    public void setPosition(Vec pos) {
        if (pos == null)
            throw new IllegalArgumentException("Position cannot be null");
        for (Vertex v : getVertices())
        {
            v.setPosition(Vec.add(Vec.sub(v.getPosition(), position), pos));
        }
        position = pos;
        notifyObservers();
    }

    /**
     * Returns the rotated angle of the puzzle pieces in radians.
     */
    public float getAngle() {
        return angle;
    }

    /**
     * Sets the angle of rotation using the position as point of rotation. Input is interpreted as radians.
     */
    public void setAngle(float a) {
        //setAngle(a, Vec.average(getVerticesAsVecs()));
        if (a == angle)
            return;
        rotateAround(a - angle, getPosition());
    }
    /**
     * Rotates this around a given a point of rotation. Input is interpreted as radians.
     */
    public void rotateAround(float delta, Vec ZAxisPoint)
    {
        rotation = Matrix2.rotMatrix(delta);
        for (Vertex v : getVertices())
        {
            v.setPosition(Vec.add(Matrix2.MVProd(rotation,Vec.sub(v.getPosition(), ZAxisPoint)),ZAxisPoint));
        }
        position = Vec.add(Matrix2.MVProd(rotation,Vec.sub(position, ZAxisPoint)),ZAxisPoint);
        angle += delta;
        notifyObservers();
    }


    private List<IObserver> observers = new LinkedList<>();

    public void addObserver(IObserver o){
        observers.add(o);
    }

    public void removeObserver(IObserver o){
        observers.remove(o);
    }

    public void notifyObservers(){
        for(IObserver o : observers){
            o.update();
        }
    }

    private float min = -1, max = -1;

    public float minRadius(){
        float dist = FiniteLine.pointDist(getEdges()[0],getPosition());
        if (min == -1){
            for (Edge e : getEdges()){
                float candidate = FiniteLine.pointDist(e,getPosition());
                if (candidate < dist){
                    dist = candidate;
                }
            }
        }
            //min = Vec.sub(Vec.closestVec(getPosition(), getVerticesAsVecs()), getPosition()).length();
        return dist;
    }

    public float maxRadius() {
        if (max == -1)
            max = Vec.sub(Vec.furthestVec(getPosition(), getVerticesAsVecs()), getPosition()).length();
        return max;
    }


    /**
     * Estimates if the is overlap by checking if sides intersect. May give false negatives, but not false positives.
     */
    //May have errors - needs testing
    public boolean fastIOE1(PuzzlePiece other)
    {
        for (Edge e1 : getEdges()){
            for (Edge e2 : other.getEdges()){
                if (!Approx.areParallel(e1,e2) && FiniteLine.intersects(e1,e2))
                {
                    Vec point = FiniteLine.tryGetIntersection(e1,e2);
                    if (!(Approx.areEqual(point,e1.getP1()) || Approx.areEqual(point,e1.getP2())
                            || Approx.areEqual(point,e2.getP1()) || Approx.areEqual(point,e2.getP2()))){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Estimates if there is overlap by using circles. May give false negatives, but not false positives.
     */
    public boolean fastIOE2(PuzzlePiece other)
    {
        //System.out.println((minRadius() + other.minRadius()) + " > " + Vec.sub(getPosition(),other.getPosition()).length());
        return (minRadius() + other.minRadius()) * 0.95f > Vec.sub(getPosition(),other.getPosition()).length();
    }

    public void setPositionAnchor(Vec v)
    {
        position = v;
    }
}
//GUI only moves piece when mouse moves. Not on click

/*
package obsolete.puzzle.pieces;

import main.puzzle.util.geometry.polygon.Polygon;
import main.puzzle.util.geometry.polygon.Vertex;
import main.puzzle.util.general.listandarrays.ListAndArrayUtil;
import main.puzzle.util.geometry.linearalgebra.Matrix2;
import main.puzzle.util.geometry.linearalgebra.Vec;

import java.util.ArrayList;
import java.util.List;

public class PU
{
    private List<Polygon> pieces = new ArrayList<>();
    private Polygon union;

    public PU(Polygon initialPiece){
        pieces.add(initialPiece);
        union = initialPiece;
    }

     // Connects the piece by applying a polygon union.
     // Does not check the type of overlap or if the conditions for a legal connection are met.
public void connect(Polygon piece)
{
    union = Polygon.union(union,piece);
}

    public boolean isLegalConnection(Polygon piece)
    {
        return isLegalConnection(piece, new ConnectionCondition[0]);
    }

    public boolean isLegalConnection(Polygon piece, ConnectionCondition... conditions)
    {
        if (union.overlaps(piece) && !union.insideOverlaps(piece))
        {
            //Vertex_Overlap = perfect fit
            if (ListAndArrayUtil.arrayContains(conditions,ConnectionCondition.VERTEX_OVERLAP))
            {

            }
            return true;
        }
        return false;
    }

    private Vec position;
    private Matrix2 rotation;
    private float angle;

    public Vec getPosition() {
        return position;
    }

    public void setPosition(Vec position) {
        if (position == null)
            throw new IllegalArgumentException("Position cannot be null");
        this.position = position;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float a) {
        float newAngle = a % 2*(float)Math.PI;
        rotation = Matrix2.rotMatrix(newAngle - angle);
        //NOT DONE YET - NEEDS TO REMEMBER IT FOR ALL VERTICES NOT JUST IN THE UNION.
        for (Vertex v : union.getVertices())
        {
            v.setPosition(Matrix2.MVProd(rotation,v.getPosition()));
        }
    }

    public final List<Vertex> getVertices() {
        return union.getVertices();
    }
}
 */