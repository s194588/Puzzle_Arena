package main.puzzle.logic;

import main.puzzle.util.geometry.polygon.Polygon;

import java.io.IOException;

public interface IPuzzle {
    Polygon getForm();

    PuzzlePiece getPiece(int id);

    int getPieceCount();

    void reload() throws IOException;

    boolean isSolved();

    boolean containsIdenticalPieces();
}

